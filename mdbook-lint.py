#!/usr/bin/python

from pprint import pprint
import os
import re
import sys
import json
import itertools
from datetime import datetime
from unidecode import unidecode

# Logging
# https://docs.python.org/3/howto/logging.html
# DEBUG, INFO, WARNING, ERROR, CRITICAL
import logging
#logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)


def validate_date(date: str) -> bool:
    try:
        datetime.strptime(date, 'pub_date: %a, %d %b %Y %H:%M:%S %Z')
        return True
    except ValueError as ex:
        logging.error(ex)
        return False


def get_all_rating_files() -> list:
    SECTIONS = ["shows", "movies"]
    result = []
    for section in SECTIONS:
        for directory, subdirectories, files in os.walk("src/"+section, topdown=False):
            for filename in files:
                path = os.path.join(directory, filename)
                if not re.match(r'.*/images/.*', path):
                    result.append(path)
    return result


class SummaryClass():
    def __init__(self, path: str) -> None:
        self.path = path

        # Read content
        with open(self.path, "r", encoding="utf-8") as summary_file:
            self.raw_content = summary_file.read().splitlines()

        # Extract listed movies and shows
        self.items = {}
        section_with_items = False
        for line in self.raw_content:
            if re.match(r'\- \[.*\]\(./.*\)$', line):
                section_with_items = False
            if re.match(r'\- \[(Movies|Shows)\]\((movies|shows).md\)$', line):
                section_with_items = True
                continue
            if re.match(r'^\t\- \[.*\]\(.*\.md\)$', line) and section_with_items:
                found = re.search(r'\[(.*)\]\((.*)\)', line)
                name = found.group(1)
                path = found.group(2)
                self.items[path] = name

    def print(self) -> None:
        print("SummaryClass")
        print("- path:", self.path)
        print("- items:")
        pprint(self.items)


class FileClass():
    def __init__(self, path: str) -> None:
        self.path = path
        self.path_without_src = re.sub(r'src/', r'', path)
        self.filename_full = os.path.basename(self.path)
        self.filename = os.path.splitext(os.path.basename(self.path))[0]

        # Read content
        try:
            with open(self.path, "r", encoding="utf-8") as file_file:
                self.raw_content = file_file.read().splitlines()
        except:
            logging.error(f'Failed to open {self.path}')
            sys.exit(1)

        # Regular expressions to find attributes and to verify them
        self.attributes = {
            'PUB_DATE': {
                'regex_match': r'^\w{3}, \d{1,2} \w{3} \d{4} \d{1,2}:\d{2}:\d{2} UTC$',
            },
            'MOVIE_NAME': {
                'regex_match': r'^[\w\d \(\):,;\+\/\-\'\.\?]*$',
                #'regex_match': r'[\w\d \(\):/\-\'\.]* \(\d{4}\)',
            },
            'MOVIE_URL': {
                'regex_match':r'^https:\/\/.*',
            },
            'MOVIE_IMG': {
                'regex_match': r'^[a-z_\-\d]+.(jpg|png)$',
            },
            'PLOT': {
                'regex_match': r'.+',
            },
            'WATCHED_UNTIL': {
                'regex_match': r'(Watched .*|$)',
            },
            'FUCKED_UP_LEVEL': {
                'regex_match': r'^[0-4]$',
            },
            'RELEASE_VERSION': {
                'regex_match': r'.+',
            },
            'SPOKEN_LANG': {
                'regex_match': r'^[A-Z]{3}$',
            },
        }

        for item in [
            'VISUAL_WATCHABLE',
            'VISUAL_STORYTELLING',
            'MUSIC_MEMORABLE',
            'MUSIC_STORYTELLING',
            'STORY_SENSIBLE',
            'STORY_SMART',
            'STORY_ENDING',
            'FEELING_MEMORABLE',
            'FEELING_RELATABLE',
            'OVERALL_WATCHED_TO_END',
            'OVERALL_WOULD_WATCH_AGAIN',
            'OVERALL_WOULD_PAY_FOR',
            'OVERALL_WOULD_RECOMMEND',
            ]:
            self.attributes[item] = {
                'regex_match':  r'^(Yes|No|Maybe|N\/A)$',
            }

        for item in [
            'RELIGIOUS',
            'VIOLENCE',
            'DRUGS',
            'EROTIC',
            'SEX',
            ]:
            self.attributes[item] = {
                'regex_match':  r'^(Yes|No|Maybe|Sometimes|N\/A)$',
            }

        # Pre-fill with default values
        for key in self.attributes.keys():
            self.attributes[key]['value'] = None
            self.attributes[key]['regex_search'] = fr'^\s+{key}=(.*)$'

        # Search for atributes and verify their values
        for key in self.attributes.keys():
            for line in self.raw_content:
                if re.match(self.attributes[key]['regex_search'], line):
                    capture_group = re.search(self.attributes[key]['regex_search'], line).group(1)
                    self.attributes[key]['value'] = capture_group
                    if not re.match( self.attributes[key]['regex_match'], capture_group ):
                        logging.error(f'Invalid value for {key} in file: {self.path}')
                    break
            if self.attributes[key]['value'] is None:
                logging.error(f'Attribute {key} not found in file: {self.path}')

        # Search for 'None' values after conversion to templates
        for key in self.attributes.keys():
            if self.attributes[key]['value'] == 'None':
                logging.error(f'Attribute {key} is None in file: {self.path}')

        # Expose few attributes
        self.imagename = re.sub(r'\.(jpg|png)', r'', self.attributes['MOVIE_IMG']['value'])


class Template():
    def __init__(self, path: str) -> None:
        self.path = path

    def list_attributes(self) -> list:
        attributes = []
        with open(self.path, 'r', encoding='utf-8') as temp:
            self.content = temp.read().splitlines()

        for line in self.content:
            attr_regex = r'\[\[#([\w_]+)\]\]'
            if re.match(fr'.*{attr_regex}.*', line):
                attributes += re.findall(attr_regex, line)

        return attributes
            

if __name__ == '__main__':
    # Dissect summary
    summary = SummaryClass("src/SUMMARY.md")
    # summary.print()

    # Get all attributes used in templates
    list_of_all_attributes = []
    for temp in ['src/templates/movie.md', 'src/templates/show.md']:
        tmp = Template(path=temp)
        list_of_all_attributes += tmp.list_attributes()
    list_of_all_attributes = list(set(list_of_all_attributes))

    # Get all files with ratings
    rating_files = get_all_rating_files()

    for rating_file in rating_files:
        myfile = FileClass(rating_file)

        # Filename formatting
        if not re.match(r'[a-z0-9\-:_]+__[0-9]{4}', myfile.filename):
            logging.warning("%s: Filename does not match naming format", myfile.path)
        else:
            logging.debug("%s: Filename does match naming format", myfile.path)

        # Not listed in in SUMMARY
        if not myfile.path_without_src in summary.items:
            logging.warning("%s: Item is missing in summary", myfile.path)
        else:
            logging.debug("%s: Item is present in summary", myfile.path)

        # Mismatch of name in SUMMARY and FILE
        if myfile.path_without_src in summary.items and myfile.attributes['MOVIE_NAME']['value'] != summary.items[myfile.path_without_src]:
            logging.warning("%s: Mismatch of name in SUMMARY and FILE", myfile.path)
            logging.warning(" - in SUMMARY:  %s",
                            summary.items[myfile.path_without_src])
            logging.warning(" - in FILE:     %s", myfile.attributes['MOVIE_NAME']['value'])
        else:
            logging.debug("%s: Match of name in SUMMARY and FILE", myfile.path)

        # Image mismatch
        if not myfile.imagename == myfile.filename:
            logging.warning("%s: Wrong image used in", myfile.path)
            logging.warning(" - expected:  %s", myfile.filename)
            logging.warning(" - used:      %s", myfile.imagename)
        else:
            logging.debug("%s: Correct image used", myfile.path)

        # Missing attributes
        diff = set( myfile.attributes.keys()) - set(list_of_all_attributes)
        if len(diff) > 0:
            logging.error(f'Missing attribute in file: {myfile.path}')
            pprint(diff)
            print('** Found attributes **')
            pprint( set(list_of_all_attributes) )
            print('** All attributes **')
            pprint( set(myfile.attributes.keys()) )

