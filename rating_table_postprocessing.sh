#!/bin/bash

find_and_replace() {
	# Yes/No/Maybe/Sometimes
	find . -name '*.html' -exec sed -i 's/style="text-align: center">Yes</style="text-align: center;background-color:green;color: white">Yes</g' {} \;
	find . -name '*.html' -exec sed -i 's/style="text-align: center">No</style="text-align: center;background-color:red;color: white">No</g' {} \;
	find . -name '*.html' -exec sed -i 's/style="text-align: center">Maybe</style="text-align: center;background-color:yellow;color: black">Maybe</g' {} \;
	find . -name '*.html' -exec sed -i 's/style="text-align: center">Sometimes</style="text-align: center;background-color:yellow;color: black">Sometimes</g' {} \;
	# Fucked-up
	find . -name '*.html' -exec sed -i 's/style="text-align: center">0</style="text-align: center;background-color:green;color: white">0</g' {} \;
	find . -name '*.html' -exec sed -i 's/style="text-align: center">1</style="text-align: center;background-color:green;color: white">1</g' {} \;
	find . -name '*.html' -exec sed -i 's/style="text-align: center">2</style="text-align: center;background-color:yellow;color: black">2</g' {} \;
	find . -name '*.html' -exec sed -i 's/style="text-align: center">3</style="text-align: center;background-color:orange;color: black">3</g' {} \;
	find . -name '*.html' -exec sed -i 's/style="text-align: center">4</style="text-align: center;background-color:red;color: white">4</g' {} \;
	find . -name '*.html' -exec sed -i 's/style="text-align: center">5</style="text-align: center;background-color:black;color: white">5</g' {} \;
}

cd public
find_and_replace

