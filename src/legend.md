
## Rating explained

- Visuals
	- Watchable?
		- *Well, can you watch it without your eyes bleeding?*
	- Does it support the story telling?
		- *For example, are the feelings of main character reflected in used colour pallete?*
- Music
	- Memorable?
		- *Is the soundtrack unique, iconic and recognisable?*
	- Does it support the story telling?
		- *For example, are the feelings of main character reflected in music?*
- Story
	- Does it make sense?
		- *Are there errors in logic or decission making of characters? For example are character's reactions realistic?*
	- Smart?
		- *Are there clever twists? Maybe even some meta-commentary?*
	- Good ending?
		- *Is the ending satisfying? Does it wrap up all loose ends?*
- Feeling
	- Memorable?
		- *Is the movie/show memorable?*
	- Relatable?
		- *Can I relate to the characters? Or maybe the message in the movie/show?*
- Overall
	- Would I watch it again?
		- *Was is so good that I would not mind to watch it all over again?*
	- Would I pay for it?
		- *Would I consider it worth my money? Or would I demand refund?*
	- Would I recommend it?
		- *Would I recommend it to someone else?*


- Additional
	- How much is the story disturbing/fucked up?
		- *0 = not at all*
		- *1 = a little bit, might make you think*
			- thought provoking topics
		- *2 = there is some heavy stuff*
			- death, loss, relationships, tragic accidents, mental ilness, existencionalism, ...
		- *3 = you might not sleep tonight*
			- suicide, self-harm, malevolence, ...
		- *4 = seek professional help after viewing*
			- traumatising experience, something seriously messed up
	- Contains religious references?
		- *Does it reference some religious movements? (some light references might be ignored)*
- Version
	- Release version
		- `Standard` is the most common version, then there are other versions like `Director's cut`, `Remastered` and so on
	- Spoken language
		- Which language is in audio track, written in [ISO 639-2 code](https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes)


### Score

Each of the items can have one of the following answers:

| Answer | Score |
|:-------|:-----:|
| Yes    | +2    |
| Maybe  | +1    |
| No     |  0    |
| N/A    |       |

- `Yes` means definitive YES.
- `Maybe` means either very little, or very sparse occurrence.
- `No` means definitive NO.
- `N/A` means `Not Applicable` and is used in special cases, like documentaries where it is difficult to find a story for example. This answer is not included in the score calculation.

Final score `S` can be then calculated with following equation, where `n` is the number of questions, and `x` is question score. Giving resulting score between `0%` and `100%`:

\\[ S = \frac{100}{2n} \sum_{i=0}^n x_i \\]

Simply said, score of:
- `0%` means unwatchable utter garbage
- `50%` is for mediocre
- `100%` is perfection incarnated

