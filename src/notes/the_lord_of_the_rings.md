---
<details><summary>VIEWING GUIDE</summary>

- Lord of the Rings: The Rings of Power (2022)
- The Hobbit: An Unexpected Journey (2012)
- The Hobbit: The Desolation of Smaug (2013)
- The Hobbit: The Battle of the Five Armies (2014)
- [The Lord of the Rings: The Fellowship of the Ring (2001)](the_lord_of_the_rings__2001.md)
- [The Lord of the Rings: The Two Towers (2002)](the_lord_of_the_rings__2001.md)
- [The Lord of the Rings: Return of the King (2003)](the_lord_of_the_rings__2001.md)

</details>

---

