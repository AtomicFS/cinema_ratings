
```admonish tip collapsible=true title="Viewing guide"
Alright, let me start by stating the obvious: this franchise is confusing. Not only the story, but also the multitude of variants that were released.

- [Neon Genesis Evangelion (1995)](../shows/neon_genesis_evangelion__1995.md) ([themoviedb.org](https://www.themoviedb.org/tv/890-neon-genesis-evangelion))
	- The original anime series
	- Episodes 25 and 26 are rather odd and have been met with mixed feelings from audience (mostly negative). TLDR: the studio ran into financial problems and had to improvise
	- Episodes 20, 22, 23 and 24 also have Director's cut (preferred over original thanks to additional content)
	- **SEEN** (considered important, episodes 25 and 26 are not as important)
- `Neon Genesis Evangelion: Death & Rebirth` ([themoviedb.org](https://www.themoviedb.org/movie/21832-shin-seiki-evangelion-gekij-ban-shito-shinsei))
	- Two-part film, mostly remix of the original show `Neon Genesis Evangelion` with few outtakes (basically recap).
	- **NOT SEEN** (considered optional)
	- TLDR: `Neon Genesis Evangelion: Death & Rebirth` = `Neon Genesis Evangelion` + `outtakes`
- `Evangelion: Death (True)` ([themoviedb.org](https://www.themoviedb.org/movie/857862-evangelion-death-true))
	- Re-edited version of `Neon Genesis Evangelion: Death & Rebirth`
	- TLDR: `Evangelion: Death (True)` = `Neon Genesis Evangelion: Death & Rebirth`
	- **NOT SEEN** (considered optional)
- `Revival of Evangelion` ([themoviedb.org](https://www.themoviedb.org/movie/54270))
	- TLDR: `Revival of Evangelion` = `Evangelion: Death (True)` + `Neon Genesis Evangelion: Death & Rebirth`
	- **NOT SEEN** (considered optional)
- [Neon Genesis Evangelion: The End of Evangelion (1997)](../movies/neon_genesis_evangelion_the_end_of_evangelion__1997.md) ([themoviedb.org](https://www.themoviedb.org/movie/18491))
	- Replacement for episode 25 and 26 of the original
	- **SEEN** (considered important)
- The Rebuild of Evangelion
	- Collection of films:
		- [Evangelion: 1.0 You Are (Not) Alone](evangelion_111_you_are_not_alone__2007.md) ([themoviedb.org](https://www.themoviedb.org/movie/15137-evangerion-shin-gekij-ban-jo))
		- [Evangelion: 2.0 You Can (Not) Advance](evangelion_222_you_can_not_advance__2009.md) ([themoviedb.org](https://www.themoviedb.org/movie/22843-evangerion-shin-gekij-ban-ha))
		- [Evangelion: 3.0 You Can (Not) Redo](evangelion_333_you_can_not_redo__2012.md) ([themoviedb.org](https://www.themoviedb.org/movie/75629-evangerion-shin-gekij-ban-kyu))
		- [Evangelion: 3.0 + 1.0 Thrice Upon a Time](evangelion_30_10_thrice_upon_a_time__2021.md)([themoviedb.org](https://www.themoviedb.org/movie/283566))
		- `Evangelion: 3.0 + 2.0 The End of Hideaki Anno`
	- Starts by retelling the original series, but soon diverges into very different direction.
	- NOTE: Films numbered `1.11`, `2.22` and `3.33` are enhanced version for home release with additional content (watch these rather then `1.0`, `2.0` and `3.0`).
	- **SEEN** (considered optional)

As for my recommendation, watch only this:
- [Neon Genesis Evangelion (1995)](../shows/neon_genesis_evangelion__1995.md) episodes 1-20
- [Neon Genesis Evangelion (1995)](../shows/neon_genesis_evangelion__1995.md) episodes 21-24 Director's cut
- [Neon Genesis Evangelion: The End of Evangelion (1997)](../movies/neon_genesis_evangelion_the_end_of_evangelion__1997.md)
- [Neon Genesis Evangelion (1995)](../shows/neon_genesis_evangelion__1995.md) episodes 25 and 26

This is in my opinion the best combination, especially if you want to keep it simple. However feel free to watch more ;)

Some sources:
- [How to watch Neon Genesis Evangelion in order – including the Rebuild movies](https://www.gamesradar.com/neon-genesis-evangelion-order-netflix-end-of-evangelion-rebuild-movies/)
- [A Simple Watch Order Guide to Neon Genesis Evangelion](https://www.otaquest.com/neon-genesis-evangelion-watch-guide/)
```

