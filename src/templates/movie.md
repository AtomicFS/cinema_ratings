---
pub_date: [[#PUB_DATE]]
description: [[#MOVIE_NAME]]
---

# [ [[#MOVIE_NAME]] ]( [[#MOVIE_URL]] )

![](images/[[#MOVIE_IMG]])


```admonish info collapsible=true title="Plot (might contain spoilers)"
[[#PLOT]]
```


## My rating

| Category    | Question                           | Yes/Maybe/No                   |
|:------------|:-----------------------------------|:------------------------------:|
| **Visuals** |                                    |                                |
|             | Watchable?                         | [[#VISUAL_WATCHABLE]]          |
|             | Does it support the story telling? | [[#VISUAL_STORYTELLING]]       |
| **Music**   |                                    |                                |
|             | Memorable?                         | [[#MUSIC_MEMORABLE]]           |
|             | Does it support the story telling? | [[#MUSIC_STORYTELLING]]        |
| **Story**   |                                    |                                |
|             | Does it make sense?                | [[#STORY_SENSIBLE]]            |
|             | Smart?                             | [[#STORY_SMART]]               |
|             | Good ending?                       | [[#STORY_ENDING]]              |
| **Feeling** |                                    |                                |
|             | Memorable?                         | [[#FEELING_MEMORABLE]]         |
|             | Relatable?                         | [[#FEELING_RELATABLE]]         |
| **Overall** |                                    |                                |
|             | Did I watch it to the end?         | [[#OVERALL_WATCHED_TO_END]]    |
|             | Would I watch it again?            | [[#OVERALL_WOULD_WATCH_AGAIN]] |
|             | Would I pay for it?                | [[#OVERALL_WOULD_PAY_FOR]]     |
|             | Would I recommend it?              | [[#OVERALL_WOULD_RECOMMEND]]   |


## Supplementary information

| Category       | Question                                    |                         |
|:---------------|:--------------------------------------------|:-----------------------:|
| **Additional** |                                             |                         |
|                | How much is the story disturbing/fucked up? | [[#FUCKED_UP_LEVEL]]    |
|                | Contains religious references?              | [[#RELIGIOUS]]          |
|                | After credit scene?                         | [[#AFTER_CREDIT_SCENE]] |
|                | Violence?                                   | [[#VIOLENCE]]           |
|                | Drugs?                                      | [[#DRUGS]]              |
|                | Erotic?                                     | [[#EROTIC]]             |
|                | Sex?                                        | [[#SEX]]                |
| **Version**    |                                             |                         |
|                | Release version                             | [[#RELEASE_VERSION]]    |
|                | Spoken language                             | [[#SPOKEN_LANG]]        |


## My notes


