# Cinema Ratings

```admonish warning
These are **purely subjective** ratings of movies and shows that I have watched.

Since you and I might have very different tastes, it is likely that my recommendations will not meet your expectations. Please keep that in mind while browsing these pages.
```

```admonish warning collapsible=true title="Disclaimer - legal stuff"
I do not own rights to the used promotional pictures, nor the plot text.

Copyright Disclaimer Under Section 107 of the Copyright Act in 1976; Allowance is made for "Fair Use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research.

Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, educational or personal use tips the balance in favor of fair use.

All rights and credit go directly to its rightful owners. No copyright infringement intended.
```


It is common that tastes and preferences change over time, therefore it is likely that ratings of these movies or shows can differ in the future. I might update them, but probably without any consistency or schedule. This is the main reason of this project - to show me how my preferences and perception change over time.

With all of that being said, please don't take these ratings as definitive. Your opinions might differ, as well as my future's self - which is perfectly fine.

```admonish
I prefer to watch movies and shows in language that I can understand (which is English most of the time).

I am slow reader and reading subtitles usually takes longer then the allocated screen time. So if movie, show or anime has English dubbing, that's likely what I have watched. In some special cases I will even compare dubbing and original.
```

[](https://cinema-ratings.white-hat-hacker.icu)

Important links:
- [This site](https://cinema-ratings.white-hat-hacker.icu/)
- [Main git repository at sourcehut](https://git.sr.ht/~atomicfs/cinema_ratings)
- [Mirror GitLab repository](https://gitlab.com/AtomicFS/cinema_ratings)
- [RSS feed](./rss.xml)


QR code with this site's link:

![](images/qr_code.png)

