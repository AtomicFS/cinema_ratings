{{#template
	../templates/movie.md
	PUB_DATE=Sun, 3 Apr 2022 21:10:00 UTC
	MOVIE_NAME=Office Space (1999)
	MOVIE_URL=https://www.imdb.com/title/tt0151804
	MOVIE_IMG=office_space__1999.jpg
	PLOT=Three office workers strike back at their evil employers by hatching a hapless attempt to embezzle money.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=No

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=0
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Movie about people slowly ground down by soulless corporate environment where capable people are stuck at the bottom and management is filled up to the brim with clueless managers.

Looking at the main character, it is interesting to see how he goes though various stages of mind during the story. From this frustrated hopeless corporate drone to whatever he becomes at the end.

My favorite character is Milton - the odd ball who constantly mumbles to himself and does not interact well with people. I just love how the actor managed to believably portrait such character, it is brilliant.

Overall, it is a good watch.

The sad thing is that since the movies was created, many businesses still work exactly the same way.

