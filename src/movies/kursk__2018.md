{{#template
	../templates/movie.md
	PUB_DATE=Sat, 19 Feb 2022 15:04:00 UTC
	MOVIE_NAME=Kursk (2018)
	MOVIE_URL=https://www.imdb.com/title/tt4951982
	MOVIE_IMG=kursk__2018.jpg
	PLOT=Barents Sea, August 12th, 2000. During a Russian naval exercise, and after suffering a serious accident, the K-141 Kursk submarine sinks with 118 crew members on board. While the few sailors who are still alive barely manage to survive, their families push for accurate information and a British officer struggles to obtain from the Russian government a permit to attempt a rescue before it is late. But general incompetence are against all their efforts.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=N/A
	STORY_SMART=N/A
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Movie inspired by real-life events. Unfortunately there is not a lot of credible details out there about the events, especially because recording of operating activity of the boat was disabled - basically there is very little information about what was going on aboard the submarine.

Anyway, the creators of the movie took some artistic decisions which do not necessarily align with known facts or widely accepted theories (thankfully not too many). In general the movie holds up to the available information. In addition, it does sometimes feel like there are too many political undertones, but the whole event was very political, so I guess it was unavoidable (although they could have tried to dial down the Russia is bad & the rest of the world is good narrative).

```admonish warning collapsible=true title="SPOILER WARNING"
To my pleasant surprise, they filmed the moment when inquisitive wife of sailor trapped in the submarine was forcefully sedated and removed from hearing (this actually happened) - I would have expected this to not pass through politically-correct-filter.
```

Overall, it is a watchable.

