{{#template
	../templates/movie.md
	PUB_DATE=Sun, 20 Feb 2022 10:40:00 UTC
	MOVIE_NAME=Meet the Robinson's (2007)
	MOVIE_URL=https://www.imdb.com/title/tt0396555
	MOVIE_IMG=meet_the_robinsons__2007.jpg
	PLOT=Lewis, a brilliant young inventor, is keen on creating a time machine to find his mother, who abandoned him in an orphanage. Things take a turn when he meets Wilbur Robinson and his family.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


The story is good, with some clever twists, and even though this is obviously aimed at younger audience, it is dealing with some heavy topics. I would say the only weakness of this movie are the visuals which could really use some rework.

If you get over the visuals, it is a decent watch, although a bit infantile.

