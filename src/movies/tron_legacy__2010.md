{{#template
	../templates/movie.md
	PUB_DATE=Tue, 20 Jun 2023 08:28:35 UTC
	MOVIE_NAME=Tron: Legacy (2010)
	MOVIE_URL=https://www.imdb.com/title/tt1104001
	MOVIE_IMG=tron_legacy__2010.jpg
	PLOT=Sam Flynn, the tech-savvy and daring son of Kevin Flynn, investigates his father's disappearance and is pulled into The Grid. With the help of a mysterious program named Quorra, Sam quests to stop evil dictator Clu from crossing into the real world.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I really love the soundtrack, it is so perfectly fitting the theme, it is memorable, it is perfect. Made by legendary [Daft Punk](https://en.wikipedia.org/wiki/Daft_Punk). Fun fact: they are actually in the movie - in the club they are the DJ's!

The visuals are also great ... maybe except young Kevin Flynn, that one hits a bit the [uncanny valley](https://en.wikipedia.org/wiki/Uncanny_valley).

What could be improved are the characters, they don't have much going on and are rather bland. 

Before you watch, consider watching the previous movie [Tron (1982)](tron__1982.md).

In general, one of my favorite movies.


```admonish warning collapsible=true title="SPOILER WARNING"
There are few unintentionally awkward moments, like when Kevin Flynn says to Quorra that he can't loose his son again right before venturing into the city - the head tilt is quite out of place. There are more for sure, but this one really stuck in my head.

Regarding the story and story-logic, I am still not sure what Clu would do if he succeeded. Just the fact that the virtual physics and real-world physics differ so much would make it very hard to transfer any kind of tech and machinery. But he could theoretically transfer all the programs, giving himself a army of loyal subjects. But then again, if the creators wanted to stay true to the original Tron movie, they were rather limited.
```

