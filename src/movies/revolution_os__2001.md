{{#template
	../templates/movie.md
	PUB_DATE=Thu, 16 Feb 2023 17:09:04 UTC
	MOVIE_NAME=Revolution OS (2001)
	MOVIE_URL=https://www.imdb.com/title/tt0308808
	MOVIE_IMG=revolution_os__2001.jpg
	PLOT=REVOLUTION OS tells the inside story of the hackers who rebelled against the proprietary software model and Microsoft to create GNU/Linux and the Open Source movement.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=N/A

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=N/A
	STORY_SMART=N/A
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=N/A

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=0
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Really good documentary about free and open source software. I would recommend this documentary to everyone.

