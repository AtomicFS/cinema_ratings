{{#template
	../templates/movie.md
	PUB_DATE=Thu, 17 Nov 2022 16:22:20 UTC
	MOVIE_NAME=Arthur and the Invisibles (2006)
	MOVIE_URL=https://www.imdb.com/title/tt0344854
	MOVIE_IMG=arthur_and_the_invisibles__2006.jpg
	PLOT=Arthur is a spirited ten-year old whose parents are away looking for work, whose eccentric grandfather has been missing for several years, and who lives with his grandmother in a country house that, in two days, will be repossessed, torn down, and turned into a block of flats unless Arthur's grandfather returns to sign some papers and pay off the family debt. Arthur discovers that the key to success lies in his own descent into the land of the Minimoys, creatures no larger than a tooth, whom his grandfather helped relocate to their garden. Somewhere among them is hidden a pile of rubies, too. Can Arthur be of stout heart and save the day? Romance beckons as well, and a villain lurks.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Maybe
	STORY_SMART=No
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=0
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Well, this was a disappointment.

The graphics are really out-dated given today's standards, but I could get over it. What I can't get over is the uninteresting story, cheesy and cliche moments and un-relatable characters. I wanted to stop watching multiple times, but I hoped it would get better (spoiler alert: it did not).

There is nothing interesting in this one, complete rubbish.

