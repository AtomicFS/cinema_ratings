{{#template
	../templates/movie.md
	PUB_DATE=Fri, 17 Feb 2023 20:59:55 UTC
	MOVIE_NAME=Princess Mononoke (1997)
	MOVIE_URL=https://www.imdb.com/title/tt0119698
	MOVIE_IMG=princess_mononoke__1997.jpg
	PLOT=Ashitaka, a prince of the disappearing Emishi people, is cursed by a demonized boar god and must journey to the west to find a cure. Along the way, he encounters San, a young human woman fighting to protect the forest, and Lady Eboshi, who is trying to destroy it. Ashitaka must find a way to bring balance to this conflict.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Theatrical
	SPOKEN_LANG=ENG
}}


According to [dubbing.fandom.com](https://dubbing.fandom.com/wiki/Princess_Mononoke), there are two slightly different version of English dubbing. From what I can tell, I have seen the theatrical variant. The dabbing sometimes feels a bit off, especially at the beginning, but it gets a bit better later on.

As for the movie - it is a gem in cinematography. As with most (if not all) Ghibli's works, it is a masterpiece. I love the soundtrack, I love the visuals, story is excellent, characters relatable ... it has everything just right.

The only flaw that I could find, a minor one, is that the animation or drawings are sometimes just slightly off. For example eye movement or face expression of side-characters. It is not really visible and it does not disturb the feeling, you will likely not even notice (I am just a nit-picking bastard).

I certainly recommend it. If you have not seen it yet, try to watch it uninterrupted and in one peace.

