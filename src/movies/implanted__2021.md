{{#template
	../templates/movie.md
	PUB_DATE=Mon, 16 Oct 2023 15:11:51 UTC
	MOVIE_NAME=Implanted (2021)
	MOVIE_URL=https://www.imdb.com/title/tt9786896
	MOVIE_IMG=implanted__2021.jpg
	PLOT=Sarah, a struggling young woman, agrees to volunteer as a test subject and be implanted with the LEXX nanochip for a pharmaceutical company experiment. She hopes that the money received for her participation will solve her financial troubles. When the implant turns sinister and orders her to commit crimes, Sarah is plunged into a murderous spiral with only one choice: to live or die.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Now this is a good movie about AI implant going rogue. This is what the movie [Upgrade (2018)](upgrade__2018.md) should have been like.

There is much less focus on the technical aspects (which is OK) and much more on the individual experience. Good movie.

