{{#template
	../templates/movie.md
	PUB_DATE=Tue, 19 Jul 2022 19:00:33 UTC
	MOVIE_NAME=Blade 3: Trinity (2004)
	MOVIE_URL=https://www.imdb.com/title/tt0359013
	MOVIE_IMG=blade_3_trinity__2004.jpg
	PLOT=For years, Blade has fought against the vampires in the cover of the night. But now, after falling into the crosshairs of the FBI, he is forced out into the daylight, where he is driven to join forces with a clan of human vampire hunters he never knew existed—The Nightstalkers. Together with Abigail and Hannibal, two deftly trained Nightstalkers, Blade follows a trail of blood to the ancient creature that is also hunting him—the original vampire, Dracula.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Maybe
	STORY_SMART=No
	STORY_ENDING=No

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


As before, the recipe did not change, again the same flaws as before. This time again with more questionable [CGI](https://en.wikipedia.org/wiki/Computer-generated_imagery), unlikable characters and badly written story.

And that ever-present unfunny childish humor to appease thirteen-year-olds, please stop.

Overall, in some aspects a little bit of improvement, but still not good.


```admonish warning collapsible=true title="SPOILER WARNING"
There is only one moment that I feel like is worth mentioning, and that when Danica Talos make this wholesome threat to Hannibal King.

Danica basically threatens Hannibal to make him vampire again. And my first though was "nah, he can cure again", but then shit hits the fan with hunger and the little girl Zoe being there locked with him in room. Ou yeah, that is good threat, totally doable and would hit really hard - my threat rating 10 out of 10. Well done whoever thought of that one.

Unfortunately the only memorable moment for me.
```

