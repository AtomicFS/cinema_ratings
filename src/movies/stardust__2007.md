{{#template
	../templates/movie.md
	PUB_DATE=Sun, 3 Apr 2022 18:55:00 UTC
	MOVIE_NAME=Stardust (2007)
	MOVIE_URL=https://www.imdb.com/title/tt0486655
	MOVIE_IMG=stardust__2007.jpg
	PLOT=In a countryside town bordering on a magical land, a young man makes a promise to his beloved that he'll retrieve a fallen star by venturing into the magical realm. His journey takes him into a world beyond his wildest dreams and reveals his true identity.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Simply said, I love it.

Visuals are great (although that poorly made CGI double-headed elephant from beginning hunts me), but besides that misstep the visuals are superb, music is just magical and story is charming and relatable.

As promised in the initial narration, the story is about growing up from a boy into man, and it delivered. There might be some things here and there that I would change (for example the elephant), but none of them are critical, none of them break it immersion. There are even some subtle jokes here and there that are difficult to spot, but make you chuckle once you spot them.

