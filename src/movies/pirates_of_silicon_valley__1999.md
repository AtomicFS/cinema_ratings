{{#template
	../templates/movie.md
	PUB_DATE=Sat, 23 Jul 2022 19:43:58 UTC
	MOVIE_NAME=Pirates of Silicon Valley (1999)
	MOVIE_URL=https://www.imdb.com/title/tt0168122
	MOVIE_IMG=pirates_of_silicon_valley__1999.jpg
	PLOT=The story about the men who made the world of technology what it is today, their struggles during college, the founding of their companies, and the ingenious actions they took to build up the global corporate empires of Apple Computer Inc. and Microsoft Corporation.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=N/A
	STORY_SMART=N/A
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Good movie about beginnings of Apple and Microsoft, certainly very interesting.

Both Apple's and Microsoft's stories are told in parallel, which is a bit confusing sometimes which story is which.

Overall, I recommend it, good watch.

