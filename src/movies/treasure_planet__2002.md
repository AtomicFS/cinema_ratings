{{#template
	../templates/movie.md
	PUB_DATE=Sat, 26 Mar 2022 10:50:00 UTC
	MOVIE_NAME=Treasure Planet (2002)
	MOVIE_URL=https://www.imdb.com/title/tt0133240
	MOVIE_IMG=treasure_planet__2002.jpg
	PLOT=A Disney animated version of "Treasure Island". The only difference is that this movie is set in outer space with alien worlds and other galactic wonders.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Simply said, I love this movie.

Music is perfect, story is great, characters are well written - also each of them undergoes character development over the course of the story! I would not change a thing about it. In my opinion, it is a masterpiece.

Sometimes, the visuals are lacking a bit, especially when 3D models are used, but given the age, it still holds up well.

Sure, some might argue that there are plot-holes, that it is not scientifically correct, or that there are some annoying characters. But I consider none of that valid point. Sure it might lack in science department, but why not? It supports the story, it feels well crafted and the rules of this strange universe are consistent - what more to ask?

My recommendation, do not look at any trailers, do not read about it, just go and watch it.

**Nerd warning**: This movie is not well known and there are reasons for it. Disney did not want to make this movie, but they were kinda forced to. At this point, I would recommend you to look this up if you are interested. But long story short, Disney purposefully buried this movie - little to no advertisement, and released trailers managed in 30 seconds to spoil the entirety of 1 hour 30 minute long story.

```admonish warning collapsible=true title="SPOILER WARNING"
When you observe closely, practically all characters character development over the course of the movie - how awesome is that? This is unfortunately not common occurrence.

As for the main villain? In my opinion one of, if not the best villains Disney created. This guy is not evil per say, only follows his dream, although goes to extremes trying to achieve it. This guy is relatable and he is understandable - you can understand his actions and they make sense. And at the end, he realizes that he blindly followed hollow dream, and decides to abandon it! Sure, he is not 100% converted by the movie end, but you get the feeling he just started a new chapter in his life.

And the main character - Jim? We get a story of young boy who loves his mother and seeks love from his father, who then abandons him at young age. And this feeling of abandonment resonates with him deeply, throwing him into a spiral of isolation and self-depriving thoughts - he believes there is no future for him and that he can't do anything right. So this immediately make this guy very relatable to many people - they have been in similar situation.

And then you get the whole story arch about Jim without father, and John the cyborg without son. In many ways you can see that they are very similar to each other - if Jim would continue on his current path, he would probably end up just like John. As they spend more and more time with each other, they form a bond, which is then tested and broken by circumstances, only to be mended by the end.

At the end, Jim starts to believe in himself and charts for himself a future he wants, while John realizes that there are more important things than material wealth.
```

As I already said, well written characters, great story, perfect music to go with it. Perfection incarnated. Wouldn't change a thing.

