{{#template
	../templates/movie.md
	PUB_DATE=Sun, 26 Feb 2023 19:57:46 UTC
	MOVIE_NAME=Bionicle 2: Legends of Metru Nui (2004)
	MOVIE_URL=https://www.imdb.com/title/tt0387658
	MOVIE_IMG=bionicle_2_legends_of_metru_nui__2004.jpg
	PLOT=In the time before time, six unlikely Matoran find themselves transformed into mighty Toa. The new Toa Metru must prove themselves as true heroes, find the missing Toa Lhikan, and uncover a plot that threatens the heart of Metru Nui.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=0
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Infantile humor, weak story and disporting story-telling. The main villain is a bit better, but still rather shape-less and taste-less.

While improvement over the previous movie [Bionicle: Mask of Light (2003)](bionicle_mask_of_light__2003.md), still rather disappointment.

