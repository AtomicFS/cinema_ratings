{{#template
	../templates/movie.md
	PUB_DATE=Sun, 10 Jul 2022 13:45:54 UTC
	MOVIE_NAME=The Big Short (2015)
	MOVIE_URL=https://www.imdb.com/title/tt1596363
	MOVIE_IMG=the_big_short__2015.jpg
	PLOT=The men who made millions from a global economic meltdown.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=N/A
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


The story is about the housing crisis in 2008 which shook the whole world. Since it is about real-life events which cause a lot of people their homes and livelihoods, it is a bit difficult to rate, especially the story-side.

It is interesting to watch the different stories of people who see what is coming and nobody believes them. However it shows how the banking world is greedy and corrupt, which is rather unsettling. I mean, we all probably know it, but never have seen it - and this will show you. As they say in American TV: "viewer discretion is advised", you might loose faith in humanity if you had some.

Overall, I would recommend this to everyone with advice: ask questions, don't be afraid to say no and most importantly don't invest into something you do not understand.

