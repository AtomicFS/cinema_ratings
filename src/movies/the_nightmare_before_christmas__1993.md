{{#template
	../templates/movie.md
	PUB_DATE=Fri, 12 May 2023 18:50:46 UTC
	MOVIE_NAME=The Nightmare before Christmas (1993)
	MOVIE_URL=https://www.imdb.com/title/tt0107688
	MOVIE_IMG=the_nightmare_before_christmas__1993.jpg
	PLOT=Tired of scaring humans every October 31 with the same old bag of tricks, Jack Skellington, the spindly king of Halloween Town, kidnaps Santa Claus and plans to deliver shrunken heads and other ghoulish gifts to children on Christmas morning. But as Christmas approaches, Jack's rag-doll girlfriend, Sally, tries to foil his misguided plans.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes
	
	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Thanks to the stop-motion, it aged quite well. Still after the years, it has good visuals. No complains there. Audio and songs are also good. Nice and original story.

Not sure if I would recommend it, depends if the person likes musicals.

Overall, it is a good watch.

