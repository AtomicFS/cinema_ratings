{{#template
	../templates/movie.md
	PUB_DATE=Mon, 16 Oct 2023 11:28:11 UTC
	MOVIE_NAME=The Monster (2016)
	MOVIE_URL=https://www.imdb.com/title/tt3976144
	MOVIE_IMG=the_monster__2016.jpg
	PLOT=A mother and her 10-year old daughter are trapped in a forest. There is something in this forest. Something unlike anything they have heard before. Something that lurks in the darkness and it’s coming after them.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=Yes
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Quite a watch this one.

I do not like the monster. First of all, it gets shown way too soon. I am always disappointed when the monster is shown, it is almost always a letdown. Secondly, it just behaves weirdly. I get a feeling it is just playing with its "food" for a horror movie sake, it feels unrealistic.

Personally, I would have preferred some more realistic monster, for example wild-life (wolf) with rabies or some other disease. This made-up monster breaks the immersion for me.

Lets say wolf with rabies - what would make it scary for me would be the unpredictability. The disease causes, among other things, altered consciousness, abnormal behavior, hallucinations, paranoia and terror. The combination of unpredictable behavior and deadliness of such animal (not only by physical force, but also by infection) would be indeed terrifying.

Besides the monster factor, I like the deep dive into the complicated relationship between mother and daughter. That, combined with good visual story-telling and subtle sound-track make for a decent watch.

```admonish warning collapsible=true title="SPOILER WARNING"
As for the ending, I am not sure about the deadliness of the "flame-thrower". Maybe if the fire got into the monsters lungs, it might have caused it to suffocate do to burn damage in the lungs (since they are very delicate). So I would put it into unlikely, but plausible category.
```

