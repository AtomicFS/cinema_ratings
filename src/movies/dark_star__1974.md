{{#template
	../templates/movie.md
	PUB_DATE=Sat, 19 Feb 2022 14:41:00 UTC
	MOVIE_NAME=Dark Star (1974)
	MOVIE_URL=https://www.imdb.com/title/tt0069945
	MOVIE_IMG=dark_star__1974.jpg
	PLOT=A low-budget, sci-fi satire that focuses on a group of scientists whose mission is to destroy unstable planets. 20 years into their mission, they have battle their alien mascot, that resembles a beach ball, as well as a "sensitive" and intelligent bombing device that starts to question the meaning of its existence.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=0
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


First of all, the pace of the movie is very slow, it would cut to half by today's standards. Although, it does serve it's purpose by re-creating the atmosphere of boring isolation in deep space.

Overall, not bad, I do not regret seeing it, but I don't think I would watch it again.


```admonish warning collapsible=true title="SPOILER WARNING"
The best scene is with the intelligent self-aware bomb which is about to explode, and the crew trying to disarm it. Besides this, it feels like there is not much else to offer.
```

