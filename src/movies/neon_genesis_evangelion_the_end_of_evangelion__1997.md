{{#template
	../templates/movie.md
	PUB_DATE=Tue, 01 Nov 2022 18:35:22 UTC
	MOVIE_NAME=Neon Genesis Evangelion: The End of Evangelion (1997)
	MOVIE_URL=https://www.imdb.com/title/tt0169858
	MOVIE_IMG=neon_genesis_evangelion_the_end_of_evangelion__1997.jpg
	PLOT=The second of two theatrically released follow-ups to the Neon Genesis Evangelion series. Comprising of two alternate episodes which were first intended to take the place of episodes 25 and 26, this finale answers many of the questions surrounding the series, while also opening up some new possibilities.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=4
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=Yes

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Most of my thoughts are noted int he show [Neon Genesis Evangelion (1995)](../shows/neon_genesis_evangelion__1995.md), since this movie is more-less just replacement ending for the show.

