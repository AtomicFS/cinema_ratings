{{#template
	../templates/movie.md
	PUB_DATE=Sat, 13 May 2023 09:32:39 UTC
	MOVIE_NAME=Galaxy Quest (1999)
	MOVIE_URL=https://www.imdb.com/title/tt0177789
	MOVIE_IMG=galaxy_quest__1999.jpg
	PLOT=For four years, the courageous crew of the NSEA protector - Commander Peter Quincy Taggart (Tim Allen), Lt. Tawny Madison (Sigourney Weaver) and Dr.Lazarus (Alan Rickman) - set off on a thrilling and often dangerous mission in space...and then their series was cancelled! Now, twenty years later, aliens under attack have mistaken the Galaxy Quest television transmissions for historical documents and beam up the crew of has-been actors to save the universe. With no script, no director and no clue, the actors must turn in the performances of their lives.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes
	
	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=0
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Funny, silly, self-aware parody of Star Trek and it's fans.

Overall, decent.

