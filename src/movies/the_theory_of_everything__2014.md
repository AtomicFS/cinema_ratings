{{#template
	../templates/movie.md
	PUB_DATE=Fri, 12 May 2023 12:18:00 UTC
	MOVIE_NAME=The Theory of Everything (2014)
	MOVIE_URL=https://www.imdb.com/title/tt2980516
	MOVIE_IMG=the_theory_of_everything__2014.jpg
	PLOT=The Theory of Everything is the extraordinary story of one of the world’s greatest living minds, the renowned astrophysicist Stephen Hawking, who falls deeply in love with fellow Cambridge student Jane Wilde.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes
	
	STORY_SENSIBLE=N/A
	STORY_SMART=N/A
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I can't speak for the accuracy of the story, since I do not know that much about Stephen Hawking, but the movies seems good.

Well made visuals, good soundtrack, good story-telling and well paced.
{{#template
	../templates/movie.md
	PUB_DATE=Fri, 12 May 2023 12:18:00 UTC
	MOVIE_NAME=The Theory of Everything (2014)
	MOVIE_URL=https://www.imdb.com/title/tt2980516
	MOVIE_IMG=the_theory_of_everything__2014.jpg
	PLOT=The Theory of Everything is the extraordinary story of one of the world’s greatest living minds, the renowned astrophysicist Stephen Hawking, who falls deeply in love with fellow Cambridge student Jane Wilde.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes
	
	STORY_SENSIBLE=N/A
	STORY_SMART=N/A
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I can't speak for the accuracy of the story, since I do not know that much about Stephen Hawking, but the movies seems good. Well made visuals, good soundtrack, good story-telling.

Would recommend.


Would recommend.

