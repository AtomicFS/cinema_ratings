{{#template
	../templates/movie.md
	PUB_DATE=Wed, 01 Mar 2023 17:10:53 UTC
	MOVIE_NAME=Unfriended: Dark Web (2018)
	MOVIE_URL=https://www.imdb.com/title/tt4761916
	MOVIE_IMG=unfriended_dark_web__2018.jpg
	PLOT=When a 20-something finds a cache of hidden files on his new laptop, he and his friends are unwittingly thrust into the depths of the dark web. They soon discover someone has been watching their every move and will go to unimaginable lengths to protect the dark web.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


This is a really good movie. I like the unique story-telling and I like the story.

I might have one or two technical issues, but only minor ones. Really good watch.

```admonish warning collapsible=true title="SPOILER WARNING"
I think that the girl with the hole in her head at the end was a bit over the top. In my opinion, it did not add much to the story nor the feel.
```

