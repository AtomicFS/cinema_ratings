{{#template
	../templates/movie.md
	PUB_DATE=Tue, 02 May 2023 23:19:46 UTC
	MOVIE_NAME=Short Circuit 2 (1988)
	MOVIE_URL=https://www.imdb.com/title/tt0096101
	MOVIE_IMG=short_circuit_2__1988.jpg
	PLOT=Robot Johnny 5 moves to the city to help his friend Ben Jahrvi with his toy manufacturing enterprise, only to be manipulated by criminals who want to use him for their own nefarious purposes.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=No
	STORY_SMART=No
	STORY_ENDING=No

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Well, compare to the first movie, this one is quite a disappointment. It no longer has this originality to it, feels just like cheap way to milk the success.

