{{#template
	../templates/movie.md
	PUB_DATE=Mon, 29 May 2023 16:56:59 UTC
	MOVIE_NAME=Thanks for Sharing (2012)
	MOVIE_URL=https://www.imdb.com/title/tt1932718
	MOVIE_IMG=thanks_for_sharing__2012.jpg
	PLOT=A romantic comedy that brings together three disparate characters who are learning to face a challenging and often confusing world as they struggle together against a common demon-sex addiction.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=Yes
	EROTIC=Yes
	SEX=Yes

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}

This one is a bit odd to say the least. It is about addiction (mostly sex addiction) and a lot of emotional issues. Get ready for emotional roller-coaster and be warned that this movie does get really fucked up towards the end, but it has a good ending.

Not sure if I would recommend it to anyone, but I would watch it again. In a way, it does have therapeutic effect, but your mileage might vary depending on your own emotional baggage.

My message to the creators and actors in the movie: Thank you.


```admonish warning collapsible=true title="SPOILER WARNING"
It does make me feel really good to see Dede and Neil to have non-sexual relationship, pure friendship. It is just great contrast to the main theme and really ties it all together.
```

