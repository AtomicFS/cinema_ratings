{{#template
	../templates/movie.md
	PUB_DATE=Fri, 04 Nov 2022 14:17:08 UTC
	MOVIE_NAME=Evangelion: 2.0 You Can (Not) Advance (2009)
	MOVIE_URL=https://www.imdb.com/title/tt0860906
	MOVIE_IMG=evangelion_222_you_can_not_advance__2009.jpg
	PLOT=Under constant attack by monstrous creatures called Angels that seek to eradicate humankind, U.N. Special Agency NERV introduces two new EVA pilots to help defend the city of Tokyo-3: the mysterious Makinami Mari Illustrous and the intense Asuka Langley Shikinami. Meanwhile, Gendo Ikari and SEELE proceed with a secret project that involves both Rei and Shinji.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=4
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Home release
	SPOKEN_LANG=ENG
}}


{{#include ../notes/neon_genesis_evangelion.md}}

I cannot decide whether or not to give it `fucked up` rating 3 or 4. Since right now I am re-watching the entirety of Evangelion franchise, my sensitivity to weirdness and messed up stuff is lowered significantly. It feels weaker compared to the original show [Neon Genesis Evangelion (1995)](../shows/neon_genesis_evangelion__1995.md), but standalone it can do 4.

Not sure if I like this one or not. For the most part, I would say it is on par with the previous film [Evangelion: 1.0 You Are (Not) Alone (2007)](evangelion_111_you_are_not_alone__2007.md). It is a good continuation, but it did not make me change my opinion of this Evangelion rebuild - not a big fan.

