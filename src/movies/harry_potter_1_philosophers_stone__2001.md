{{#template
	../templates/movie.md
	PUB_DATE=Thu, 16 Feb 2023 11:57:46 UTC
	MOVIE_NAME=Harry Potter 1: Philosophers Stone (2001)
	MOVIE_URL=https://www.imdb.com/title/tt0241527
	MOVIE_IMG=harry_potter_1_philosophers_stone__2001.jpg
	PLOT=Harry Potter has lived under the stairs at his aunt and uncle's house his whole life. But on his 11th birthday, he learns he's a powerful wizard -- with a place waiting for him at the Hogwarts School of Witchcraft and Wizardry. As he learns to harness his newfound powers with the help of the school's kindly headmaster, Harry uncovers the truth about his parents' deaths -- and about the villain who's to blame.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=No
	STORY_ENDING=No

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Extended
	SPOKEN_LANG=ENG
}}


I dare to say that most of the special effects are top notch, but there are few which look really dated.

Seeing it now, with adult eyes, I must say that the story is a disappointing. Quite a few things make no sense - see the spoiler section.

Overall, the movie feels more like a prototype with rather crude and under-cooked bits and pieces here and there. Back in the day when the movie was released, it certainly was a spectacle, but nowadays you can see that cracks.

Good music, good actors, often good special effects, nice environment. Story could use more work.

If anything I recommend to watch the extended version.

```admonish warning collapsible=true title="SPOILER WARNING"

**Quidditch**

I am sorry to say this straight up, but rules of Quidditch are garbage.

- 10 points for throwing a ball though hoop, OK no problem co far

- 150 points for catching Golden Snitch and ending the game?
- This is utter rubbish. Catching the Snitch is basically instant victory with no balance.
- It would be far more interesting to get maybe 30 points and ending the game. Then you would have to switch between catching and protecting the snitch based on your team's points.
- And then there is the violence that nobody seems to care about. Students are in grave danger!

**Mirror of Erised**

Dumbledore said:
> It shows us nothing more or less than the deepest and most desperate desires of our hearts.

And then he also says this:
> This mirror gives us neither knowledge or truth

That is so stupid. It gives us truth about ourselves, and that is very important. I know so many people who have no idea what they want. This thing could be used in therapy!

**Miscellaneous**

- Giving points for breaking school rules? Really?

- Gulping - just fuck off, this is not a cartoon. Sorry, but it pisses me off every time it happens.
- For being up late after hours, you get to go into the forbidden forest at night? Basically breaking more school rules? What the fuck?
- Hermione is smarter than teachers, or rather teachers are that much incompetent.
- The whole protection of the Philosophers stone is inadequate. Who on earth though that big dog, plant, few flying keys and game of chess are enough?
- At the end Dumbledore decides to announce the years winners, and then proceeds to change the results. What the actual fuck? If I were the kid there, I would feel like the game was rigged, and rightfully so.

**Summary**

- Teachers are utterly incompetent
- Harry by trying to help makes things worse
	- Voldemort was just a inches from the stone thanks to Harry
- Then the Deus ex Machina with magical touch thing ...

```

