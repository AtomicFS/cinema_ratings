{{#template
	../templates/movie.md
	PUB_DATE=Sat, 23 Jul 2022 19:55:15 UTC
	MOVIE_NAME=Untraceable (2008)
	MOVIE_URL=https://www.imdb.com/title/tt0880578
	MOVIE_IMG=untraceable__2008.jpg
	PLOT=Special Agent Jennifer Marsh works in an elite division of the FBI dedicated to fighting cybercrime. She thinks she has seen it all, until a particularly sadistic criminal arises on the Internet. This tech-savvy killer posts live feeds of his crimes on his website; the more hits the site gets, the faster the victim dies. Marsh and her team must find the elusive killer before time runs out.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


This is one of the best hacker-themed movies I have seen so far.

They are not afraid to talk IT a bit, but without [technobabble](https://www.urbandictionary.com/define.php?term=technobabble) (at least I did not notice any - all terms seemed to be correctly). There is also rather interesting story-arch with commentary on today's society.

Overall, really well done movie about computers and hackers.

