{{#template
	../templates/movie.md
	PUB_DATE=Tue, 28 Feb 2023 21:16:01 UTC
	MOVIE_NAME=The Secret History of Hacking (2001)
	MOVIE_URL=https://www.imdb.com/title/tt2335921
	MOVIE_IMG=the_secret_history_of_hacking__2001.jpg
	PLOT=The Secret History of Hacking is a 2001 documentary film that focuses on phreaking, computer hacking and social engineering occurring from the 1970s through to the 1990s. Archive footage concerning the subject matter and (computer generated) graphical imagery specifically created for the film are voiced over with narrative audio commentary, intermixed with commentary from people who in one way or another have been closely involved in these matters.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=N/A
	STORY_SMART=N/A
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=N/A

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=0
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Very nice documentary, starting with phone phreaking ending with personal computer revolution.

I really like following about the home-brew club:
- It was a phenomenal information exchange.
- It was set as hacker enterprise with total sharing of information.
	- The "real" engineers would be blocked by this need to "get permission to do this".
	- The hackers were going there because they knew no permission is needed.
- Nobody had any real reason to have a personal computer, though few excuses were made up.

Thanks to these people, we have the technology that we have nowadays.

