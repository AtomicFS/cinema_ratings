{{#template
	../templates/movie.md
	PUB_DATE=Fri, 04 Nov 2022 14:17:20 UTC
	MOVIE_NAME=Evangelion: 3.0 You Can (Not) Redo (2012)
	MOVIE_URL=https://www.imdb.com/title/tt0860907
	MOVIE_IMG=evangelion_333_you_can_not_redo__2012.jpg
	PLOT=Fourteen years after Third Impact, Shinji Ikari awakens to a world he does not remember. He hasn't aged. Much of Earth is laid in ruins, NERV has been dismantled, and people who he once protected have turned against him. Befriending the enigmatic Kaworu Nagisa, Shinji continues the fight against the angels and realizes the fighting is far from over, even when it could be against his former allies. The characters' struggles continue amidst the battles against the angels and each other, spiraling down to what could inevitably be the end of the world.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=No
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=4
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Home release
	SPOKEN_LANG=ENG
}}


{{#include ../notes/neon_genesis_evangelion.md}}

Far too much 3D animation kinda does not feel right in the rest of show.

As for the story, I am completely lost. The original show [Neon Genesis Evangelion (1995)](../shows/neon_genesis_evangelion__1995.md) had at least order to the insanity, but this one feels like a complete mess. I can't even relate to the characters properly anymore. Not my cup of tee.

