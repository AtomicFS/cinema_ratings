{{#template
	../templates/movie.md
	PUB_DATE=Tue, 28 Feb 2023 19:23:49 UTC
	MOVIE_NAME=Takedown (2000)
	MOVIE_URL=https://www.imdb.com/title/tt0159784
	MOVIE_IMG=takedown__2000.jpg
	PLOT=Kevin Mitnick is quite possibly the best hacker in the world. Hunting for more and more information, seeking more and more cyber-trophies every day, he constantly looks for bigger challenges. When he breaks into the computer of a security expert and an ex-hacker, he finds one - and much more than that ...

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=N/A
	STORY_SMART=N/A
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=Yes
	SEX=Yes

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


The `Adapted from a true story` is kinda lie. This is about Kevin Mitnick, with no input from Kevin Mitnick. I highly recommend to watch a documentary [Freedom Downtime (2001)](freedom_downtime__2001.md) along with followup interview with Kevin Mitnick (probably watch it before this movie).

With all of that being said, I very much dislike the fact that they use real names in so highly inaccurate depiction of the events. It is based on John Markoff claims, which are widely unfounded. Look into the spoiler section for deeper analysis and fact-checking.

Overall, the movie is just anti-hacker meh.


```admonish warning collapsible=true title="SPOILER WARNING"

[From wikipedia](https://en.wikipedia.org/wiki/Track_Down#Factual_inaccuracies):
- In Kevin Mitnick's The Art of Deception, Mitnick states that both book and movie are "extremely inaccurate" and based on media hype.
- In the film, Mitnick and Shimomura meet twice; one of these meetings prompts Kevin to flee to Seattle. This meeting did not actually take place.
- The film depicts Mitnick hacking into Shimomura's computers and stealing/deleting his files and software. Though Mitnick admits hacking Shimomura's computers using IP spoofing, he claims he never caused any damage to anyone by deleting files or data, merely copying source code of some software, out of curiosity.
- The film also shows Mitnick hacking NORAD, the NSA and other famous government institutes, which never in fact happened.
- The 2001 documentary [Freedom Downtime](freedom_downtime__2001.md) tries to get behind some of the false rumors about Kevin Mitnick that ended up being presented as facts in the film. 

[Nice comment from IMDB.com](https://www.imdb.com/review/rw0560764/) which summarizes it quite nicely:
> This was a movie that deserved to tank. Kevin Mitnick, a genius with computers who was a little too inquisitive for the authorities liking, has been the victim of so many abuses that it can make one's stomach turn. "Takedown" was adapted from a book written by John Markoff and Tsutomu Shimomura who exploited Kevin, a man about whom neither of the authors had any direct knowledge, and pretended to be Mitnick experts when in fact they couldn't have been more clueless.
> 
> Markoff, a hack journalist who did everything that he could to portray Kevin a danger to society in order to keep writing articles about him, has claimed wild rumors about Mitnick to be fact (rumors such as Kevin hacking into NORAD computers, harassing Christie McNickle, and converting home phones into pay phones) with no regard for the fact that he was demonizing Kevin in the eyes of society and in the eyes of a justice system - a system that would eventually lock Kevin in solitary confinement for 8 months because they were afraid he would use prison phones to launch nuclear missiles if placed in general population. Tsutomu Shimomura is nothing but a smart-ass hacker wanna-be whose main contribution to the book "Takedown" was a list of his skateboarding and eating habits.

```

