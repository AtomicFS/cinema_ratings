{{#template
	../templates/movie.md
	PUB_DATE=Sat, 14 May 2022 08:54:21 UTC
	MOVIE_NAME=Chronicle (2012)
	MOVIE_URL=https://www.imdb.com/title/tt1706593
	MOVIE_IMG=chronicle__2012.jpg
	PLOT=Three high school students make an incredible discovery, leading to their developing uncanny powers beyond their understanding. As they learn to control their abilities and use them to their advantage, their lives start to spin out of control, and their darker sides begin to take over.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Movie is made more-less in found-footage style, which is quite fitting and in my opinion help's the story-telling.

As for the story, intriguing with some sensitive topics.

Overall quite interesting look into how different people would handle getting a super powers.


```admonish warning collapsible=true title="SPOILER WARNING"
Rather interesting to see what happens with Matt and Andrew. Matt starts as this philosopher contemplating existence and how nothing matters (nihilism), but over time finds his calling and purpose. Basically it turns his life around, finds a desire to change the world and help people.

Andrew however starts as depressed and isolated guy which only deepens over time. He slowly adopts Matt's initial nihilistic views and distances himself even further. And as Andrew looses the only thing that tethers him to the world (his mother), he just looses it completely, going into breakdown.
```

