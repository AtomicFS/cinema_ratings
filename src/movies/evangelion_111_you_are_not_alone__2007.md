{{#template
	../templates/movie.md
	PUB_DATE=Thu, 03 Nov 2022 18:24:51 UTC
	MOVIE_NAME=Evangelion: 1.0 You Are (Not) Alone (2007)
	MOVIE_URL=https://www.imdb.com/title/tt0923811
	MOVIE_IMG=evangelion_111_you_are_not_alone__2007.jpg
	PLOT=After the Second Impact, Tokyo-3 is being attacked by giant monsters called Angels that seek to eradicate humankind. The child Shinji's objective is to fight the Angels by piloting one of the mysterious Evangelion mecha units. A remake of the first six episodes of GAINAX's famous 1996 anime series. The film was retitled "Evangelion: 1.01"; for its home video version and "Evangelion: 1.11"; for a release with additional scenes.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=3
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Home release
	SPOKEN_LANG=ENG
}}


{{#include ../notes/neon_genesis_evangelion.md}}

Well, it feels like inadequate shortened version of the original show, and you can see some differences in the story.

The original show slowly builds the story and the world around step by step, but this remake just throws stuff at you in quick succession with no buildup. If this was my first ever experience with `Evangelion` franchise, I would be completely lost, but not in a good way like with the show. In the original show it is done so that it tackles your curiosity, it builds up a mystery. Here it is just slap into face. But difficult to say for sure since I have seen the original show and I already know the backstory.

Regarding the visuals, they do have more details and probably even higher budget than the original show [Neon Genesis Evangelion (1995)](../shows/neon_genesis_evangelion__1995.md), but sometime the use of 3D animation is a bit off. Nevertheless the it is good, although I do prefer the original a little bit more. But I must say that the battle with final angel of this movie is eye-candy.

In short, I am not a big fan of this sequel / remake / reboot. Maybe the sequel [Evangelion: 2.0 You Can (Not) Advance (2009)](evangelion_222_you_can_not_advance__2009.md) will be better.

To make some sence to this, maybe consider these films sequels - see spoiler section.


```admonish warning collapsible=true title="SPOILER WARNING"
There are hints here and there that the universe of Evangelion is repeating itself, in some sort of cycle. Kaworu seems to be aware of this, and with this in mind it makes a lot of sense to what he says.
```

