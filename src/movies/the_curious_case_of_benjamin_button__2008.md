{{#template
	../templates/movie.md
	PUB_DATE=Wed, 10 May 2023 16:46:55 UTC
	MOVIE_NAME=The Curious Case of Benjamin Button (2008)
	MOVIE_URL=https://www.imdb.com/title/tt0421715
	MOVIE_IMG=the_curious_case_of_benjamin_button__2008.jpg
	PLOT=I was born under unusual circumstances. And so begins The Curious Case of Benjamin Button, adapted from the 1920s story by F. Scott Fitzgerald about a man who is born in his eighties and ages backwards: a man, like any of us, who is unable to stop time. We follow his story, set in New Orleans, from the end of World War I in 1918 into the 21st century, following his journey that is as unusual as any man's life can be. Benjamin Button, is a grand tale of a not-so-ordinary man and the people and places he discovers along the way, the loves he finds, the joys of life and the sadness of death, and what lasts beyond time.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe
	
	STORY_SENSIBLE=No
	STORY_SMART=No
	STORY_ENDING=No

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Maybe
	SEX=Yes

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


CGI is not good. Story make little to no sense, especially the first quarter. A lot of over-sexualised rubbish.

I do not like it one bit. There might have been some potential.

Overall, nah.


```admonish warning collapsible=true title="SPOILER WARNING"
How the fuck did the submarine explode? Does not make sense at all. It was hit somewhere in the middle, so there was no way to hit ammunition (usually at the front) or fuel tanks (at the back).

Giving birth at home? WTF?

Start looking for another father? She needs father and not playmate? What is this? Is he assuming he will go into puberty, and eventually shrink down to baby? Doesn't really make any sense. And then he leaves?

What do they mean by raising both of them? Mentally this guy is an adult, he does not need raising? Or is he loosing his abilities and mental capacity? What the fuck is going on?!

Sure, he eventually actually reverts all the way back and looses mental capacity and memories, but there are no hints, nothing. That is what annoys me the most.
```

