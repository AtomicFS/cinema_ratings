{{#template
	../templates/movie.md
	PUB_DATE=Sat, 19 Feb 2022 15:14:00 UTC
	MOVIE_NAME=Blackhat (2015)
	MOVIE_URL=https://www.imdb.com/title/tt2717822
	MOVIE_IMG=blackhat__2015.jpg
	PLOT=A man is released from prison to help American and Chinese authorities pursue a mysterious cyber criminal. The dangerous search leads them from Chicago to Hong Kong.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Maybe
	STORY_SMART=No
	STORY_ENDING=No

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=Maybe
	EROTIC=Yes
	SEX=Yes

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Barely watchable, full of Hollywood crap. Mix between crime and romantic movie. And of course they involve [the 9/11](https://simple.wikipedia.org/wiki/September_11_attacks) like there was not enough if it already.

