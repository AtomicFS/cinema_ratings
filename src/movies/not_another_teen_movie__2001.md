{{#template
	../templates/movie.md
	PUB_DATE=Sat, 01 Oct 2022 18:16:46 UTC
	MOVIE_NAME=Not Another Teen Movie (2001)
	MOVIE_URL=https://www.imdb.com/title/tt0277371
	MOVIE_IMG=not_another_teen_movie__2001.jpg
	PLOT=On a bet, a gridiron hero at John Hughes High School sets out to turn a bespectacled plain Jane into a beautiful and popular prom queen in this outrageous send-up of the teen movies of the 1980s and '90s.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=No
	STORY_SMART=N/A
	STORY_ENDING=N/A

	FEELING_MEMORABLE=No
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=No
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=0
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=Maybe
	DRUGS=Maybe
	EROTIC=Yes
	SEX=Yes

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I have managed to watch only 15 or 20 minutes, then I just turned it off and walked away thinking to myself "who made this and why?".

I do not like the story, I dislike the acting, I hate the juvenile jokes ... Sure, some migth find it interesting, and maybe even funny, but I am not one of those people.

Overall: no thanks, refund please.

