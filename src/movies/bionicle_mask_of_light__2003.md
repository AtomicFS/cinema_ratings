{{#template
	../templates/movie.md
	PUB_DATE=Sun, 26 Feb 2023 17:58:54 UTC
	MOVIE_NAME=Bionicle: Mask of Light (2003)
	MOVIE_URL=https://www.imdb.com/title/tt0369281
	MOVIE_IMG=bionicle_mask_of_light__2003.jpg
	PLOT=With darkness encroaching on the island of Mata Nui, the only hope for the survival of the islanders rests with two Matoran villagers, who must find the seventh Toa and deliver to him the fabled Mask of Light.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Maybe
	STORY_SMART=No
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=0
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Right from the start a lot of unknown words, locations and names, which is quote confusing and overwhelming.

The age shows a lot. Textures are low-poly, often textures pass thought each other. From today's standards, it looks rather like cut-scenes from old video-game rather than movie.

Not bad sound-track, but lacking story. Reminds me a bit of [Lord of the Rings](the_lord_of_the_rings__2001.md) with both the story and music.

It was created to promote LEGO's new Bionicle toy, [the toy that saved LEGO](https://www.youtube.com/watch?v=126bLUkHuNI).

Overall, meh.

