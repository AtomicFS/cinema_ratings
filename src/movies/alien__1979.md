{{#template
	../templates/movie.md
	PUB_DATE=Sat, 19 Feb 2022 21:38:00 UTC
	MOVIE_NAME=Alien (1979)
	MOVIE_URL=https://www.imdb.com/title/tt0078748
	MOVIE_IMG=alien__1979.jpg
	PLOT=During its return to the earth, commercial spaceship Nostromo intercepts a distress signal from a distant planet. When a three-member team of the crew discovers a chamber containing thousands of eggs on the planet, a creature inside one of the eggs attacks an explorer. The entire crew is unaware of the impending nightmare set to descend upon them when the alien parasite planted inside its unfortunate host is birthed.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


This movie is legendary and deservingly so.

The concept is rather simple: people are stranded on ship in deep space with alien trying to kill them, nothing more and nothing less. The music is great, the story simple but effective, characters are acting realistically and don't make ridiculously stupid decisions.

Even though it is very old movie, the visual effects are still respectable and are holding up to even today's standards. I suppose that there are some things that might be improved, but there are all minor and not many.

