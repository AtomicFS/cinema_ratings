{{#template
	../templates/movie.md
	PUB_DATE=Sat, 09 Jul 2022 18:21:11 UTC
	MOVIE_NAME=The Invasion (2007)
	MOVIE_URL=https://www.imdb.com/title/tt0427392
	MOVIE_IMG=the_invasion__2007.jpg
	PLOT=Washington, D.C. psychologist Carol Bennell and her colleague Dr. Ben Driscoll are the only two people on Earth who are aware of an epidemic running rampant through the city. They discover an alien virus aboard a crashed space shuttle that transforms anyone who comes into contact with it into unfeeling drones while they sleep. Carol realizes her son holds the key to stopping the spread of the plague and she races to find him before it is too late.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=No

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


More-less a remake of Body Snatchers, with more focus on the global pandemic.

Action scenes are, as usual in Hollywood, just epilepsy-inducing blur, but thankfully there are not that many of them.

The ending is underwhelming. Just fast forward to end result is not good enough in my opinion.

Overall it is not bad, worth a watch.

