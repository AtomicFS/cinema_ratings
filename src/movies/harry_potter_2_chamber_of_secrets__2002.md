{{#template
	../templates/movie.md
	PUB_DATE=Thu, 16 Feb 2023 13:36:08 UTC
	MOVIE_NAME=Harry Potter 2: Chamber of Secrets (2002)
	MOVIE_URL=https://www.imdb.com/title/tt0295297
	MOVIE_IMG=harry_potter_2_chamber_of_secrets__2002.jpg
	PLOT=Cars fly, trees fight back, and a mysterious house-elf comes to warn Harry Potter at the start of his second year at Hogwarts. Adventure and danger await when bloody writing on a wall announces: The Chamber Of Secrets Has Been Opened. To save Hogwarts will require all of Harry, Ron and Hermione's magical abilities and courage.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=No
	STORY_SMART=No
	STORY_ENDING=No

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Extended
	SPOKEN_LANG=ENG
}}


Compared to the previous movie [Philosophers Stone](harry_potter_1_philosophers_stone__2001.md), it is a big improvement regarding the world building, however the story is somewhat worse and the ending is just terrible.

If anything I recommend to watch the extended version.

```admonish warning collapsible=true title="SPOILER WARNING"

The problem I have with this movie:

- When Harry uses the Floo powder and mispronounces the target location, I am amazed that nobody came in to rescue him? What the fuck?
	- When I used subway as a kid with my parents, I was instructed in case of problem to get out at the closes stop and wait there until they come for me.
	- FYI: In the book, the reason Harry lands in wrong place is not mispronunciation but other factors.
- How on earth could Dumbledore let Gilderoy Lockhart teach?
	- According to Wizarding World (formerly Pottermore) [Dumbledore hired Lockhart to expose him](https://screenrant.com/harry-potter-dumbledore-gilderoy-lockhart-teach-hogwarts-bad-explained/). It would be nice to get some explanation in the movie.
- When Harry and Ron miss the train, they take the car. Sure it is stupid, but they are teenagers after all, and they get scolded afterwards. Therefore I give it a pass.
- Here is just more proof that Quidditch is a violent game.
- The fight between the Basilisk and Harry is just not good, borderline bad.
- Dumbledore yet again encourages braking school rules by rewarding points.
- Right at the end, the light in face of Lucius Malfoy ... are you serious? It reminded me of Morticia Addams from movie Addams Family.
- Also at the end, did Dumbledore just cancel exams? What the fuck?
	- Just from older students point of view, you need exams to get a diploma and job. This could easily ruin your life. I know that diploma means very little in the real world, but without it you are fucked.

One good thing that stood out for me was this quote from Dumbledore:
> It is not our abilities that show what we truly are. It is our choices.

```

