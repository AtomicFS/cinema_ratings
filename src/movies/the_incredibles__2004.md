{{#template
	../templates/movie.md
	PUB_DATE=Mon, 18 Apr 2022 14:36:36 UTC
	MOVIE_NAME=The Incredibles (2004)
	MOVIE_URL=https://www.imdb.com/title/tt0317705
	MOVIE_IMG=the_incredibles__2004.jpg
	PLOT=Bob Parr has given up his superhero days to log in time as an insurance adjuster and raise his three children with his formerly heroic wife in suburbia. But when he receives a mysterious assignment, it's time to get back into costume.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I used to love this movie, and only upon recent re-watch I have realized how old it is. Wow. The visuals still look decent but you can clearly see the age. But it is still watchable. Some scenes are rather plane, but story is great and catchy. It also has a great villain and awesome ending.

The story is interesting and well written with plenty of humor. I like the portrayal of family of superheros trying to act normal, only to fail miserably. The family has many issues and it is good to see them all grow over the course of story and resolve most of their problems.

The basic premise is having the abilities to help, and being prohibited to. This is rather fresh perspective, since most of other movies or shows are about the exact opposite - not being able to help but wanting to.

