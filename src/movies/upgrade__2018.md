{{#template
	../templates/movie.md
	PUB_DATE=Mon, 18 Apr 2022 09:18:34 UTC
	MOVIE_NAME=Upgrade (2018)
	MOVIE_URL=https://www.imdb.com/title/tt6499752
	MOVIE_IMG=upgrade__2018.jpg
	PLOT=Set in the near-future, technology controls nearly all aspects of life. But when the world of Grey, a self-labeled technophobe, is turned upside down, his only hope for revenge is an experimental computer chip implant.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=3
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=Maybe
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Start was promising, however it took turn for the worse.

Visuals are gorgeous, sound-track is also good, the story is also somewhat decent, but the execution is garbage.

It feels like it is made entirely for looks, just slapping irrelevant stuff in there to fill each scene with bullshit. It is so unnatural, instead it should take a form-from-function when looks are secondary to function (even though the function is fictional). It is also full of medical and technical inaccuracies (I am no doctor and even I can tell). Sometimes a bit too obvious fore-shadowing for my taste. It also feels a bit too cheesy with one too many cliches.

Overall, the most prevailing feeling I am getting is disappointment. And given that the idea is great and the execution is half-arsed, I just feel disappointed. It leaves this weird after-taste. I can see how it could become a great science fiction movie, but instead it is this undercooked, half-arsed, unpolished, shapeless blob. Such a shame, such disappointment.

```admonish warning collapsible=true title="SPOILER WARNING"
Well, there are so many things to say ...

Here are some things I like:
- the whole premise of having machine take over your body
- the self-driving car is hacked and passengers are kidnapped followed by murder
- when STEM takes over, it nicely demonstrates the superiority of machines in certain fields, giving the movie and the idea of machines taking over even more umph
	- I also like the idea of being a puppet in your own body
- I like how STEM is slowly taking over, gaining trust and autonomy over time
- the moment when STEM is being hacked is kinda hilarious - main character scribbling all over his arm while running with his body slowly stopping to work

Here are things I don't like:
- kitchen robotic arms are under-utilized - you don't need expensive arm to pass drink
	- *It feels it is there just for looks, have it do something more than just squirt green goo into glass*
- the house is smart enough to have a decent conversation, yet it can't figure out that the wife is dead
	- *I can clearly see it is there to emphasize the loss, but it feels a bit off*
- technical mambo-jumbo with fancy words used out of place
	- *c'mon, get some consultants who know what they are talking about*
- the operating room with see-though glass which shows internals, bone and stuff looks cool, but it is useless - the doctors need to see this stuff, not some dude who is left outside the room
- the discussion about name / no-name and gender stuff
	- *It feels out of place, just random bleep with no context and no story significance*
- hand instead of shotgun, it feels a bit ridiculous, especially when you take into account the size of bullets the guy puts in and visual/sound of shooting them, not to mention the bullets loaded does not match bullets shot
	- *these scenes just feels like ordinary Hollywood stupid*
- the listening device is undetectable because it is analog?
	- *WTF? That is not how real world works. It is still a radio transmitter and as such, it is easily detectable.*
- car is not electronic?
	- *Another non-sense statement. All cars, even old ones, have electronics inside - lights, radio, electric windows and so on. The degree to which they are computerized is what matters, and whether or not they have wireless-capabilities.*

Meh:
- the hand-to-hand fight scenes are not too bad, at least they are not one big epilepsy-inducing blur
- when the STEM draws the tattoo based on 4 pixels on TV
	- *This is pure non-sense from technological point of view. However given the story and the ending, it does have a good explanation. But the thing is, since the movie is already so full of bullshit, it does not raise any red flags - which is missed opportunity. And I would say there are many more instances like this.*
```

