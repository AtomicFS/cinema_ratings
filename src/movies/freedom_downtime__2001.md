{{#template
	../templates/movie.md
	PUB_DATE=Tue, 28 Feb 2023 15:03:52 UTC
	MOVIE_NAME=Freedom Downtime (2001)
	MOVIE_URL=https://www.imdb.com/title/tt0309614
	MOVIE_IMG=freedom_downtime__2001.jpg
	PLOT=A feature-length documentary about the Free Kevin movement and the hacker world.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=N/A

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=N/A
	STORY_SMART=N/A
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=N/A

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Extended
	SPOKEN_LANG=ENG
}}


Great documentary about Kevin Mitnick and the movement [FREE KEVIN](https://www.linuxjournal.com/article/5052). Especially valuable since there are many interviews with other hackers which gives a new perspective to the whole thing. And I really like the punk style typical for hackers, for example whole scene of removing a chewing gum from the car with car-keys.

I am terribly sorry that things like this happened, probably to more people than just Kevin. And I am amazed that unfounded claims and ignorance can so easily ruin your life like this.

I can't agree more with the interview psychologists, who said:

> I think it's inhumane, and I think there are better ways to handle our problems.
>
> Obviously if this person has been able to do some of the things that they think he was doing with a computer, he's very bright.
>
> That's a real talent that's being wasted.
>
> And, to lock him up, especially in solitary confinement for 8 months ... we wouldn't treat an animal that way. And yet, this is a talented individual? Excuse me. It's not right.

I am happy that the comminity of hackers got together, started a movement [FREE KEVIN](https://www.linuxjournal.com/article/5052) and protested in peace.

There is also a over 1 hour long interview with Kevin Mitnick which was later added to re-release of the documentary in 2004.

Certainly a must watch if you have seen movie [Takedown (2000)](takedown__2000.md).

