{{#template
	../templates/movie.md
	PUB_DATE=Tue, 21 Feb 2023 19:05:02 UTC
	MOVIE_NAME=The Da Vinci Code (2006)
	MOVIE_URL=https://www.imdb.com/title/tt0382625
	MOVIE_IMG=the_da_vinci_code__2006.jpg
	PLOT=A murder in Paris' Louvre Museum and cryptic clues in some of Leonardo da Vinci's most famous paintings lead to the discovery of a religious mystery. For 2,000 years a secret society closely guards information that - should it come to light - could rock the very foundations of Christianity.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=No
	STORY_SMART=No
	STORY_ENDING=No

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Maybe
	SEX=Maybe

	RELEASE_VERSION=Extended
	SPOKEN_LANG=ENG
}}


I went into watching this movie expecting interesting history-fiction, and instead I got disappointment.

As one could expect from history-fiction movie, it is full of historic inaccuracies.

In all honesty, this could have been a good movie, but they blew it. The potential was there, but there are so many plot-holes and logic-defying puzzles, it is just bad.


```admonish warning collapsible=true title="SPOILER WARNING"

What I did not like:
- 19m mark: GPS tracker this small with accuracy 2 feet anywhere in the world? In 2006? Complete bullshit.
- Regarding the murder of Jacques Sauniere:
	- It is totally unbelievable that the whole gallery did not went into full lock-down after the painting was taken from the wall.
	- It took literally forever for anyone (mostly the guards) to show up!
		- The guy managed to completelly stip down, draw circle on the floor, a pentacle on chest and encrypt a text?
		- Oh and also run around the muzeum, making blody hand-prints everywhere and grafiti on paintings?
		- And then go lie down into circle and wait for death, or guards.
- 27m mark: The "encrypted text"
	- At first, I though that the numbers have to do something with the cipher, but apparently no. It is just a [anagram](https://en.wikipedia.org/wiki/Anagram) (re-arranged letters).
	- I spent almost 20 minutes trying to figure it out. Even with the solution from Langdon, I could not figure out the principle.
	- The real answer is - you have to brute-force it, trial and error. The numbers are there as a hint - it is [Fibonacci sequence](https://en.wikipedia.org/wiki/Fibonacci_number) but out-of-order. Which "apparently" hits that the letters are out of order too.
	- How could you come up with this on your death bed? Or rather death floor? To say that I was disported with this result is understatement.
- The rest of the puzzles are complete garbage! Basically two people are running around generating random words and making wild guessed.
- 30m mark: The security guy surrenders his gun because Sophie threatens to destroy a painting? Also, shouldn't they just get locked behind the bars like Jacques?
	- The security guard then gets scolded for surrendering his gun, so that makes up for it. At least the movie acknowledges the stupidity of guards actions.
- The chief of investigation Fache is very unprofessional, to make a guess with questionable evidence is really bad. Later in the movie he explains his motives, but I can't forgive this one.
- 41m mark: Did Sophie just take off cover from a needle, bent it and threw away? What logic is that? With the cover on, it is safe. Without it, it is not.
- 2h 22m mark: Did Langdon really have to spell out word `apple`? What the fuck? Why?

There are probably more things, but at some point I just did not bother to write everything down.

Here are some tips how to fix this movie:
- Replace conclusion-jumping Fache with ordinary policeman.
- Replace all the rubbish-puzzles with decent puzzles.
- Make Jacques death actual mystery, do not show what happened.
	- Also, make him set-up some kind of [dead-man-switch](https://en.wikipedia.org/wiki/Dead_man%27s_switch) on his email or something instead of stripping down and making graffiti.
	- Or he cloud write his computer password in blood on the floor. There are so many options.
- Make the secret societies cooler, they appear mostly like some weird sex clubs.
- Make characters more relatable, give them a bit more depth. For example Sophie is terrible character.

The only smart thing in the whole movie was throwing the GPS tracker out of window into car.

```

