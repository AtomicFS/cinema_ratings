{{#template
	../templates/movie.md
	PUB_DATE=Tue, 19 Jul 2022 18:22:00 UTC
	MOVIE_NAME=Blade (1998)
	MOVIE_URL=https://www.imdb.com/title/tt0120611
	MOVIE_IMG=blade__1998.jpg
	PLOT=The Daywalker known as Blade - a half-vampire, half-mortal man - becomes the protector of humanity against an underground army of vampires.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Maybe
	STORY_SMART=No
	STORY_ENDING=No

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=Maybe
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I think that the world building could be better. There are some inconsistencies, but still passable.

The visuals are decent for the majority of the movie, mostly thanks to good [practical effects](https://en.wikipedia.org/wiki/Practical_effect). However at the end they switch to dodgy [CGI](https://en.wikipedia.org/wiki/Computer-generated_imagery), which effectively ruins feel of the movie. As for the action scenes, some are good, but there is plenty where the camera man is having a stroke and editor trying to switch between camera angles as fast of possible.

As for the characters, there is no character development whatsoever. In essence, it feels like just bunch of assholes thrown into vampire story with guns.

The story is not great either, there are some serious holes in logic and plot. Many times I just kept asking myself "why?" since there is no sense of motivation from anybody. Sure, Blade wants to kill vampires because they killed his mother or something, but that's it.

Overall, meh.

