{{#template
	../templates/movie.md
	PUB_DATE=Tue, 28 Feb 2023 14:36:14 UTC
	MOVIE_NAME=Scott Pilgrim vs The World (2010)
	MOVIE_URL=https://www.imdb.com/title/tt0446029
	MOVIE_IMG=scott_pilgrim_vs_the_world__2010.jpg
	PLOT=As bass guitarist for a garage-rock band, Scott Pilgrim has never had trouble getting a girlfriend; usually, the problem is getting rid of them. But when Ramona Flowers skates into his heart, he finds she has the most troublesome baggage of all: an army of ex-boyfriends who will stop at nothing to eliminate him from her list of suitors.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Well, I liked it. Nice journey of Scott's self-discovery and growth.

Not sure how I feel about the ending.

Really decent watch.


```admonish warning collapsible=true title="SPOILER WARNING"

What I really liked:
- The whole vegan guy character was great, with the vegan-police and everything, just great.
- Scott's roommate Wallace is great! I love that guy! (no homo ... maybe ...)
- The scene when Chau Knives come knocking and Scott jumps out of the window is genius.

Not only that Chau Knives is too cool for Scott, she is also too good for him too.

```

