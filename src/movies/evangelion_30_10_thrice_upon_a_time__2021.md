{{#template
	../templates/movie.md
	PUB_DATE=Thu, 17 Nov 2022 16:22:04 UTC
	MOVIE_NAME=Evangelion: 3.0+1.0 Thrice Upon a Time (2021)
	MOVIE_URL=https://www.imdb.com/title/tt2458948
	MOVIE_IMG=evangelion_30_10_thrice_upon_a_time__2021.jpg
	PLOT=In the aftermath of the Fourth Impact, stranded without their Evangelions, Shinji, Asuka, and Rei find refuge in one of the rare pockets of humanity that still exist on the ruined planet Earth. There, each of them live a life far different from their days as an Evangelion pilot. However, the danger to the world is far from over. A new impact is looming on the horizon—one that will prove to be the true end of Evangelion.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=No
	STORY_SMART=Maybe
	STORY_ENDING=No

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=4
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


{{#include ../notes/neon_genesis_evangelion.md}}

Eeeh ... what the fuck?

I am not a fan. If Evangelion, the original was far superior.

