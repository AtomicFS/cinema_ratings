{{#template
	../templates/movie.md
	PUB_DATE=Sat, 2 Apr 2022 18:30:00 UTC
	MOVIE_NAME=Johnny Mnemonic (1995)
	MOVIE_URL=https://www.imdb.com/title/tt0113481
	MOVIE_IMG=johnny_mnemonic__1995.jpg
	PLOT=In a dystopian 2021, Johnny is a data trafficker who has an implant that allows him to securely store data too sensitive for regular computer networks. On one delivery run, he accepts a package that not only exceeds the implant's safety limits--and will kill him if the data is not removed in time--but also contains information far more important and valuable than he had ever imagined. On a race against time, he must avoid the assassins sent to kill him and remove the data before it, too, ends his life.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Well, not bad, but neither great.

Since the story takes place in future, and given it's age, some things feel a bit cringy. It also has rather typical movie feel for something in it's era.

The action scenes are well made, acting is good, and story is decent with some very interesting elements.

Overall, it is decent watch.

