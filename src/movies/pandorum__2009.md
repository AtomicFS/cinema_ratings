{{#template
	../templates/movie.md
	PUB_DATE=Sun, 20 Feb 2022 10:39:00 UTC
	MOVIE_NAME=Pandorum (2009)
	MOVIE_URL=https://www.imdb.com/title/tt1188729
	MOVIE_IMG=pandorum__2009.jpg
	PLOT=Two crew members wake up on an abandoned spacecraft with no idea who they are, how long they've been asleep, or what their mission is. The two soon discover they're actually not alone - and the reality of their situation is more horrifying than they could have imagined.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Story is good, acting is good, but everything is a bit too dark. In general the movie works well with light and dark, but sometimes it is too dark to see what is going on.

Actions scenes are hard to follow since they are heavily edited with few seconds for each camera shot, just blinking away. Unfortunately quite typical to American action scenes.

Overall, it is not bad. Decent watch.

