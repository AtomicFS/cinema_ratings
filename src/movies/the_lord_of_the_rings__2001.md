{{#template
	../templates/movie.md
	PUB_DATE=Thu, 01 Dec 2022 15:57:37 UTC
	MOVIE_NAME=The Lord of the Rings (trilogy; 2001-2003)
	MOVIE_URL=https://www.imdb.com/title/tt0120737
	MOVIE_IMG=the_lord_of_the_rings__2001.jpg
	PLOT=Young hobbit Frodo Baggins, after inheriting a mysterious ring from his uncle Bilbo, must leave his home in order to keep it from falling into the hands of its evil creator. Along the way, a fellowship is formed to protect the ringbearer and make sure that the ring arrives at its final destination: Mt. Doom, the only place where it can be destroyed.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Extended
	SPOKEN_LANG=ENG
}}


I am reviewing all 3 `Lord of the Rings` movies in a single review, since they were made is quick succession and there is an excellent transition in between them. They are really one big story.

Simply said: I love it. The story is great, characters are wonderful and relatable, music can touch your heart. Overall - excellent.

Regarding the extended vs standard, let me put it this way:
- If you are a big fan or you have already seen the standard release of the movies, then go and watch the extended variants.
- If you are new to `Lord of the Rings`, go and watch the standard release.

I am not sure exactly why or how to explain this, but the standard variant seems to have better story-flow. Somehow it feels like there is a bit more color. On the other hand, the extended variant is great, but feels a bit exhausting and sometimes rather monotone.

One big thing that stuck to me was the conversation between Frodo and Gandalf, from the first movie, when they are both in Moria.

> **Frodo:** I wish the Ring had never come to me. I wish none of this had happened.
>
> **Gandalf:** So do all who live to see such times. But that is not for them to decide. All we have to decide is what to do with the time that is given to us.

