{{#template
	../templates/movie.md
	PUB_DATE=Mon, 29 May 2023 13:22:00 UTC
	MOVIE_NAME=The Big Lebowski (1998)
	MOVIE_URL=https://www.imdb.com/title/tt0118715
	MOVIE_IMG=the_big_lebowski__1998.jpg
	PLOT=Jeffrey "The Dude" Lebowski, a Los Angeles slacker who only wants to bowl and drink White Russians, is mistaken for another Jeffrey Lebowski, a wheelchair-bound millionaire, and finds himself dragged into a strange series of events involving nihilists, adult film producers, ferrets, errant toes, and large sums of money.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes
	
	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=Yes
	EROTIC=Yes
	SEX=Maybe

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Brilliant, simply brilliant, loving it.

I love the sense of humor, the story, the actors. Work of a genius.

