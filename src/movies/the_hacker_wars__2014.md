{{#template
	../templates/movie.md
	PUB_DATE=Sat, 13 May 2023 09:49:57 UTC
	MOVIE_NAME=The Hacker Wars (2014)
	MOVIE_URL=https://www.imdb.com/title/tt4047350
	MOVIE_IMG=the_hacker_wars__2014.jpg
	PLOT=h)ac(k)tivist-noun: a person who uses technology to bring about social change. The Hacker Wars - a film about the targeting of (h)ac(k)tivists, activists and journalists by the US government. There is a war going on- the war for our minds. The Hacker Wars.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=N/A

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes
	
	STORY_SENSIBLE=N/A
	STORY_SMART=N/A
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=N/A

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=Maybe
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Interesting documentary about hacktivism, about groups like [Anonymous](https://en.wikipedia.org/wiki/Anonymous_(hacker_group)) or [LulzSec](https://en.wikipedia.org/wiki/LulzSec).

It shows how the political and juridical systems are connected and corrupt. For example Andrew Aurenheimer beeing sentenced to 3,5 years of prison for accesing publically accesible information to demonstrate poor security practises at AT&T, or journalist Barrett Brown sentenced to 105 year for writing about Jeremy Hammond hacks.

Sometimes it is a bit confusing, and lacking in structure. Still would recommend.

