{{#template
	../templates/movie.md
	PUB_DATE=Mon, 18 Apr 2022 20:15:21 UTC
	MOVIE_NAME=The Emoji Movie (2017)
	MOVIE_URL=https://www.imdb.com/title/tt4877122
	MOVIE_IMG=the_emoji_movie__2017.jpg
	PLOT=Gene, a multi-expressional emoji, sets out on a journey to become a normal emoji.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Maybe
	STORY_SMART=No
	STORY_ENDING=No

	FEELING_MEMORABLE=No
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=No
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=0
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Story is very stereotypical, the same old story, nothing new. It is simply a long running commercial full of embedded advertising.

Do not waste your time with this commercial.

Interesting video about the movie from [Film Theory: Is The Emoji Movie ILLEGAL?](https://www.youtube.com/watch?v=hkA2a4_tNOs)

