{{#template
	../templates/movie.md
	PUB_DATE=Mon, 18 Apr 2022 20:00:57 UTC
	MOVIE_NAME=The Incredibles 2 (2018)
	MOVIE_URL=https://www.imdb.com/title/tt3606756
	MOVIE_IMG=the_incredibles_2__2018.jpg
	PLOT=Elastigirl springs into action to save the day, while Mr. Incredible faces his greatest challenge yet - taking care of the problems of his three children.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=No
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Continuation of previous [The Incredibles (2004)](the_incredibles__2004.md) movie, which was so much better.

The one feels like it is trying to tell the story all over again in more "modern" style where man as parents suck and only women are capable. As result, family issues emerge which have been already resolved in last movie. Story is predictable and often far too unrealistic, which is being covered by infantile and stupid jokes (including poop jokes).

It feels like bad remake.

