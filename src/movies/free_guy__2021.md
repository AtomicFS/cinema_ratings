{{#template
	../templates/movie.md
	PUB_DATE=Sun, 20 Feb 2022 10:39:00 UTC
	MOVIE_NAME=Free Guy (2021)
	MOVIE_URL=https://www.imdb.com/title/tt6264654
	MOVIE_IMG=free_guy__2021.jpg
	PLOT=A bank teller called Guy realizes he is a background character in an open world video game called Free City that will soon go offline.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=No
	STORY_ENDING=No

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I was looking forward to this movie, not too hyped but still looking forward to it. And to be honest I was disappointed. Visuals and music are good, acting is also decent, but the story sucks. Concept is intriguing and has a lot of potential, but it is untapped.

Characters are under-developed, some are just annoying. For example the boss of the company is a character that should be replaced. I understand that he is supposed to be asshole, but he comes out just as stupid - and as the result the ending is unsatisfying. Seems like many of his actions are lacking any meaning behind them, any motivation. I guess he is supposed to represent the main villain - it is weak. This goes also for some side-characters.

In addition, I had a feeling during the entire movie, that script was written by some out-of-touch-with-reality manager who tried very hard to make movie for young audience by filling it up to brim with buzzwords and slogans without understanding their meaning. And as result, the movie feels like it is making fun of young internet-savvy gaming people, instead of having fun with them as equals.

I would have asked for refund and not watch it again.

