{{#template
	../templates/movie.md
	PUB_DATE=Thu, 16 Feb 2023 16:23:58 UTC
	MOVIE_NAME=Harry Potter 3: Prisoner of Azkaban (2004)
	MOVIE_URL=https://www.imdb.com/title/tt0304141
	MOVIE_IMG=harry_potter_3_prisoner_of_azkaban__2004.jpg
	PLOT=Year three at Hogwarts means new fun and challenges as Harry learns the delicate art of approaching a Hippogriff, transforming shape-shifting Boggarts into hilarity and even turning back time. But the term also brings danger: soul-sucking Dementors hover over the school, an ally of the accursed He-Who-Cannot-Be-Named lurks within the castle walls, and fearsome wizard Sirius Black escapes Azkaban. And Harry will confront them all.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I love the visuals, it feels like the franchise has finally found the perfect formula for world-building, visual style and overall feel for the wizard world.

What I really dislike is the ever increasing inconsistency of the world-building. It seriously feels like J. K. Rowling willy nilly slams in random stuff to make it interesting.

So far, my favorite Harry Potter movie, mostly because of professor Lupin and Severus Black. These two just added so much to the story.

```admonish warning collapsible=true title="SPOILER WARNING"

- Severus Snape is acting unnecessarily asshole-ish.
- Yet even more dangerous Quidditch, where multiple students were seriously harmed.
- The map of Hogwarts is so overpowered, how come it is not widely used?
	- Not only the map but many spells and items are so under-utilized.
- The werewolf just looks terrible. I guess they wanted it to be a bit original, but it looks just horrible, kinda like angry Golum from Lord of the Rings.
- Dumbledore smacking Ron's leg is kinda funny, but very uncharacteristic of Dumbledore we are used to.
- Regarding the infamous time-traveling
	- This thing just feels out of place to be honest. It serves no real purpose.
	- I am not going to dive into the fact that this world-altering super power was used to just take more classes. Mostly because Harry Potter is following the [Novikov self-consistency principle](https://en.wikipedia.org/wiki/Novikov_self-consistency_principle). In short: past cannot be changed, only observed from another perspective.

Those moments between Harry and professor Lupin with flute solo, those are gorgeous and memorable.

```

