{{#template
	../templates/movie.md
	PUB_DATE=Tue, 02 May 2023 23:19:22 UTC
	MOVIE_NAME=Short Circuit (1986)
	MOVIE_URL=https://www.imdb.com/title/tt0091949
	MOVIE_IMG=short_circuit__1986.jpg
	PLOT=After a lightning bolt zaps a robot named Number 5, the lovable machine starts to think he's human and escapes the lab. Hot on his trail is his designer, Newton, who hopes to get to Number 5 before the military does. In the meantime, a spunky animal lover mistakes the robot for an alien and takes him in, teaching her new guest about life on Earth.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


The visuals are great thanks to excellent practical special effects. It has aged quite well and is still a decent watch nowadays.

There are some cringe moments, like when number 5 robot reprograms by frantically touching single button. Then there are some other technicalities, however most of these issues are just details.

Overall, decent.

