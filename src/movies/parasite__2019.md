{{#template
	../templates/movie.md
	PUB_DATE=Mon, 4 Apr 2022 21:51:00 UTC
	MOVIE_NAME=Parasite (2019)
	MOVIE_URL=https://www.imdb.com/title/tt6751668
	MOVIE_IMG=parasite__2019.jpg
	PLOT=All unemployed, Ki-taek's family takes peculiar interest in the wealthy and glamorous Parks for their livelihood until they get entangled in an unexpected incident.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=Yes

	RELEASE_VERSION=Standard
	SPOKEN_LANG=KOR
}}


This one I started watching without knowing anything about it, not even the genre. And in all honesty, I was expecting some kind of science fiction movie with alien running around and killing people. The movie is actually about wealth gap, showing a contract between rich and poor.

Certainly a interesting story worth watching. Music is very gentle, only few tones here and there just to underline the scene, you almost miss it as you get sucked in into the world behind screen. Great job at showing the contrast not only in the material, but also the way of thinking and even difference between family relationships.

There are even some interesting twists and revelations - because of that I am not sure about the re-watchability - but it is certainly worth your time.

