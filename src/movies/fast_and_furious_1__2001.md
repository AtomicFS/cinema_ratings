{{#template
	../templates/movie.md
	PUB_DATE=Sat, 19 Feb 2022 14:41:00 UTC
	MOVIE_NAME=Fast and the Furious (2001)
	MOVIE_URL=https://www.imdb.com/title/tt0232500
	MOVIE_IMG=fast_and_furious_1__2001.jpg
	PLOT=Dominic Toretto is a Los Angeles street racer suspected of masterminding a series of big-rig hijackings. When undercover cop Brian O'Conner infiltrates Toretto's iconoclastic crew, he falls for Toretto's sister and must choose a side: the gang or the LAPD.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=0
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=Yes
	DRUGS=Maybe
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Well, quite predictable, almost no surprises. I would call some decision-making of the characters plain stupid, but overall it is not too bad (I have seen way worse).

In the end, it is as decent watch.

