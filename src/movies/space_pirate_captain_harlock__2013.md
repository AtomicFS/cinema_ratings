{{#template
	../templates/movie.md
	PUB_DATE=Sun, 3 Apr 2022 9:33:00 UTC
	MOVIE_NAME=Space Pirate Captain Harlock (2013)
	MOVIE_URL=https://www.imdb.com/title/tt2668134
	MOVIE_IMG=space_pirate_captain_harlock__2013.jpg
	PLOT=Space Pirate Captain Harlock and his fearless crew face off against the space invaders who seek to conquer the planet Earth.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=JPN
}}


I have seen the movie both with original audio and with dubbing, and I must say - the original audio with subtitles is better. Don't take me wrong, the dubbing is decent, but the translation is off. With original soundtrack and subtitles you get much deeper into the characters and story, it is just better story-telling. The translation team has taken too many liberties and changed too much - some characters feel very different from the original.

Good story, good flow, good visuals. Overall good watch.

I little bit of critique: I don't like Captain's bird - it hits this uncanny valley when the I can see the resemblance to some pelican, but it is just off. Mostly, the neck is too unnatural. It just does not fit in. There are odd creatures, but they are either real-life looking or completely alien, nothing in between like this bird.

