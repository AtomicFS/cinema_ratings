{{#template
	../templates/movie.md
	PUB_DATE=Sun, 26 Feb 2023 12:16:45 UTC
	MOVIE_NAME=The Island (2005)
	MOVIE_URL=https://www.imdb.com/title/tt0399201
	MOVIE_IMG=the_island__2005.jpg
	PLOT=In 2019, Lincoln Six-Echo is a resident of a seemingly "Utopian" but contained facility. Like all of the inhabitants of this carefully-controlled environment, Lincoln hopes to be chosen to go to The Island - reportedly the last uncontaminated location on the planet. But Lincoln soon discovers that everything about his existence is a lie.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Maybe
	SEX=Maybe

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


The music is incredible, I love it. The story is also great with awesome twist and clever story-telling. Characters and actors are awesome too.

Visuals are awesome, with the exception of excessively shaky camera in action moments, and occasional epilepsy inducing blinking mess.

Reminds me of anime [The Promised Neverland (2019)](../shows/the_promised_neverland__2019.md).

Overall, great movie, one of my favorites.


```admonish warning collapsible=true title="SPOILER WARNING"
I am not sure about the ending. While cool and all, you basically end up with few hundred people in desert with no means of transportation, no rations and no water.

Few things that I do not like:

- 1h 9m mark: I do not understand why they would shoot McCord in public place, causing panic and allowing Lincoln and Jordan to escape.
- 1h 17m mark: Tracking vehicle with frequency 204 GHz - more hertz does not make it better. At these frequencies the tracking thing would be pretty much useless since you would need direct line of sight (higher the frequency, lower penetration power - simply said anything even thin wall would block the signal, [source](https://en.wikipedia.org/wiki/Extremely_high_frequency)). I do not see the benefits in this type of application. Maybe if the city was covered in 204 GHz network?
- 1h 18m mark: The police car stops in the middle of the intersection for no reason, also there is no way for the truck to crash into them since there are cars waiting for the green light.
- 1h 23m mark: Why on earth would drive of the Black Wasp drive like that by the truck the get smacked in face? I mean, If it was me driving, I would probably drive up to the truck, come to full stop and try to physically arrest Lincoln and Jordan. The maneuver makes no sense, even if you job is to kill shoot them - just drive up, stop, shoot, cleanup. I don't get it.
- 1h 29m mark: I am not sure what is the deal with Dr Merrick potentially killing Gandu? I would be far better just to drug him and keep him sedated until new clone is made. And the same goes for all the other clones when Merrick decides to kill them. He is basically running insurance company in case shit happens - what is shit happened and the customer dies because his only clone was incinerated?

Most of these are very minor details. One or two are not that minor, but none of them messes with the story telling nor would break the immersion.

```

