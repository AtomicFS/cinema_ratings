{{#template
	../templates/movie.md
	PUB_DATE=Mon, 16 Oct 2023 11:03:26 UTC
	MOVIE_NAME=The Dyatlov Pass Incident (2013)
	MOVIE_URL=https://www.imdb.com/title/tt1905040
	MOVIE_IMG=the_dyatlov_pass_incident__2013.jpg
	PLOT=In February of 1959, nine Russian hikers ventured into a remote area of the Ural Mountains. Two weeks later, all of them were found dead. What happened is a mystery that has baffled investigators and researchers for decades. It has become known as the Dyatlov Pass Incident. When five ambitious American college students are issued a grant to return to the site of the original events, they gear up with the belief that they can uncover and document the truth of what happened to the supposedly experienced hikers. But what they find is more shocking than anything they could have imagined. Retracing the steps of the Russians' ill-fated journey, the students are plagued by strange and increasingly terrifying phenomena that suggest that in spite of the desolate surroundings, they are not alone. The forces at work in the Dyatlov Pass Incident have been waiting for them.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Maybe
	STORY_SMART=No
	STORY_ENDING=No

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Well, started out interesting, with some potential, but in the end disappointed.

Overall it is just blend, uninteresting, run of the mill horror movie.

