{{#template
	../templates/movie.md
	PUB_DATE=Tue, 28 Feb 2023 14:14:11 UTC
	MOVIE_NAME=Bionicle 3: Web of Shadows (2005)
	MOVIE_URL=https://www.imdb.com/title/tt0471588
	MOVIE_IMG=bionicle_3_web_of_shadows__2005.jpg
	PLOT=The Toa Metru return to their glorious city of Metru Nui to finally accomplish their mission, only to find the city in ruins, overrun by the dreaded Visorak horde. Captured by their new enemy and transformed into hideous Toa Hordika, our heroes must find a figure out of myth to be cured of their mutations and fulfill their destiny before the beast within overtakes them.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=0
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Story-vise quite a improvement over previous movie [Bionicle 2: Legends of Metru Nui (2004)](movies/bionicle_2_legends_of_metru_nui__2004.md).

Very repetitive music, average story, meh characters.

Overall, meh.

