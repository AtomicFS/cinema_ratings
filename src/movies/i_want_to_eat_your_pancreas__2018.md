{{#template
	../templates/movie.md
	PUB_DATE=Sun, 27 Mar 2022 22:10:00 UTC
	MOVIE_NAME=I Want to Eat Your Pancreas (2018)
	MOVIE_URL=https://www.imdb.com/title/tt7236034
	MOVIE_IMG=i_want_to_eat_your_pancreas__2018.jpg
	PLOT=After his classmate and crush is diagnosed with a pancreatic disease, an average high schooler sets out to make the most of her final days.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Another quite emotional item on my watch-list.

Well, right from the start you are told that Sakura will die - it is said in the first 2 minutes - and that makes it rather interesting since you are aware of the this the whole time. That makes one think what would they do if they met someone with terminal illness. Would it make sense to start a friendship? Or maybe even romantic relationship?

How would knowing your time of death change your view of the world? How is it different from the death by random chance, for example in car accident?

In a way, the movie has rather a positive message, not questioning what it means to die, but rather what it means to live.

