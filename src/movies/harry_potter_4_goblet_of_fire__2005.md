{{#template
	../templates/movie.md
	PUB_DATE=Thu, 16 Feb 2023 21:57:11 UTC
	MOVIE_NAME=Harry Potter 4: Goblet of Fire (2005)
	MOVIE_URL=https://www.imdb.com/title/tt0330373
	MOVIE_IMG=harry_potter_4_goblet_of_fire__2005.jpg
	PLOT=When Harry Potter's name emerges from the Goblet of Fire, he becomes a competitor in a grueling battle for glory among three wizarding schools - the Triwizard Tournament. But since Harry never submitted his name for the Tournament, who did? Now Harry must confront a deadly dragon, fierce water demons and an enchanted maze only to find himself in the cruel grasp of He Who Must Not Be Named. In this fourth film adaptation of J.K. Rowling's Harry Potter series, everything changes as Harry, Ron and Hermione leave childhood forever and take on challenges greater than anything they could have imagined.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


This new addition to Harry Potter franchise took a darker turn than the last one, but it feels shallower. Many shiny things but not enough meaningful ones.

Compared to the previous one, it is also bit worse.

```admonish warning collapsible=true title="SPOILER WARNING"
While the introduction of Voldemort wasn't too bad, I still feel like he is a poor villain. I did not like the interaction between Voldemort and Harry, it felt just like Voldemort is high-school bully and not evil wizard for whole world to be afraid of.

The whole plot with Alastor Moody was actually quite decent.

I am not very happy about Dumbledore's plan to use Harry as bait, which totally backfired. Cedric died, Voldemort was resurrected, and Harry got out just by dumb luck.
```

