{{#template
	../templates/movie.md
	PUB_DATE=Sat, 14 May 2022 08:27:45 UTC
	MOVIE_NAME=Dude, Where's My Car? (2000)
	MOVIE_URL=https://www.imdb.com/title/tt0242423
	MOVIE_IMG=dude_wheres_my_car__2000.jpg
	PLOT=Two stoners wake up after a night of partying and cannot remember where they parked their car.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=No
	STORY_SMART=No
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=0
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=Yes
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Well, long story short, this is not my cup of tea. The humor is rather primitive and immature. The story makes little to no sense, which is intentional, so that is ok.

Overall, I would not watch again.

