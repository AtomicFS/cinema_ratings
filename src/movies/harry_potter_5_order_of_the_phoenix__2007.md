{{#template
	../templates/movie.md
	PUB_DATE=Fri, 17 Feb 2023 13:13:56 UTC
	MOVIE_NAME=Harry Potter 5: Order of the Phoenix (2007)
	MOVIE_URL=https://www.imdb.com/title/tt0373889
	MOVIE_IMG=harry_potter_5_order_of_the_phoenix__2007.jpg
	PLOT=The rebellion begins! Lord Voldemort has returned, but the Ministry of Magic is doing everything it can to keep the wizarding world from knowing the truth - including appointing Ministry official Dolores Umbridge as the new Defence Against the Dark Arts professor at Hogwarts. When Umbridge refuses to teach practical defensive magic, Ron and Hermione convince Harry to secretly train a select group of students for the wizarding war that lies ahead. A terrifying showdown between good and evil awaits in this enthralling film version of the fifth novel in J.K. Rowling's Harry Potter series. Prepare for battle!

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=Maybe
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I am not really sure. One one hand, the movie introduces great a awesome characters, but on the other the story is mediocre at best.

And I am getting the feeling again that J. K. Rowling keeps adding random stuff, with no consideration for continuity. "Suddently we have this new thing, but we had it always!"



```admonish warning collapsible=true title="SPOILER WARNING"
Things I like:

- I love the aesthetics of the Order of the Phoenix headquarters
	- I also love the headquarters sections, they feel homey and resonate with me.
- Dolores Umbridge, as probably many others, I really hate this character - which is great!
	- We finally have a good villain.
	- Voldemort, the school bully, can pack his sack and leave.
	- The actress Imelda Staunton is superb, flawless and deserves a reward for her performance.
- There are some really interesting characters too, such as Luna Lovegood or Bellatrix Lestrange.

Things I do not like:

- Mrs Figg, introduced at the beginning
	- Dumbledore left Mrs Figg keep an eye on Harry? Sure she is nice and all, but she is a muddle and as such completelly useless even against 1st year student.
	- Technically she is a [squib](https://harrypotter.fandom.com/wiki/Squib), non-magical person who was born to at least one magical parent.
	- Still, Harry Potter is high profile target and non-magical bodyguard is just stupid.
- "I kept you in dark to keep you safe" bullshit
	- Any writer who uses this should be punished by listening to `Rick Astley - Never Gonna Give You Up` non-stop for one hour

```

