{{#template
	../templates/movie.md
	PUB_DATE=Sun, 6 Aug 2023 22:51:23 UTC
	MOVIE_NAME=Captain America: The First Avenger (2011)
	MOVIE_URL=https://www.imdb.com/title/tt0458339
	MOVIE_IMG=captain_america_the_first_avenger__2011.jpg
	PLOT=During World War II, Steve Rogers is a sickly man from Brooklyn who's transformed into super-soldier Captain America to aid in the war effort. Rogers must stop the Red Skull – Adolf Hitler's ruthless head of weaponry, and the leader of an organization that intends to use a mysterious device of untold powers for world domination.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=No

	STORY_SENSIBLE=Yes
	STORY_SMART=No
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=0
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


And then the fighting, it just seems like most of the soldiers are controlled by AI which decided to ignore their range advantage and try to melee attack the enemies instead of shooting them from distance. But I guess melee is better than storm-trooper accuracy.

Then there is this ever-present smell of American propaganda, although I assume it is accurate since in world war 2 there was a butt-load of propaganda everywhere, USA especially ... and the movie is called Captain America ... so yeah. ['Murica, fuck yeah!](https://knowyourmeme.com/memes/murica)

Certainly not a big fan of the "you don't know women" thing with women this and women that. Feels stupid and immature.

The selection of Captain's teams is kinda stupid too, especially the guy in bowler hat ... what the fuck man, take a proper helmet for fuck sake.

Overall, meh.

