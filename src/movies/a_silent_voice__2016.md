{{#template
	../templates/movie.md
	PUB_DATE=Sun, 20 Feb 2022 10:41:00 UTC
	MOVIE_NAME=A Silent Voice (2016)
	MOVIE_URL=https://www.imdb.com/title/tt5323662
	MOVIE_IMG=a_silent_voice__2016.jpg
	PLOT=Shouya Ishida starts bullying the new girl in class, Shouko Nishimiya, because she is deaf. But as the teasing continues, the rest of the class starts to turn on Shouya for his lack of compassion. When they leave elementary school, Shouko and Shouya do not speak to each other again... until an older, wiser Shouya, tormented by his past behaviour, decides he must see Shouko once more. He wants to atone for his sins, but is it already too late...?

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


If you are going to watch this, brace yourself for emotional rollercoaster. This one is difficult to watch, it will make you feel feelings, but it is worth it.

There are some very heavy topics displayed in raw uncensored form. It is dealing with people's complicated feelings, their relationships, bullying and suicide.

On the other side, the underlying message is a positive story about forgiveness and personal growth.

