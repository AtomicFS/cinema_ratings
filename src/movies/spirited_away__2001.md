{{#template
	../templates/movie.md
	PUB_DATE=Sat, 18 Feb 2023 19:59:36 UTC
	MOVIE_NAME=Spirited Away (2001)
	MOVIE_URL=https://www.imdb.com/title/tt0245429
	MOVIE_IMG=spirited_away__2001.jpg
	PLOT=A young girl, Chihiro, becomes trapped in a strange new world of spirits. When her parents undergo a mysterious transformation, she must call upon the courage she never knew she had to free her family.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Another perfect Ghibli movie.

Great music, great story, great characters, everything is just great. Awesome and gentle humor, no under-cooked infantile rubbish like in moder main-stream movies. It is just magical. Even the pacing of the story telling is perfect.

I just figured the flaw all Ghibli movies have! They end. Eventually each of them ends and you have to get back into real world.

```admonish warning collapsible=true title="SPOILER WARNING"
I just love the pacing. For example you get this action heavy scene where `No face` is chasing `San` across the bathhouse, just to end in the train with chill music and charming scenery moving outside the windows.

It gives such a contrast and space for the viewer to relax a bit a pick u breath for what comes next. And of course beautiful soundtrack.
```

