{{#template
	../templates/movie.md
	PUB_DATE=Sat, 09 Jul 2022 17:53:23 UTC
	MOVIE_NAME=A Quiet Place (2018)
	MOVIE_URL=https://www.imdb.com/title/tt6644200
	MOVIE_IMG=a_quiet_place__2018.jpg
	PLOT=A family is forced to live in silence while hiding from creatures that hunt by sound.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=Yes
	STORY_ENDING=No

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I really like the idea, I think it is smart and innovative. There are many things that are really thought out well, but there are few things I did not like.

Overall, I do not regret watching it, but I hoped for a bit more. In the end, I was a disappointed.


```admonish warning collapsible=true title="SPOILER WARNING"
First of all, in first 10 minutes of the movie I was able to guess correctly the rest of the plot. First of all, the rocket in the store, especially given the reaction of everyone when the little girl brought it, immediately made me think that it will cause death of someone (and moments later my prediction turns out correct).

After I saw the death of little girl and the whiteboard in basement with "What is the weakness", I have screamed at the TV "SOUND, the weakness is sound". In their situation, I would have taken some noise-making thing and hang it over hole with spikes or something like that (really basic trap). In the end, it was a feedback from hearing aids, which is in all honesty far less awesome. I like that they were smart enough to paint on the floorboards which spots do not make sounds, but could not figure this one out.

And what really triggered me was the scene when the dad sacrifices himself by creaming loudly and then just standing still. That douche just could not be bothered to move 2 steps to the side. Totally unnecessary and stupid waste of human life. Not to mention he could have just thrown a rock into fields.

Then there are small things like that the creature can jump though steel in the granary, but can't get into car. And few more.
```

