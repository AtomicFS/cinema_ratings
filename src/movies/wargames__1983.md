{{#template
	../templates/movie.md
	PUB_DATE=Sun, 08 May 2022 14:40:24 UTC
	MOVIE_NAME=WarGames (1983)
	MOVIE_URL=https://www.imdb.com/title/tt0086567
	MOVIE_IMG=wargames__1983.jpg
	PLOT=High School student David Lightman has a talent for hacking. But while trying to hack into a computer system to play unreleased video games, he unwittingly taps into the Defense Department's war computer and initiates a confrontation of global proportions. Together with his girlfriend and a wizardly computer genius, David must race against time to outwit his opponent and prevent a nuclear Armageddon.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Good movie, nicely captures the atmosphere of the time when phone phreaking was a thing and computers were simple 8-bit machines.

I love it, especially how everything is taken seriously and presented rather accurately. Modern hacking movies are usually just abomination of visuals that make no sense. Sure it is old, but when you read about first hackers and first computers, this will show you surprisingly realistically how it worked and looked like.

There are also more than few Easter-eggs. For example the first scene in the nuclear silo is based on real events - just look here how many times the nuclear war was a single press of a button away in [this wikipedia article](https://en.wikipedia.org/wiki/List_of_nuclear_close_calls). A lot of them were because of false data.

The entire movies is more or less focused on nuclear war, it's futility and stupidity. But still it is rather fun.

```admonish warning collapsible=true title="SPOILER WARNING"
The end is kinda week, since there is no real ending. You are left with questions like what happened to David afterwards? Did he throw away his computer and vowed to never touch it again? Or became a security engineer? And many more questions.

Quite interesting review of the movie whre the author is taking a detailed look into the used technology, political and historical background and so on: [WarGames | The Greatest Movie of All Time - J. Matthew Movies, Ep. 13](https://www.youtube.com/watch?v=prHPlwJNclw)
```

