{{#template
	../templates/show.md
	PUB_DATE=Sat, 13 Aug 2022 17:09:11 UTC
	MOVIE_NAME=Sword Art Online (2013)
	MOVIE_URL=https://www.imdb.com/title/tt2250192
	MOVIE_IMG=sword_art_online__2013.jpg
	PLOT=In the near future, a Virtual Reality Massive Multiplayer Online Role-Playing Game (VRMMORPG) called Sword Art Online has been released where players control their avatars with their bodies using a piece of technology called Nerve Gear. One day, players discover they cannot log out, as the game creator is holding them captive unless they reach the 100th floor of the game's tower and defeat the final boss. However, if they die in the game, they die in real life. Their struggle for survival starts now...

	WATCHED_UNTIL=Watched until season 3 (included).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=Sometimes
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


In short: I love it.

I am aware of the mixed reception by public, but I don't care about that.

The characters are good, villains are done well, the main idea is good and execution is solid. And I really like how the show manages to capture the internet / online community, this diverse spectrum of weirdos I am proud be part of :D.

However, it is not entirely black and white. Some season and some episodes are better than the others. (see spoiler section)

One more complain could be that the soundtrack is a bit too repetitive.

Overall, very good watch with some ups and downs.

```admonish warning collapsible=true title="SPOILER WARNING"
Regarding the seasons episodes, I would divide them like so:

- episodes **S01E01** though **S01E14** are great and superb
- episodes **S01E15** though **S01E25** are less good
	- I seriously dislike the damsel in distress theme, but at least there is a decent villain (although he could use a bit more depth to his character)
- episodes **S02E01** though **S02E24** are great again
	- also good villain(s)
- episodes **S03E01** though **S03E24** are a bit strange given this dream-like world and stuff (definitely not my favorite)
```

