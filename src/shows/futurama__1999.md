{{#template
	../templates/show.md
	PUB_DATE=Fri, 02 Jun 2023 13:34:56 UTC
	MOVIE_NAME=Futurama (1999)
	MOVIE_URL=https://www.imdb.com/title/tt0149460
	MOVIE_IMG=futurama__1999.jpg
	PLOT=A late 20th-century New York City pizza delivery boy, Philip J. Fry, after being unwittingly cryogenically frozen for one thousand years, finds employment at Planet Express, an interplanetary delivery company in the 31st century.

	WATCHED_UNTIL=Watched until season 7 (included) with specials.

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes
	
	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=Maybe
	EROTIC=Yes
	SEX=Maybe

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Work of genius, I love it. Aged quite well, still relevant.

