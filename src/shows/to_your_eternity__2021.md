{{#template
	../templates/show.md
	PUB_DATE=Sat, 26 Nov 2022 11:05:27 UTC
	MOVIE_NAME=To Your Eternity (2021)
	MOVIE_URL=https://www.imdb.com/title/tt12063450
	MOVIE_IMG=to_your_eternity__2021.jpg
	PLOT=In the beginning, an orb is cast unto Earth. It can do two things: change into the form of the thing that stimulates it; and come back to life after death. It morphs from orb to rock, then to wolf, and finally to boy, but roams about like a newborn who knows nothing. As a boy, it becomes Fushi. Through encounters with human kindness, Fushi not only gains survival skills, but grows as a person. But his journey is darkened by the inexplicable and destructive enemy Nokker, as well as cruel partings with the people he loves.

	WATCHED_UNTIL=Watched season 1 (included; 20 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Rather odd anime, especially at the beginning when it hits this uncanny valley (the wolf looks weird), but either it improves over time or I got used to it.

The story is interesting and intriguing and characters are relatable. The start might be a bit slower, but it perfectly mirrors the experience on the protagonist.

The end of season 1 might hit you hard, especially if it hits close to home. Good watch, I am looking forward to next season.

