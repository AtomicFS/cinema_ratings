{{#template
	../templates/show.md
	PUB_DATE=Sun, 01 May 2022 18:09:43 UTC
	MOVIE_NAME=KonoSuba: Gods Blessing on this Wonderful World (2016)
	MOVIE_URL=https://www.imdb.com/title/tt5370118
	MOVIE_IMG=konosuba_gods_blessing_on_this_wonderful_world__2016.jpg
	PLOT=After a traffic accident, Kazuma Sato's disappointingly brief life was supposed to be over, but he wakes up to see a beautiful girl before him. She claims to be a goddess, Aqua, and asks if he would like to go to another world and bring only one thing with him.<br><br>Kazuma decides to bring the goddess herself, and they are transported to a fantasy world filled with adventure, ruled by a demon king. Now Kazuma only wants to live in peace, but Aqua wants to solve many of this world's problems, and the demon king will only turn a blind eye for so long...

	WATCHED_UNTIL=Watched until season 2 (included; 10+10 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=Sometimes
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


This show is a blast and I love it.

Maybe a bit too [ecchi](https://www.anime-planet.com/anime/tags/ecchi) for my taste, but the show is self-aware it fits rather well to the overall feeling.

I don't have a lot to critique, the only minor thing is the art-style sometimes used - for example Kazuma's expression with triangular mouth looks weird, and some other things. Only minor things here and there, nothing of real importance.

Nice summary of why the show is soo good: [Satou Kazuma: Art of the D/ckhead](https://www.youtube.com/watch?v=Y22UjYAtV-A).

```admonish warning collapsible=true title="SPOILER WARNING"
I dare to say that most [isekai](https://www.anime-planet.com/anime/tags/isekai) shows have overpowers protagonists, however this is the exact opposite. Kazuma is more or less stuck with team of rejects that nobody wants to team up with, going from one disaster to another, and it is hilarious.

All these characters have great chemistry in between them and it just works.
```

