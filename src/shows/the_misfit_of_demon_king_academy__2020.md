{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 15:04:00 UTC
	MOVIE_NAME=The Misfit of Demon King Academy (2020)
	MOVIE_URL=https://www.imdb.com/title/tt12432936
	MOVIE_IMG=the_misfit_of_demon_king_academy__2020.jpg
	PLOT=Anos, the Demon King of Tyranny, has defeated humans, spirits, and gods alike. But even demon kings get tired of all the fighting sometimes! Hoping for a more peaceful life, Anos decides to reincarnate himself. When he wakes two thousand years later, though, he finds the world has become too peaceful--his descendants have grown weak and magic is in serious decline. Intending to reclaim his rightful place, he enrolls in Demon King Academy, where he finds that his magical power is off the charts. Literally. And because they can't measure his power, the faculty and other students regard Anos as a misfit. With the support of Misha, the one student he manages to befriend, the misfit (Demon King) begins his climb up the demon ranks!

	WATCHED_UNTIL=Watched season 1 (13 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


It is kinda mess. The rules and mechanics of the world and not well explained, while on the other hand the history is being told multiple times. Unfortunately most of the characters seem to be very flat and simple, besides the main few.

I very much like the morals of the story, and also the Alucard-wibes (from anime Hellsing Ultimate) I am getting from Anos Voldigord.

Overall, it is a decent start to what might be good anime.

