{{#template
	../templates/show.md
	PUB_DATE=Fri, 28 Oct 2022 18:40:53 UTC
	MOVIE_NAME=Beverly Hills 90210 (1990)
	MOVIE_URL=https://www.imdb.com/title/tt0098749
	MOVIE_IMG=beverly_hills_90210__1990.jpg
	PLOT=Follow the lives of a group of teenagers living in the upscale, star-studded community of Beverly Hills, California and attending the fictitious West Beverly Hills High School and, subsequently, the fictitious California University after graduation.

	WATCHED_UNTIL=Watched first episode (less than half).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=No

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=No

	STORY_SENSIBLE=No
	STORY_SMART=No
	STORY_ENDING=N/A

	FEELING_MEMORABLE=No
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=No
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=0
	RELIGIOUS=N/A
	AFTER_CREDIT_SCENE=N/A
	VIOLENCE=N/A
	DRUGS=N/A
	EROTIC=N/A
	SEX=N/A

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Oh my, what did I step into. Such superficial and hollow piece of garbage. I did not finish even the first episode.

