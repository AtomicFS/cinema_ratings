{{#template
	../templates/show.md
	PUB_DATE=Sun, 11 Jun 2023 16:11:02 UTC
	MOVIE_NAME=Haven't You Heard? I'm Sakamoto (2016)
	MOVIE_URL=https://www.imdb.com/title/tt5701624
	MOVIE_IMG=havent_you_heard_i_am_sakamoto__2016.jpg
	PLOT=First year high school student Sakamoto isn't just cool, he's the coolest! Almost immediately after starting school, he began attracting everyone's attention. The girls love him, and most of the boys resent him. There's even a boy in the class who works as a model, but who is constantly upstaged by Sakamoto!\n\nNo matter what tricks the other boys try to play on him, Sakamoto always manages to foil them with ease and grace. Though Sakamoto may seem cool and aloof, he helps others when asked, such as in the case of the boy in his class who was being constantly bullied. No matter what difficulties Sakamoto encounters, he moves through his high school life with confidence and class!

	WATCHED_UNTIL=Watched season 1 (12 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes
	
	STORY_SENSIBLE=Maybe
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=Maybe
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Over the top, totally exaggerated, strange and unrealistic, yet awesome and hilarious. I like it and would recommend.

