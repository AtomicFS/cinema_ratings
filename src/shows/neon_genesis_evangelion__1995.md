{{#template
	../templates/show.md
	PUB_DATE=Tue, 01 Nov 2022 18:35:22 UTC
	MOVIE_NAME=Neon Genesis Evangelion (1995)
	MOVIE_URL=https://www.imdb.com/title/tt0112159
	MOVIE_IMG=neon_genesis_evangelion__1995.jpg
	PLOT=At the turn of the century, the Angels returned to Earth, seeking to wipe out humanity in an apocalyptic fury. Devastated, mankind's last remnants moved underground to wait for the day when the Angels would come back to finish the job. Fifteen years later, that day has come... but this time, humanity is ready to fight back with terrifying bio-mechanical weapons known as the Evangelions. Watch as Shinji, Rei, Asuka and the rest of the mysterious shadow agency Nerv battle to save earth from total annihilation.

	WATCHED_UNTIL=Watched season 1 (26 episodes, with addition of Director's cut episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=4
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=Yes

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Be ready for royal brain-fuck beyond your understanding.

This show is very heavy on existential, emotional, spiritual and religious themes. And after viewing it, you will likely experience some form of existential crisis.

Somewhere until episode 19 everything is still rather coherent and understandable. Episode 20 is already getting weird, then the real brain-fuck happens.

For episodes 21, 22, 23 and 24 there is also Director's cut, which is just the original episode with additional content. I do recommend to watch Director's cut.

{{#include ../notes/neon_genesis_evangelion.md}}

However episodes 25 and 26 are as some would say "a fewer dream" (non-linear dream-like hallucination). I personally kinda prefer to first watch the re-make of the final episodes in form of movie [Neon Genesis Evangelion: The End of Evangelion (1997)](../movies/neon_genesis_evangelion_the_end_of_evangelion__1997.md) and then finish with the final two episodes from the show.

I really like most of the visuals, although sometimes the original episodes do feel rather static, especially towards the end when the budget was running low. I do not mind the static non-moving scenes without dialog in earlier episodes, but starting with episode 20, they are getting obscenely long (sometimes even a whole minute with absolutely nothing happening). As for the animation of fight scenes - they are glorious, oh my goodness, that is eye-candy. And the last thing regarding visuals are flashing images - why on earth people just keep using this flash-image bullshit in cinematography? I hate it.

Regarding story, I dare to say it is right on the line between brilliant and insane. If you manage to get over the religious matter, since the show borrows heavily from Christianity, you will get fascinating story combined with secret organization with secret plan ... it is just superb. As the show progresses you are slowly peeling off layers and layers of secrets and plots and everything you thought you knew is proven to be wrong.

This is not the first time I saw this show. As matter of fact Neon Genesis Evangelion was one of the first anime shows that I have watched (maybe even the very first one, not sure anymore). But I must admit, the first time I saw it, I had a very different experience. Although I found it interesting, I could not comprehend what I just saw. Nowadays I feel like I can understand the character much more, and I can relate to them, which does make a huge difference. I addition I have read and seen a lot of supplementary material to help me understand the world and the story.

Overall, while I do not regret watching it (generally speaking), I must promise myself to not watch this again. I have made this promise to myself before, and I have just broken it. Hopefully this website and this message to my future self will make sure it does not happen again. In my opinion, it is just too far in mind-blowing, mind-warping zone for my own good.

Just this review, I have been writing and re-writing it again and again. I want to do this show justice, because I do feel a strange connection to to, but on the other hand I feel a bit hurt by it. Kinda like that hedgehog's dilema mentined in 4th episode.

Because of this, I cannot recommend this anime to anyone to watch. If you decide to watch, consider yourself warned.

```admonish warning collapsible=true title="SPOILER WARNING"
I have seen many complain that Shinji Ikari (the main character) is just insufferable wimp. And I must disagree. After viewing the show again, I can see that Shinji and Asuka are rather similar in many ways - both have experienced loss in family, both have sense of abandonment and both feel lonely. However each of them take a different stand.

Asuka is rather defensive and draws her self-worth from how other people perceive her. As a result, whenever she is bested by Shinji it is frustrating to her, especially because Shinji does not care. And when she is defeated by angel and then bested by Shinji, her world falls apart as she lost the only thing holding her together - praise of others and sense of being needed (as Rei has pointed out).

Then there is Shinji, who came from similar background but that is where the similarity ends. He starts with nothing - no confidence and self-hatherd. Then over the period of the show he slowly builds up a much stronger foundation then Asuka. And to me it seems that his outbursts of hate, anger and also the need to run away are caused by the people around him who try to pull him back down into mud of worthlessness (like his father who might be the best example). So when he comes up and says "I will not pilot Eva anymore!", it is not him bitching like a crybaby, but rather him standing up for himself. And Asuka can't stand for herself because she requires other people's approval.

Not sure if I can explaining my thought properly, but at least I have tried. In short, Shinji might have started as crybaby, but has began to grown into self-reliant young man. He is on the right track.

As for the ending of the show, while I believe I understand the big picture and main premise, I am pretty much lost with the details and hidden meanings.

If you want to know more, I do have few recommendations (warning: since the ending is vague, there are multiple theories and explanations to the show):
- [Explaining Evangelion--The Lore of Japan's Most Brilliant Sci-Fi](https://www.youtube.com/watch?v=E5gVDDpviKo)
	- focused on the lore and the big picture
- [Neon Genesis Evangelion Ending Explained](https://www.youtube.com/watch?v=DuBOOcWIO2o), there is also text version [Neon Genesis Evangelion Explained: Ending Differences and Reasons](https://www.ign.com/articles/2019/06/28/evangelion-neon-genesis-ending-explained)
	- focused on the personal experience of Shinji and meaning behing his actions

Interesting note here, I found it also mentioned in the comments of [Neon Genesis Evangelion Ending Explained](https://www.youtube.com/watch?v=DuBOOcWIO2o) video.

> *[geno]:* The fact that Shinji hardly knew Kaworu, but he was his most loved person is genuinely heartbreaking. He really did have no one.

For context: it is said that during the 3rd impact Rei appeared to everyone in form of their most beloved person ([time stamp 3m 17s](https://youtu.be/DuBOOcWIO2o?t=197)) - for Shinji it was Kaworu.

```

