{{#template
	../templates/show.md
	PUB_DATE=Sat, 24 Jun 2023 10:36:41 UTC
	MOVIE_NAME=ReLIFE (2016)
	MOVIE_URL=https://www.imdb.com/title/tt5857720
	MOVIE_IMG=relife__2016.jpg
	PLOT=The story follows Kaizaki Arata, a 27-year-old jobless man, who fails at every job interview he had after quitting his last company. His life changes after he met Yoake Ryou of the ReLife Research Institute, who offered him a drug that can change his appearance to 17-years-old and to become a subject in an experiment for one year. Thus, he begins his life as a high school student once more.

	WATCHED_UNTIL=Watched season 1 (12 episodes + 4 OVA).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=Maybe
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Funny and lovely show. Mostly lighthearted comedy, sometimes with heavy topics. Also a lot of attention is placed on self-improvement and personal growth.

I am not really sure about the accompanying music, quite often it does not really fit well the show and feels off.

Do not forget to watch the OVAs, they actually contain a proper ending to the show.

Overall I like it. Certainly worth to watch.

