{{#template
	../templates/show.md
	PUB_DATE=Mon, 14 Mar 2022 18:15:00 UTC
	MOVIE_NAME=Fullmetal Alchemist: Brotherhood (2009)
	MOVIE_URL=https://www.imdb.com/title/tt1355642
	MOVIE_IMG=fullmetal_alchemist_brotherhood__2009.jpg
	PLOT=Edward and Alphonse Elric's reckless disregard for alchemy's fundamental laws ripped half of Ed's limbs from his body and left Al's soul clinging to a cold suit of armor. To restore what was lost, the brothers scour a war-torn land for the Philosopher's Stone, a fabled relic which grants the ability to perform alchemy in impossible ways.<br><br><br><br>The Elrics are not alone in their search; the corrupt State Military is also eager to harness the artifact's power. So too are the strange Homunculi and their shadowy creator. The mythical gem lures exotic alchemists from distant kingdoms, scarring some deeply enough to inspire murder. As the Elrics find their course altered by these enemies and allies, their purpose remains unchanged - and their bond unbreakable.

	WATCHED_UNTIL=Watched season 1 (64 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Regarding visuals, the is one character that is most of the time visibly rendered in 3D, but given it's complexity, it is understandable. Although it stands out a bit, it is OK.

Regarding story, the creators have assumed that the viewer has seen `Fullmetal Alchemist (2003)` and is familiar with the story - therefore the first episode jumps straight into the action. You are going to hear "equivalent exchange a lot", and on this premise the whole plot stands, often questioning it and searching for shortcuts.

Some people recommend to first watch the original `Fullmetal Alchemist (2003)`, wait some time and then watch the remake `Fullmetal Alchemist: Brotherhood (2009)`. Some recommend to skip the first and go straight to the remake - which is what I did, and I am not disappointed.

My only regret is that I already knew some crucial parts of the story beforehand, which in a way ruined the experience. So my recommendation is, don't look it up beforehand. On the topic of original vs remake, should you watch both or which to watch first, I can't say since I have not seen the original one. Given how much of it is story-driven, I am not sure about re-watchability.

