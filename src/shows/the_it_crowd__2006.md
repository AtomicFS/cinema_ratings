{{#template
	../templates/show.md
	PUB_DATE=Thu, 04 May 2023 09:49:41 UTC
	MOVIE_NAME=The IT Crowd (2006)
	MOVIE_URL=https://www.imdb.com/title/tt0487831
	MOVIE_IMG=the_it_crowd__2006.jpg
	PLOT=At the UK company Reynholm Industries, their corporate high-rise towers are full of beautiful happy people with one success story after another. Well, except for the employees that work in the basement - the IT department. While their colleagues work in fantastic surroundings, Jen, Roy and Moss must work below ground in the dark and horrible basement, struggling to make it into normal society. The IT Crowd is a playful and somewhat surreal look at what it's really like to be the underclass of every company - the IT Department.

	WATCHED_UNTIL=Watched until season 5 (included, 4x6+1).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes
	
	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=0
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I just love it, it is a work of pure genius. Just brilliant. Perfection.

