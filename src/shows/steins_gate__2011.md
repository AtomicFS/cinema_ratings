{{#template
	../templates/show.md
	PUB_DATE=Sun, 11 Jun 2023 09:26:21 UTC
	MOVIE_NAME=Steins Gate (2011)
	MOVIE_URL=https://www.imdb.com/title/tt1910272
	MOVIE_IMG=steins_gate__2011.jpg
	PLOT=Steins;Gate is about a group of friends who have customized their microwave into a device that can send text messages to the past. As they perform different experiments, an organization named SERN who has been doing their own research on time travel tracks them down and now the characters have to find a way to avoid being captured by them.

	WATCHED_UNTIL=Watched season 1 (24 episodes) and OVA.

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes
	
	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=Maybe
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Not gonna lie, this one is a bit odd one. If you don't pay attention, you will get lost. I have understood it on second viewing. And what makes it a bit more difficult is that the English subtitles and English dubbing I have seen are sometimes confusing - I have watched both, and thankfully they complement each other (when one is confusing, the other makes more sense).

It is really good and captivating story and I really like it.

```admonish warning collapsible=true title="SPOILER WARNING"
What really got me confused the first time, was that the others were talking about alternate world-line while not possessing the `Reading Steiner` ability. Thankfully, it is explained towards the end that people can perceive memories from alternate world-lines via subconscious dreams, and thus having memories of more than one world-line.
```

