{{#template
	../templates/show.md
	PUB_DATE=Sun, 17 Apr 2022 20:54:41 UTC
	MOVIE_NAME=Erased (2016)
	MOVIE_URL=https://www.imdb.com/title/tt5249462
	MOVIE_IMG=erased__2016.jpg
	PLOT=Struggling manga author Satoru Fujinuma is beset by his fear to express himself. However, he has a supernatural ability of being able to prevent deaths and catastrophes by being sent back in time before the incident occurred, repeating time until the accident is prevented. One day, he gets involved in an accident that has him framed as a murderer. Desperate to save the victim, he sends himself back in time only to find himself as a grade-schooler one month before fellow classmate Kayo Hinazuki went missing. Satoru now embarks on a new quest: to save Kayo and solve the mystery behind her disappearance.

	WATCHED_UNTIL=Watched season 1 (12 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Another of my favorites, and as usual, with some heavy topics. Make sure to keep that in mind since it can hit you really hard.

I like how it flows, the story and music. Characters are cool and relatable.

```admonish warning collapsible=true title="SPOILER WARNING"
To be honest, I am not exactly sure about the ending. On one hand it makes sense and fits the story well, but on the other hand, it is a bit underwhelming. Especially the teacher part, that feels a bit undercooked.

The ending is not bad, but not great either - it is only good. Still, great watch.
```

