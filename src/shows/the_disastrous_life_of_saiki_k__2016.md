{{#template
	../templates/show.md
	PUB_DATE=Sat, 13 Aug 2022 10:07:53 UTC
	MOVIE_NAME=The Disastrous Life of Saiki K. (2016)
	MOVIE_URL=https://www.imdb.com/title/tt6354518
	MOVIE_IMG=the_disastrous_life_of_saiki_k__2016.jpg
	PLOT=Gifted with a wide assortment of supernatural abilities ranging from telepathy to x-ray vision, Kusuo Saiki finds this so-called blessing to be nothing but a curse. As all the inconveniences his powers cause constantly pile up, all Kusuo aims for is an ordinary, hassle-free life--a life where ignorance is bliss.

	WATCHED_UNTIL=Watched season 1.

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Feels a bot like combination of [One punch man](one_punch_man__2015.md) and [Kaguya-sama: Love Is War](kaguya-sama_love_is_war__2019.md).

Sometimes it is not clear if what Saiki says is only in his head or audible to other characters (at least in the ENG version, don't know about original).

Overall, good chill watch with great sense of humor.

