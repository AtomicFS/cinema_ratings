{{#template
	../templates/show.md
	PUB_DATE=Tue, 17 Oct 2023 23:19:12 UTC
	MOVIE_NAME=Raised by Wolves (2020)
	MOVIE_URL=https://www.imdb.com/title/tt9170108
	MOVIE_IMG=raised_by_wolves__2020.jpg
	PLOT=After Earth is ravaged by a great religious war, an atheistic android architect sends two of his creations, Mother and Father, to start a peaceful, godless colony on the planet Kepler-22b. Their treacherous task is jeopardized by the arrival of the Mithraic, a deeply devout religious order of surviving humans.

	WATCHED_UNTIL=Watched until season 2 (included; 10+8 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=No
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=Yes

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Just to start with, I must say that I hate this new streaming-service culture when each and every good show gets canceled. I wow to myself that this is a last show made by a streaming service, that I watch before it gets ending. From now on I will always wait until the show has ended to watch it. I am sick of this. Fuck you HBO.

Sorry about the rant, back to the review.

I really liked this show. It was refreshing to see something like this, it is kinda like a mixture of futuristic [Alien universe](../movies/alien__1979.md) and [Made in Abyss (2017)](made_in_abyss__2017.md).

At this time, I cannot recommend this show as it has no ending. There should have been season 3, but it was canceled. If there is ever a season 3, then definitely watch it.

```admonish warning collapsible=true title="SPOILER WARNING"
The only thing that I do not like (besides the cancellation) is some of the aesthetics. For example the serpent, in both of it's form is just weird and unnatural looking. Then the design of the tanks, they look just like spray-painted paper-box - compared to everything else it just stands out.
```

