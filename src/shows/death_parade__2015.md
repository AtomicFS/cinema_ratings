{{#template
	../templates/show.md
	PUB_DATE=Fri, 15 Apr 2022 13:40:00 UTC
	MOVIE_NAME=Death Parade (2015)
	MOVIE_URL=https://www.imdb.com/title/tt4279012
	MOVIE_IMG=death_parade__2015.jpg
	PLOT=Welcome to QuinnDecim. What greets two unsuspecting guests is a strange bar, QuinnDecim, and the white-haired bartender, Decim. From here you two shall begin a battle where your lives hang in the balance, he says to introduce the Death Game. Before long the guests' true natures become apparent. As a matter of course, at the game's end Decim is revealed to be the arbiter.

	WATCHED_UNTIL=Watched season 1 (12 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Oh my, this one is a true magnificent gem.

The first time I watched this anime I was fully sucked into the world and could not stop watching. Visuals are breath-taking, music is perfect fit. And the story, oh boy - it has this depth to it which is not fully explored, making it even more intriguing.

```admonish warning collapsible=true title="SPOILER WARNING"
The whole premise is more-less questioning humanity, morality, good and bad.

How would you decide if person is good or bad? If you push them hard enough, would they show their true colours? What if you push them too far? Where is the threshold of good and bad? How can you reduce person's life full of decisions and actions which fall into the gray area into binary question of good and bad? How do you determine this threshold?

This show is just presenting so many questions but no real answers, and I am simply in love with it. It has had real impact on my life and strongly resonates with me to this day.
```

