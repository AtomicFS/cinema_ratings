{{#template
	../templates/show.md
	PUB_DATE=Sat, 13 Aug 2022 10:48:02 UTC
	MOVIE_NAME=Kaguya-sama: Love Is War (2019)
	MOVIE_URL=https://www.imdb.com/title/tt9522300
	MOVIE_IMG=kaguya-sama_love_is_war__2019.jpg
	PLOT=Considered a genius due to having the highest grades in the country, Miyuki Shirogane leads the prestigious Shuchiin Academy's student council as its president, working alongside the beautiful and wealthy vice president Kaguya Shinomiya. The two are often regarded as the perfect couple by students despite them not being in any sort of romantic relationship.

	WATCHED_UNTIL=Watched until season 2 (24 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I love this anime.

Great and relatable characters, funny jokes and moments, stories and motives make sense. I do not have any complains.

Basically the entire premise is two ordinary high-school students have crush on each other, but they can't admit it publicly. And so they always plot and conspire against each other to force the other one to confess their affection.

Sure, there would no show if people were simply honest to each other, but oh boy it makes for a good show.

The ending of 2nd season is not really an ending, but there is going to be a next season.

