{{#template
	../templates/show.md
	PUB_DATE=Fri, 22 Apr 2022 16:32:51 UTC
	MOVIE_NAME=The Rising of the Shield Hero (2019)
	MOVIE_URL=https://www.imdb.com/title/tt9529546
	MOVIE_IMG=the_rising_of_the_shield_hero__2019.jpg
	PLOT=Iwatani Naofumi was summoned into a parallel world along with 3 other people to become the world's Heroes. Each of the heroes respectively equipped with their own legendary equipment when summoned, Naofumi received the Legendary Shield as his weapon. Due to Naofumi's lack of charisma and experience he's labeled as the weakest, only to end up betrayed, falsely accused, and robbed by on the third day of adventure. Shunned by everyone from the king to peasants, Naofumi's thoughts were filled with nothing but vengeance and hatred. Thus, his destiny in a parallel World begins...

	WATCHED_UNTIL=Watched season 1 (25 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=Sometimes
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I like it. I like the music, I like the story so far, I like the characters.

The 3D animations are not the best, but not too bad either. In addition, they are not used too often.

So far, I have seen only season 1. It has somewhat satisfying closure to it, although rather open-ended, but that is perfectly fine since season 2 is already in making. I am looking forward to it. It has left many questions unanswered, and I really hope to get some answers soon.

```admonish warning collapsible=true title="SPOILER WARNING"
The main reason for calling season's 1 end only somewhat satisfying is mostly because the king/trash and Malty/bitch have not yet learned their lesson nor really undergo any character-development yet. They do feel sorry, but for getting caught, not for their actions. But let's see what 2nd season will bring.
```

