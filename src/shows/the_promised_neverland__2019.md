{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 21:21:00 UTC
	MOVIE_NAME=The Promised Neverland (2019)
	MOVIE_URL=https://www.imdb.com/title/tt8788458
	MOVIE_IMG=the_promised_neverland__2019.jpg
	PLOT=A group of the smartest kids at a seemingly perfect orphanage uncover its dark truth when they break a rule to never leave the orphanage grounds. Once the truth is discovered, they begin to plan an escape to save all of the children.

	WATCHED_UNTIL=Watched season 1 (12 episodes). There is 2nd season (11 episodes), but I have not heard much good about it.

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Be prepared, this story is quite messed up, but great ... but messed up. On the surface, this orphanage seems like a paradise, however under the surface is horror.

The main characters are kids forced to play psychological games with their caretaker and other kids, trying to figure out who it on who's side, who they can trust and ultimately how to survive. Very intriguing and interesting, but probably not for everyone.

