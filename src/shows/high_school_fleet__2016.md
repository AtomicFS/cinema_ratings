{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 15:48:00 UTC
	MOVIE_NAME=High School Fleet (2016)
	MOVIE_URL=https://www.imdb.com/title/tt5567348
	MOVIE_IMG=high_school_fleet__2016.jpg
	PLOT=With Japan having developed as a maritime nation, it became necessary to have specialized schools to teach skills essential to protecting the sea. The setting is one such all-girls school. The girls who enroll here study to become blue mermaids, through classroom instruction and teaching of practical skills.<br><br><br><br>Blue mermaids are women who pursue the ocean for their professional occupation. They live by the motto In the sea we live, the sea we protect, and by the sea we go. They must excel in their studies, but must also adopt an international mindset. They are the Elite of the Sea, and are expected to be knowledgeable of the customs and cultures of other countries as well as their own.

	WATCHED_UNTIL=Watched season 1 (12 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=0
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=JPN
}}


I suppose you get what could be expected from a group a hight school girls piloting a battle ship. Most of the time chill and relax, with some light drama.

The plot does not make much sense, and there is not that much story to be told.

Overall, good watch for chill evening.

