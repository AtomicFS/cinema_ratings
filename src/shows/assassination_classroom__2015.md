{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 20:27:00 UTC
	MOVIE_NAME=Assassination Classroom (2015)
	MOVIE_URL=https://www.imdb.com/title/tt3837246
	MOVIE_IMG=assassination_classroom__2015.jpg
	PLOT=The students of class 3-E have a mission: kill their teacher before graduation. He has already destroyed the moon, and has promised to destroy the Earth if he can not be killed within a year. But how can this class of misfits kill a tentacled monster, capable of reaching Mach 20 speed, who may be the best teacher any of them have ever had?

	WATCHED_UNTIL=Watched until season 2 (included, 22+25 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


First of all, this anime starts up in very weird and make little to no sense. However, before the end, all loose ends are tied and closed, and the ending is great but a bit emotional.

The teacher, tentacled monster, under the strange exteriors is smart, wise and caring creature, and over time it grows up to you.

In short, while weird it is awesome.

