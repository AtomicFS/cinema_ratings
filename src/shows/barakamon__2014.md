{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 20:27:00 UTC
	MOVIE_NAME=Barakamon (2014)
	MOVIE_URL=https://www.imdb.com/title/tt3660104
	MOVIE_IMG=barakamon__2014.jpg
	PLOT=As a punishment for punching a famous Calligrapher, young handsome Calligrapher Handa Seishu is exiled on a small island. As someone who has never lived outside of a city, Handa has to adapt to his new wacky neighbors, like people traveling on a tractor, unwanted visitors who never use the front door, annoying kids using his home as a playground, etc. Can this city guy handle all the crazy hardships? Find out in this wacky island comedy full of innocence and laughter!!!

	WATCHED_UNTIL=Watched season 1 (12 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=0
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Some might say that it is quite repetitive, since the main character in each episode reverts back to depressed ball of mess. But in all honesty, in this way it is quite realistic portrait of depression and creative block. In this regard, it well demonstrates the lengthy and sometimes painful process of recovery.

Great story, full of humor and emotions. Often fluctuating between depression and happiness. I love it.

