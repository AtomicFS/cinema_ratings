{{#template
	../templates/show.md
	PUB_DATE=Fri, 27 May 2022 16:32:19 UTC
	MOVIE_NAME=Stargate SG1 (1997)
	MOVIE_URL=https://www.imdb.com/title/tt0118480
	MOVIE_IMG=stargate_sg1__1997.jpg
	PLOT=This sequel to the 1994 movie Stargate chronicles the further adventures of SGC (Stargate Command). It turned out that the Goa'uld Ra was only one of many alien System Lords who used the Stargates to conquer much of the universe. When Earth uncovers a working cartouche to decipher the coding system of their own Stargate, they find they can now travel anywhere. Earth's military sends out SG teams to explore new planets, find technology, and oppose the Goa'uld. Jack O'Neill and Daniel Jackson from the original movie are part of SG-1. They are joined by Sam Carter, a scientist, and Teal'c, a Jaffa who is convinced the Goa'uld are not gods.

	WATCHED_UNTIL=Watched until season 10 (included).

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=Yes
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Let me start by saying that this show is filled with American propaganda, and no wonder since it was made in collaboration with USA air force as means to recruit new people. With that being said, it creates surprisingly accurate military portrait regarding the tactics, equipment, slang, and so on (maybe not accurate all the time, but at least believable).

I will be blunt and direct - in my opinion the first season is practically unwatchable, second season has few watchable episodes, and starting with third season it is gradually getting better and better.

Especially in the beginning, there is established multitude of not well thought out ideas and principles that later cause story and logic problems, but as the show goes on, most of them get cleared up. There are more then few logical problems and loads of inconsistencies. Also occasional stupid moment or logic-defying decision making.

However, besides all of it's shortcomings, it is a good science fiction show which does many things rather well. It is entertaining, there are interesting concepts and ideas.

Overall, it is a decent watch.

