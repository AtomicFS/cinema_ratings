{{#template
	../templates/show.md
	PUB_DATE=Sat, 13 Aug 2022 11:56:22 UTC
	MOVIE_NAME=Snowflake Mountain (2022)
	MOVIE_URL=https://www.imdb.com/title/tt15292870
	MOVIE_IMG=snowflake_mountain__2022.jpg
	PLOT=Hopelessly entitled or simply in need of tough love? Ten spoiled young adults experience nature without a parental safety net in this reality series.

	WATCHED_UNTIL=Watched season 1 (8 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=N/A
	STORY_SMART=N/A
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I do have a bit of mixed feelings about this one. Not gonna lie, a lot of it comes from feeling that there is often lack of context to make it look more dramatic (or even justifiable).

Basically on one hand, I can totally see why those people needed this cold dose of reality, however on the other hand I fear that I could easily end up with them in such show xD.

Unfortunately, as usual for American reality TV show, it is overly dramatic, to the point that sometimes the drama artificially created just for sake of having drama.

It is a decent watch, especially because those snowflakes do over time undergo a personal growth.

