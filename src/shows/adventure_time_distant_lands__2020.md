{{#template
	../templates/show.md
	PUB_DATE=Mon, 18 Apr 2022 10:32:20 UTC
	MOVIE_NAME=Adventure Time: Distant Lands (2020)
	MOVIE_URL=https://www.imdb.com/title/tt11165358
	MOVIE_IMG=adventure_time_distant_lands__2020.jpg
	PLOT=Adventure Time has a sequel with new adventures around other worlds.

	WATCHED_UNTIL=Watched season 1 (4 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=Sometimes
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Well, it is not bad, but also not as good as the [original series](adventure_time__2010.md).

If I had not seen the original show, I might give it a better ratings. But since I have seen it, I am a bit disappointed. It feels like it has lost it's charm a became a bit more like main-stream. The original stories managed to mix goofy and serious topics on regular basis, however here it is just one or the other. This might have to do with the change of format from 10 minute shorts to 40 minute films.

