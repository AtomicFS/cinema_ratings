{{#template
	../templates/show.md
	PUB_DATE=Wed, 18 May 2022 20:30:55 UTC
	MOVIE_NAME=The Ancient Magus Bride (2016)
	MOVIE_URL=https://www.imdb.com/title/tt7326322
	MOVIE_IMG=the_ancient_magus_bride__2016.jpg
	PLOT=Hatori Chise is only 16, but she has lost far more than most. With neither family nor hope, it seems all doors are closed to her. But, a chance encounter began to turn the rusted wheels of fate. In her darkest hour, a mysterious magus appears before Chitose, offering a chance she couldn't turn down. This magus who seems closer to demon than human, will he bring her the light she desperately seeks, or drown her in ever deeper shadows?

	WATCHED_UNTIL=Watched season 1 (24 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Oh my glob, this anime is good.

I love the music, I love the story and characters. The first episode starts off strange, and it gets even stranger, but it is worth it. It has many mysteries, intriguing story telling, it has everything.

Sometimes, it feels a bit slow or heavily relying on dialog, but in my opinion it only makes it better by giving this contrast to action driven scenes. The are many beautiful scenes and touching side-stories, it is a gem.

```admonish warning collapsible=true title="SPOILER WARNING"
The one thing I am not sure about is the ending. In a way, there could be more, but honestly I can't think of any better way. It does feel a bit underwhelming, but it is fitting to the rest of the show.
```

