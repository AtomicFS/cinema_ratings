{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 20:27:00 UTC
	MOVIE_NAME=No Game No Life (2014)
	MOVIE_URL=https://www.imdb.com/title/tt3431758
	MOVIE_IMG=no_game_no_life__2014.jpg
	PLOT=This is a surreal comedy that follows Sora and Shiro, shut-in NEET siblings and the online gamer duo behind the legendary username Kuuhaku. They view the real world as just another lousy game; however, a strange e-mail challenging them to a chess match changes everything--the brother and sister are plunged into an otherworldly realm where they meet Tet, the God of Games.<br><br><br><br>The mysterious god welcomes the two to Disboard, a world where all forms of conflict are settled through high-stake games. This system works thanks to a fundamental rule wherein each party must wager something they deem to be of equal value to the other party's wager. In this strange land where the very idea of humanity is reduced to child's play, the indifferent genius gamer duo of Sora and Shiro have finally found a real reason to keep playing games: to unite the sixteen races of Disboard, defeat Tet, and become the gods of this new, gaming-is-everything world.

	WATCHED_UNTIL=Watched season 1 (12 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


While typical [isekai](https://www.anime-planet.com/anime/tags/isekai), it just strongly resonates with me. It does make me think of reality, daydreaming and reasons to live in the real world instead of dream. There are some questionable [ecchi](https://www.anime-planet.com/anime/tags/ecchi) scenes, but even so this anime is amazing.

To me, it was almost therapeutic, but you mileage might vary.

