{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 21:05:00 UTC
	MOVIE_NAME=Overlord (2015)
	MOVIE_URL=https://www.imdb.com/title/tt4869896
	MOVIE_IMG=overlord__2015.jpg
	PLOT=In the year 2138, virtual reality gaming is booming. Yggdrasil, a popular online game is quietly shut down one day. However, one player named Momonga decides to not log out. Momonga is then transformed into the image of a skeleton as the most powerful wizard. The world continues to change, with non-player characters (NPCs) beginning to feel emotion. Having no parents, friends, or place in society, this ordinary young man Momonga then strives to take over the new world the game has become.

	WATCHED_UNTIL=Watched until season 4 (included; 4x13 episodes).

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


On the surface it might seem like ordinary [isekai](https://www.anime-planet.com/anime/tags/isekai) anime, however under the hood it can be twisted and gruesome. Questioning morality, good, evil and existence, while also managing to have some fun.

The biggest flaw to this anime are the 3D animations sometimes used to create large quantities of entities on the screen (enemies, allies and so on). These 3D animations are some of the worst I have seen.

This might not be suitable for everyone, but if any of these properties are not problem, you will be rewarded with rich and extensive story. Especially if you pick up manga or light novels.

