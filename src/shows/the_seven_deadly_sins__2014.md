{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 15:04:00 UTC
	MOVIE_NAME=The Seven Deadly Sins (2014)
	MOVIE_URL=https://www.anime-planet.com/anime/the-seven-deadly-sins
	MOVIE_IMG=the_seven_deadly_sins__2014.jpg
	PLOT=The kingdom of Liones is thrown into turmoil when the king's Holy Knights are corrupted by a distinctly unholy demonic power. The land's only hope is a band of powerful warriors known as The Seven Deadly Sins - an exiled group of oddballs who seem to spend as much time fighting each other as they do fighting their enemies. Princess Elizabeth desperately seeks their aid, but the leader of her would-be saviors turns out to be an undersized pervert with a broken blade and a talking pig! They are soon joined by a jealous giant, a fussy fairy, and an immortal bandit pining over his lost love. Can these people really be the legendary heroes she's looking for?

	WATCHED_UNTIL=Watched until season 4 (included; 4x24 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Often nice and chill, but also mixed with action and [ecchi](https://www.anime-planet.com/anime/tags/ecchi).

It has some great concepts and characters, but the ever-present video-game mechanics always labeling someone or something with strength gets annoying. The show from the start presents overpower characters, and then there is little to no room for scaling up - probably the reason for constant stream of numbers of how much power who has. Also, the word "power" is by far the single most used word in this show.

Overall decent watch.

