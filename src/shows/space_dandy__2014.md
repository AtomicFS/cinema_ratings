{{#template
	../templates/show.md
	PUB_DATE=Mon, 19 Jun 2023 18:49:32 UTC
	MOVIE_NAME=Space Dandy (2014)
	MOVIE_URL=https://www.imdb.com/title/tt3158246
	MOVIE_IMG=space_dandy__2014.jpg
	PLOT=Space Dandy is a dandy guy in space! This dreamy adventurer, with a to-die-for pompadour, travels across the galaxy in search of aliens no one has ever laid eyes on. Each new species he discovers earns him a hefty reward, but this dandy guy has to be quick on his feet because it’s first come – first served! Accompanied by his sidekicks, a rundown robot named QT and Meow the cat-looking space alien, Dandy bravely explores unknown worlds inhabited by a variety aliens. Join the best dressed alien hunter in all of space and time as he embarks on an adventure that ends at the edge of the universe!

	WATCHED_UNTIL=Watched season 1

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=Maybe
	VIOLENCE=No
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Hilarious and generally awesome show. Each episode is unique and varies from hilarious over-the-top comedy all the way to sad and tragic, even existential sometimes.

If I am not mistaken, each episode was created by different artist - of course provided with some framework, but given a lot of freedom. This resulted in rather unique show with a lot of weird and awesome aliens.

Do not get discouraged by the seemingly superficial exterior of the show, it does have a depth to it, I promise.

```admonish warning collapsible=true title="SPOILER WARNING"
I am not exactly sure about the ending though. It certainly fits the whole theme, and Dandy refusing to take on god's position of the next universe is perfectly aligned with the character. But I am not sure about the whole theme there, reminds me too much of [Evangelion](neon_genesis_evangelion__1995.md).
```

