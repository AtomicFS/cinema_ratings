{{#template
	../templates/show.md
	PUB_DATE=Sat, 24 Jun 2023 11:20:59 UTC
	MOVIE_NAME=Grimgar of Fantasy and Ash (2016)
	MOVIE_URL=https://www.imdb.com/title/tt5341430
	MOVIE_IMG=grimgar_of_fantasy_and_ash__2016.jpg
	PLOT=Fear, survival, instinct. Thrown into a foreign land with nothing but hazy memories and the knowledge of their name, they can feel only these three emotions resonating deep within their souls. A group of strangers is given no other choice than to accept the only paying job in this game-like world - the role of a soldier in the Reserve Army - and eliminate anything that threatens the peace in their new world, Grimgar.<br><br>When all of the stronger candidates join together, those left behind must create a party together to survive: Manato, a charismatic leader and priest; Haruhiro, a nervous thief; Yume, a cheerful hunter; Shihoru, a shy mage; Mogzo, a kind warrior; and Ranta, a rowdy dark knight. Despite its resemblance to one, this is no game - there are no redos or respawns; it is kill or be killed.<br><br>It is now up to this ragtag group of unlikely fighters to survive together in a world where life and death are separated only by a fine line.

	WATCHED_UNTIL=Watched season 1 (12 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=Maybe
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Quite a interesting take on [isekai](https://www.anime-planet.com/anime/tags/isekai), quite grim dark theme with death and loss ... really promising start.

I do like the focus on monsters, showing their human-like qualities. This could in theory make for a really interesting story plot.

Looking forward to season 2.

