{{#template
	../templates/show.md
	PUB_DATE=Tue, 24 Oct 2023 22:35:49 UTC
	MOVIE_NAME=Lost in Space (2018)
	MOVIE_URL=https://www.imdb.com/title/tt5232792
	MOVIE_IMG=lost_in_space__2018.jpg
	PLOT=After crash-landing on an alien planet, the Robinson family fights against all odds to survive and escape. But they're surrounded by hidden dangers.

	WATCHED_UNTIL=

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}

Well, visuals are great, sound-track is good too, but the story and story-telling are mediocre at best. Characters also quite often feel very dumb. More on story and characters in spoiler section.

Then there is the science part, sadly. According to Burk Sharpless:
> The philosophy for when we were coming up with this particular science fiction story is that we were very interested in science and research and an experience rooted in realism and authenticity.

I am sorry to inform you, but you have failed on quite a few very basic scientific principles, often just to increase the drama. See spoiler section for mode details.

There is just this huge contrast, some things are good, but then there is the occasional brain-fart coming out of nowhere for no reason.

Overall, very average show.


```admonish warning collapsible=true title="SPOILER WARNING"
Dr Smith is quite good character. I must say, she surprised me more than once how managed to weasel out of very tight spots. Yes sometimes I would prefer a little bit more - for example when she bangs Maureen Robinson (mother) over her head with pipe to get help with the robot, claiming that she did not know about the critical function of Maureen at mission control. I believe this to be the case and as such, it should have been a little bit more apparent. But that is a minor detail.

### Season 1
Let me start with the characters. For the most part, they seem to be pretty good, and making sound decisions. However as the time progresses, they get dumber.

For example **S01-E03**: John Robinson (father) hunts the eels, but yet while wrangling with one of them decides to make few phone-calls. WTF? Why? He could easily wait 5 seconds to kill it with knife and then make the calls, there was nothing urgent there! This is just plain stupid.

But what really trigger me was in S01-E06, when Don West (the smuggler / mechanic) just watches for 3 hours fuel being drained from his crashed Jupiter, just to suddenly realize the last sends he wants to go inside and discover truth about Dr Smith. Since there is no more fuel to act as counterweight, the ship is about to fall. Yet he barges in, looks though pictures and stuff, and then proceeds to "sacrifice himself" but yet survives miraculously with this stupid cliche shot at the cliff and sudden "surprise" hand reaching grabbing onto said cliff. I have almost hat my pants with fury on this scene. I have not seen so much concentrated un-ironic bullshit in a long time.

Then in the next episode (**S01E07**) there is this "drama" around fuel tank crushing one guy ... the visuals there are terrible. The guy can clearly slip out without problems, the surrounding terrain was made to prop the tank ... it just feels lazy at best. I see their story point, and it is fine, just the execution is poor.


### Season 2
**S02-E02**: Each ship has audio signature (sonic watermark) ... audio ... in space ... where there is a vacuum and ano way for sound to propagate ... wtf, who wrote this bullshit? And before you start screaming at me that it could be audio-file transmitted over radio, well, that is just as stupid. That would be like writing a e-mail, but instead of sending it, you would print it out, take a shitty picture with a 0.3 MPx camera on old feature phone and then send it. And if you do not see anything wrong with that, you are the reason soap has instructions on packaging.

Another thing, Maureen Robinson (mother) jumping into the water to save her daughter from from the fall, with no life-line or rope is completely just a [dumb way to die](https://www.youtube.com/watch?v=IJNR2EpS0jw). That is literally as useful as trying to save someone from being hit by truck by jumping in front of the truck.

**S02-E06**: Regarding the cargo box for rescue - fisrt of all, if would have insulation, making it slow to cool. Second of all, space is cold, but there is almost no matter there, meaning only way to lose heat is via radiation, which is slow. It is not like you dump the cargo container into a pool of liquid nitrogen. Satellites and spacecraft have actually problems to cool down their heat-producing components like computers because vacuum of space is the perfect heat insulator. So this whole thing is complete and utter bullshit. Even if they took out all the insulation, I seriously doubt they would freeze, as matter of fact I would argue that they might get barbecued by the sunlight.

According to some [smart people on the internet](https://physics.stackexchange.com/questions/67503/how-fast-would-body-temperature-go-down-in-space), without the box and without sunlight, you would go into hypothermia after around 12minutes! The insulated box with other 3 people in it will make this time even longer.

Also, I am triggered by the fire extinguisher. The thrust is applied by one extinguisher, making it completely of the center of mass. As result, the box would just spin on the spot. You need at least 2 fire extinguishers placed on the opposite sides of the box. Because you will likely not place then accurately enough, you will still end up spinning a bit (depends on the offset), but you should get some thrust forward. The trajectory will be curved, but it will get you away from the wreck.

**S02-E08**: The open airlock with people in it ... well, first of all, in such small space, the pressure would equalize to the outside almost immediately. Earth's atmosphere has normally, as sea level, pressure of (1 bar or 14.7 PSI). During the descent, they call out pressure of 282 PSI (19.4 bar).

Now let's compare this to deep diving. 282 PSI is equivalent to [dive into depth of 2865 meters](https://www.omnicalculator.com/physics/hydrostatic-pressure)! This was during the descend, so it likely got bigger. According to [wikipedia](https://en.wikipedia.org/wiki/Deep_diving), the record for diving, with special breathing gas mixture (dues to a toxicity of oxygen and nitrogen at high pressure), is 534 meters. So this already is very unrealistic.

And then to top it up, they pull them out into normal atmosphere, which will definitely kill them because of [Decompression sickness](https://en.wikipedia.org/wiki/Decompression_sickness). The best course of action would be to close them off and slowly decrease the pressure down to 1 bar over couple of hours. And that is assuming that they would survive the high pressure in the first place.

**S02-E10**: Dr Smith dies which feels a bit wasteful.


### Season 3
At this point I am just tired to point every little thing out. So I will be brief.

**S03-E01**: Dr Smith is alive by the power of plot-armor or something. It actually never gets explained.

**S03-E08**: People start rescuing robots left and right and they get converted into friendly robots, because why not?! Robot sacrifices himself for Will, but it just feels empty. And of course the robot gets resurrected at the end, because we are PG13 or some other bullshit.


### Conclusion
With this many scientific and factual mistakes, some of which are elementary-school level, the show fails miserably.

Regarding characters, they are OK. Robinson's feel rather like too over-the-top cliche family with stuff like "Robinsons stick together" and "we are Robinsons, we can do anything".

One of the most interesting characters is Dr Smith, but she flatlines on character development in the middle of the show and more-less stays the same. Her actions in S02-E08 and later also feel a little too artificial. Not like they are coming from her conviction, but because it is expected from her. Just feels cheap and wasted opportunity, they could have developed the character more.
```

