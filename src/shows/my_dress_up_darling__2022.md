{{#template
	../templates/show.md
	PUB_DATE=Sun, 17 Apr 2022 15:10:58 UTC
	MOVIE_NAME=My Dress-Up Darling (2022)
	MOVIE_URL=https://www.imdb.com/title/tt15765670
	MOVIE_IMG=my_dress_up_darling__2022.jpg
	PLOT=Wakana Gojo is a high school boy who wants to become a kashirashi - a master craftsman who makes traditional Japanese Hina dolls. Though he's gung-ho about the craft, he knows nothing about the latest trends, and has a hard time fitting in with his class. The popular kids - especially one girl, Marin Kitagawa - seem like they live in a completely different world. That all changes one day, when she shares an unexpected secret with him, and their completely different worlds collide.

	WATCHED_UNTIL=Watched season 1 (12 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


A little bit too [ecchi](https://www.anime-planet.com/anime/tags/ecchi) for my taste, sometimes bordering the [NSFW](https://www.urbandictionary.com/define.php?term=NSFW). Besides that, it has decent story, music and touching moments.

It made me chuckle more than once, and the story kept me tied to the screen and as result I [binge-watched](https://www.urbandictionary.com/define.php?term=binge+watch) it whole.

Certainly not my usual type, but it is good for afternoon chill and relax.

