{{#template
	../templates/show.md
	PUB_DATE=Sat, 03 Sep 2022 07:38:14 UTC
	MOVIE_NAME=Made in Abyss (2017)
	MOVIE_URL=https://www.imdb.com/title/tt7222086
	MOVIE_IMG=made_in_abyss__2017.jpg
	PLOT=The "Abyss" is the last unexplored region, an enormous and treacherous cave system filled with ancient relics and strange creatures. Only the bravest of adventurers can traverse its depths, earning them the nickname, "Cave Raiders". Within the depths of the Abyss, a girl named Riko stumbles upon a robot who looks like a young boy. Tantalized by the Abyss, Riko and her new friend descend into uncharted territory to unlock its mysteries, but what lies in wait for them in the darkness?

	WATCHED_UNTIL=Watched season 1 and movie sequel `Made in Abyss: Dawn of the Deep Soul`. Show is still going on.

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=N/A
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=4
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=Maybe
	EROTIC=Maybe
	SEX=Yes

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


First of all, this show is a lot about about children, but certainly not for children. There are some seriously fucked-up things there (out of all the movies and shows on this page, this one is the most fucked up at the time of writing, first to rate `4` on the fucked-up scale).

Great characters, they have depth to them. Great music, great world-building, overall great.

The only flaw so far might be the re-watchability. The first time I saw this show, I was so captivated and often in shock from the events (often also mind-blown). The second time I already knew what is coming, but I still enjoyed it.

It is still going on, the second season is already out, English dubbing is about to release first episode any day now. So let's see what the creator will bring. I am certainly looking forward to it!

```admonish warning collapsible=true title="SPOILER WARNING"
My favorite character so far is Ozen. When I look at her, I see her a good person, but has no filter and says everything as is without restrains, which can be easily misinterpreted.

Another very interesting character is Bondrewd, although I would hesitate to call him my favorite. There is a thing called [uncanny valley](https://en.wikipedia.org/wiki/Uncanny_valley), which is according to Wikipedia:

> Uncanny valley is relation between an object's degree of resemblance to a human being and the emotional response to the object.
>
> The concept suggests that humanoid objects that imperfectly resemble actual human beings provoke uncanny or strangely familiar feelings of uneasiness and revulsion in observers.

I feel like Bondrewd perfectly fits the description. It is said that ascend from 6th layer causes loss of humanity - and when we look at Nanachi or Mitty, it is more of physical change, but with Bondrewd, it looks more like psychological matter.

This guys character (in my opinion) perfectly fits someone who is essentially immortal. More less, many things become pointless and yet he is full of childlike wonder and desire to explore. He knows good from bad, but looks at it from different perspective. Taking in the orphans and conducting experiments on them feels like good thing from his perspective, since he is giving a purpose to their meaningless lives???? I don't know, but that is why I consider him as so intriguing character, because in my opinion this guy is the best depiction I have seen so far as what would immortality do to a person. Fascinating.
```

