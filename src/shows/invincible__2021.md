{{#template
	../templates/show.md
	PUB_DATE=Tue, 20 Jun 2023 18:43:46 UTC
	MOVIE_NAME=Invincible (2021)
	MOVIE_URL=https://www.imdb.com/title/tt6741278
	MOVIE_IMG=invincible__2021.jpg
	PLOT=Mark Grayson is a normal teenager, except for the fact that his father is the most powerful superhero on the planet. Shortly after his seventeenth birthday, Mark begins to develop powers of his own and enters into his father’s tutelage.

	WATCHED_UNTIL=Watched season 1 (8 episodes).

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=No
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


First of all, the animation is quite awkward, specially when it comes to movements and gestures. It feels quite rather unnatural. Also, the occasional 3D rendering kinda sucks.

A bit of a warning, there is a lot a blood and guts and brains, and I mean it.

Overall, good watch. I do hope there will be season 2.

