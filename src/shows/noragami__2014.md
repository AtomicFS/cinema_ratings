{{#template
	../templates/show.md
	PUB_DATE=Mon, 14 Mar 2022 17:55:00 UTC
	MOVIE_NAME=Noragami (2014)
	MOVIE_URL=https://www.imdb.com/title/tt3225270
	MOVIE_IMG=noragami__2014.jpg
	PLOT=Yato, a minor god, dreams to become the most revered deity in the world with big shrine and all. However, being a penniless god that he is, he has a long way to go and his shinki leaving him doesn't help matter. Later, when he does his job, a high school girl named Hiyori Iki pushes him out of the way of a speeding bus; unaware of his status as deity.<br><br><br><br>Things get complicated when this incident triggers abnormality in Hiyori's soul, causing her to make an Astral Projection whenever she falls unconscious. To turn herself back to normal, Hiyori must help Yato becoming a better god who can fix the problem, along with Yato's new shinki, Yukine.

	WATCHED_UNTIL=Watched until season 2 (included; 12+13 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=Sometimes
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


This one is kinda hard to describe. It is a lot about people, relationships, meaning of family and what is important in life.

I might have preferred the ending to be not as open, but it is still good ending. You can skip the after-credit scene of the last episode to not have open end ;)

To put it simply, it is good.

