{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 15:48:00 UTC
	MOVIE_NAME=Elfen Lied (2004)
	MOVIE_URL=https://www.imdb.com/title/tt0480489
	MOVIE_IMG=elfen_lied__2004.jpg
	PLOT=The diclonius, otherwise known as a two-horned human, are mutants of the human species, and may well be the next step in human evolution. The diclonius have horns and strong telekinetic powers represented by vectors. However with this great power, they can easily destroy the human race. Fearful of their power, humans quarantined the diclonius into secret research facilities to study. However, in a freak accident, a enraged female diclonius escaped, killing many guards in the process. While escaping, she is shot in the head, and falls into the ocean. As a result she has waves of amnesia. Later, she washes ashore. There, she meets two people named Kouta and Yuka, who name the female diclonius Nyuu, after the only word she can say. They decide to watch after her, but of course, the research facility can't have this, and begin trying to retrieve her by any means necessary.

	WATCHED_UNTIL=Watched season 1 (13 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=3
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Well, this show is certainly not for everyone. It is showing some raw emotions, suppressed memories and traumatic events. The show has also a great way to show contrasting paradoxes and twisted reality. It is full of very twisted toe curling scenes.

As for the ending, I would have wisher for something a bit more concrete. It is a bit vague, but to be honest not bad at all.

Overall, it is a good watch, but prepared for some heavy stuff.

