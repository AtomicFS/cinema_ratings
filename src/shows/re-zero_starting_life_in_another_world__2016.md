{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 15:04:00 UTC
	MOVIE_NAME=Re:ZERO Starting Life in Another World (2016)
	MOVIE_URL=https://www.imdb.com/title/tt5607616
	MOVIE_IMG=re-zero_starting_life_in_another_world__2016.jpg
	PLOT=Shortly after being summoned to a new world, Subaru Natsuki and his new female companion are brutally murdered. But then he awakes to find himself in the same alley, with the same thugs, the same girl, and the day begins to repeat.

	WATCHED_UNTIL=Watched until season 2 (included; 2x25 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Maybe
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Not bad, but many things in the show did not make much sense to me. One of the reasons might be that many characters multiple names and/or nicknames, which is very confusing.

I like the concepts and characters, they seem to be fleshed out well. I like that not everything is black and white.

I would like to see the show once more, and then see what I think of it and if I manage to understand more.

