{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 14:41:00 UTC
	MOVIE_NAME=That Time I Got Reincarnated as a Slime (2018)
	MOVIE_URL=https://www.imdb.com/title/tt9054364
	MOVIE_IMG=that_time_i_got_reincarnated_as_slime__2018.jpg
	PLOT=37-year-old corporate worker Mikami Satoru is stabbed by a random killer, and is reborn to an alternate world. But he turns out to be reborn a slime! Thrown into this new world with the name Rimuru Tempest, he begins his quest to create a world that's welcoming to all races. Broken free from ordinary, stale past life, his fresh adventure in a fantasy world as a slime monster with unique abilities begins.

	WATCHED_UNTIL=Watched until season 2 (included; 2x24 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Classic [isekai](https://www.anime-planet.com/anime/tags/isekai) anime with overpowered main character, however I don't see it as a problem. It does show some video-game mechanics, but not too much. The main character is charming and relatable. The show if full of humor, cliffhangers and surprises.

Overall it is enjoyable watch.

