{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 15:48:00 UTC
	MOVIE_NAME=Gravity Falls (2012)
	MOVIE_URL=https://www.imdb.com/title/tt1865718
	MOVIE_IMG=gravity_falls__2012.jpg
	PLOT=Twin brother and sister Dipper and Mabel Pines are in for an unexpected adventure when they spend the summer helping their Great Uncle Stan run a tourist trap in the mysterious town of Gravity Falls, Oregon.

	WATCHED_UNTIL=Watched until season 2 (included; 20+20 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Oh boy, where should I start ... this show is in my opinion amazing.

I love the story, the intro song is great, characters are well written, and in the end everything ties together in excellent ending. This show makes you feel like you just spent a summer holiday in Gravity Falls (the place where story takes place). In a way, it is sad that it is so short and you would wish for it to last longer, but then it would loose it's charm.

Overall, I have practically nothing I would change about this show. I dare to say it is nearing perfection.

```admonish warning collapsible=true title="SPOILER WARNING"
Mabel does go though this as well (wishing for longer summer), giving awesome meta-commentary.
```

