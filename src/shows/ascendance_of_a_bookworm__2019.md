{{#template
	../templates/show.md
	PUB_DATE=Sat, 21 May 2022 11:50:06 UTC
	MOVIE_NAME=Ascendance of a Bookworm (2019)
	MOVIE_URL=https://www.imdb.com/title/tt10885406
	MOVIE_IMG=ascendance_of_a_bookworm__2019.jpg
	PLOT=Avid bookworm and college student Motosu Urano ends up dying in an unforeseen accident. This came right after the news that she would finally be able to work as a librarian as she had always dreamed of. When she regained consciousness, she was reborn as Myne, the daughter of a poor soldier. She was in the town of Ehrenfest, which had a harsh class system. But as long as she had books, she didn't really need anything else. However, books were scarce and belonged only to the nobles. But that doesn't stop her, so she makes a decision... If there aren't any books, I'll just create some.

	WATCHED_UNTIL=Watched until season 3 (included; 14+12+10 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=Sometimes
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Simply said: love at first sight.

I love this anime, since the first episode I was just glued to the screen and could not walk away.

With that being said, it is not just unicorns and rainbows. There is still the fact that adult woman called Motosu Urano was more-less forcefully placed into body of young girl Myne. This raises many questions like: what happened to Myne, is she ok, will she ever return, ...

None of the questions regarding the original Myne are answered. There is only one moment when the topic is brought up and it is rather brief and unsatisfactory. While everything looks awesome a great, in the back of my mind I hope it will not turn into nightmare.

On the bright side, the characters are relatable (especially the main character Motosu Urano / Myne). Her uncontrollable passion and desire is something I can identify with. To see the resolve and determination is motivating.

After watching the first season, I still feel like the shows is just starting. There is so much potential and so many opened themes and topics, I can't wait what will come.

