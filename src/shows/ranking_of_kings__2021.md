{{#template
	../templates/show.md
	PUB_DATE=Mon, 24 Oct 2022 18:19:04 UTC
	MOVIE_NAME=Ranking of Kings (2021)
	MOVIE_URL=https://www.imdb.com/title/tt13409432
	MOVIE_IMG=ranking_of_kings__2021.jpg
	PLOT=Unable to hear, speak, or wield a sword, Prince Bojji doesn't seem like a typical heir to the throne--and his kingdom agrees. But his fateful encounter with Kage, a shadow on the ground, gives him his first true friend. The two set off on a grand adventure and, together, form a bond that can overcome any obstacle...even being king.

	WATCHED_UNTIL=Watched season 1 (23 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=3
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


When going in, I have no idea what to expect. While this looks a lot like kid-show, it is not. There are some heavy topics that can hit you hard. But there is some really awesome stuff in there too, which made a lasting impact on me.

It can make you chuckle, it might make you cry. Overall decent watch, would recommend.

The ending feels a bit open, so I guess we can expect more seasons.


```admonish warning collapsible=true title="SPOILER WARNING"
One thing that will stay with me for a while is the scene when Daida sees this blind man.

Here is a excerpt from the conversation (episode 11, right after opening scene when Daida goes to town).

> *Bebin:* Prince Daida, I found one of the weaklings you spoke of. One much worse off than Prince Bojji.
>
> *Daida:* Worse off than Brother?
>
> *Bebin:* Not only is that man deaf, he is also blind.
>
> *Daida:* Why do people like that even exist? It's unfair, but worst of all, it's pitiable. If that were me, I would end my life in an instant.
>
> *Bebin:* Are you saying that under the same circumstances, you wouldn't have the self-confidence to go on living? If so, then he is a strong man to have overcome his plight and continue living.
>
> *Daida:* That's just doublespeak!
>
> *Bebin:* I'm sure he doesn't even think about it himself.

I found this to be truly thought-provoking. Sure, it is not a novel idea, but still I did not expect it here.

**WARNING: Philosophical rant incoming (also trigger warning)**

This also reminds me of something else I have heard before (not sure where):

> Handicap is acceptable only if exploitable.

This points to a bit darker side of society. Unfortunately this is a rather common theme in stories with disabled / handicapped people. While they might be handicapped in some way, they almost always have some "super-power".

Please, do not take me wrong, I appreciate the narrative of finding one's strong sides or talents, but why these characters have to always be this way? Are we supposed to help them and care for them only because they are useful to our society?

What if someone is useless to the society in every directly measurable way? Generally speaking creating net positive monetary value.

The "uselessness" does not have to be caused by disability, there are multitude of other reasons such as illness, psychological trauma, depression and so on. Should we not take care of these people too, just out of the goodness of our hearts without expecting anything in return?
```

