{{#template
	../templates/show.md
	PUB_DATE=Sun, 20 Mar 2022 08:48:00 UTC
	MOVIE_NAME=Blackadder (1986)
	MOVIE_URL=https://www.imdb.com/title/tt0084988
	MOVIE_IMG=blackadder__1986.jpg
	PLOT=Cunning plans and cutting comedy as the Blackadder dynasty plot their way through British history.

	WATCHED_UNTIL=Watched until season 4 (included; 4x6 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=No

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=No

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Great show that aged quite well. Based all around the worst human attributes and emotions embodied in the main character who is greedy, spinelessness, egocentric and malevolent. Great watch full of British humor.

Although I recommend to skip the first season, while decent on it's own, it does not match the rest of the show. Don't worry, you don't need to watch it in order to understand the rest - each season is quite independent.

The creators wanted to make something unique and something going against the norm at the time. The first season was imperfect prototype that almost got the whole show cancelled, but they learned from their mistakes and they knew what was the secret formula. Starting with season number two, the real fun begins.

Instead of grandiose and breathtaking sets, they downgraded to simple and small interiors. The number of characters was cut to bare minimum and each episode was performed before live audience (the laughs are genuine). And this then worked perfectly - excellent recipe for three seasons of fun.

