{{#template
	../templates/show.md
	PUB_DATE=Sat, 13 Aug 2022 09:10:33 UTC
	MOVIE_NAME=Record of Ragnarok (2021)
	MOVIE_URL=https://www.imdb.com/title/tt13676344
	MOVIE_IMG=record_of_ragnarok__2021.jpg
	PLOT=Before eradicating humankind from the world, the gods give them one last chance to prove themselves worthy of survival. Let the Ragnarok battles begin.

	WATCHED_UNTIL=Watched season 1.

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


The start felt a bit slower, but the it picked up the speed. Also, it feels a bit distant, there are not that many relatable characters (maybe one or two later in the first season).

Overall, not a bad start, but let's see what next season brings in.

