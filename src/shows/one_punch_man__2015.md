{{#template
	../templates/show.md
	PUB_DATE=Fri, 28 Oct 2022 18:23:32 UTC
	MOVIE_NAME=One Punch Man (2015)
	MOVIE_URL=https://www.imdb.com/title/tt4508902
	MOVIE_IMG=one_punch_man__2015.jpg
	PLOT=Saitama is a superhero who has trained so hard that his hair has fallen out, and who can overcome any enemy with one punch. However, because he is so strong, he has become bored and frustrated that he wins all of his battles too easily.

	WATCHED_UNTIL=Watched until season 2 (included, 2x12 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=N/A

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I love this show, especially first season. I have re-watched this show many times.

The music and soundtrack are amazing, the story is superb and original, characters are awesome. I have absolutely no complains for first season. Second season felt a bit weaker in comparison, but it is still pretty good. It feels like the first season was more of an introduction, while second went deeper into philosophical territory.

Overall, great show.

