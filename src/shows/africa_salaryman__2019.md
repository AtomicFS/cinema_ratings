{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 14:14:00 UTC
	MOVIE_NAME=Africa Salaryman (2019)
	MOVIE_URL=https://www.imdb.com/title/tt11157604
	MOVIE_IMG=africa_salaryman__2019.jpg
	PLOT=The comedy follows a lion, toucan, and lizard as they live the lives of office workers in a capitalist society in Japan, while also dealing with their unique situations as animals living beyond the savanna and the food chain.

	WATCHED_UNTIL=Watched season 1 (12 episodes).

	VISUAL_WATCHABLE=No
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Maybe
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=0
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=Yes
	VIOLENCE=Maybe
	DRUGS=No
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Content-wise, it is not bad, making fun of corporate environment. Unfortunately they mixed 2D and 3D in the worst possible manner, making it barely watchable.

Besides the visuals, it is rather average show.

