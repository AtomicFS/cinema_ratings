{{#template
	../templates/show.md
	PUB_DATE=Sat, 13 Aug 2022 13:47:48 UTC
	MOVIE_NAME=For All Mankind (2019)
	MOVIE_URL=https://www.imdb.com/title/tt7772588
	MOVIE_IMG=for_all_mankind__2019.jpg
	PLOT=Explore an aspirational world where NASA and the space program remained a priority and a focal point of our hopes and dreams as told through the lives of NASA astronauts, engineers, and their families.

	WATCHED_UNTIL=Watched until season 2 (included).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


In short: heavily political, decent chunk of drama, a lot of USA good and Russia bad.

Sometimes it is a bit painful to watch, especialy when people are making just bad ilogical decisions.

Overall, decent watch do far.

