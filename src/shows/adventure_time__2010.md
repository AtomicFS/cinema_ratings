{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 14:14:00 UTC
	MOVIE_NAME=Adventure Time (2010)
	MOVIE_URL=https://www.imdb.com/title/tt1305826
	MOVIE_IMG=adventure_time__2010.jpg
	PLOT=Adventure Time follows two best friends: Finn (a 12-year old boy) and Jake (a wise 28-year-old dog with magical powers), and the surreal adventures undertaken by the duo as they traverse the mystical Land of Ooo. A world built for adventure, Ooo is filled to the brim with various landscapes for the two buddies to explore and bizarre characters to assist.

	WATCHED_UNTIL=Watched until season 10 (included).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


At the beginning, the stories are short and independent, however later in second half of second season it takes off and a deeper underlying story is emerging.

Very nice show, slice of life of various characters, with deeper meaning under the hood. It has good music, funny stories and jokes, most of the time quite cheerful.

In the beginning it looks like kid's show, but in reality it fluctuates between lighthearted topics and some heavy stuff.

Overall great.

