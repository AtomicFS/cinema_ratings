{{#template
	../templates/show.md
	PUB_DATE=Sun, 22 May 2022 18:43:44 UTC
	MOVIE_NAME=Stargate Universe (2009)
	MOVIE_URL=https://www.imdb.com/title/tt1286039
	MOVIE_IMG=stargate_universe__2009.jpg
	PLOT=The previously unknown purpose of the ninth chevron is revealed and takes a group of refugees on a one-way trip to a millions of years old Ancient-built ship. Led by Dr. Nicolas Rush and Colonel Everett Young, the refugees are trapped on the ship, unable to change its programmed mission.

	WATCHED_UNTIL=Watched until season 2 (included; 2x20 episodes).

	VISUAL_WATCHABLE=Maybe
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=Maybe
	EROTIC=Yes
	SEX=Yes

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


First of all, this show does not really fit into the rest of Stargate world, however it is good.

I really like the underlying story and mystery, and it has a good music and awesome soundtrack. Characters are also good for the most part, but there is far too much focus on drama rather then the science-fiction and mystery. While the visuals and CGI are great, the shaky camera is annoying.

The story of the ship and the mystery of it's existence are very intriguing and fascinating. Just the thought of it sends shivers down my spine (in a good way). There should have been more of it.

```admonish warning collapsible=true title="SPOILER WARNING"
As for the ending, I do like it, but it is left open. Although, it is actually well fitting the the entire theme - the ancients have launched Destiny millions of years ago, it might as well take another millions of years to get the final answer.

With that in mind, the people are just a short-lived temporary crew in the long long history of Destiny.
```

