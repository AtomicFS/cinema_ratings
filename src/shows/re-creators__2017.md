{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 14:14:00 UTC
	MOVIE_NAME=Re:Creators (2017)
	MOVIE_URL=https://www.imdb.com/title/tt6755926
	MOVIE_IMG=re-creators__2017.jpg
	PLOT=Humans have created many stories. Joy, sadness, anger, deep emotion. Stories shake our emotions, and fascinate us. However, these are only the thoughts of bystanders. But what if the characters in the story have &quot;intentions&quot;? To them, are we god-like existences for bringing their story into the world? Our world is changed. Mete out punishment upon the realm of the gods. In Re:CREATORS, everyone becomes a Creator.

	WATCHED_UNTIL=Watched season 1 (22 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Maybe
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=Maybe

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Maybe
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Maybe


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=Maybe
	EROTIC=Maybe
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=JPN
}}


I really liked the soundtrack. It was a bit repetitive, but it was good and resonated well with the story-telling and fit action on the screen.

Story-wise, I like the meta-feeling to it, but in a way I hoped for a bit more. The plot reminded me of [SCP-3812](https://scp-wiki.wikidot.com/scp-3812), and so I hoped that the plot might touch on a topic of stacked narratives, but that unfortunately did not happen.

Overall, it was enjoyable.

