{{#template
	../templates/show.md
	PUB_DATE=Wed, 03 May 2023 11:48:14 UTC
	MOVIE_NAME=Black Books (2000)
	MOVIE_URL=https://www.imdb.com/title/tt0262150
	MOVIE_IMG=black_books__2000.jpg
	PLOT=Black Books centres around the foul tempered and wildly eccentric bookshop owner Bernard Black. Bernard's devotion to the twin pleasures of drunkenness and wilful antagonism deepens and enriches both his life and that of Manny, his assistant. Bearded, sweet and good, Manny is everything that Bernard isn't and is punished by Bernard relentlessly just for the crime of existing. They depend on each other for meaning as Fran, their oldest friend, depends on them for distraction. Black Books is a haven of books, wine and conversation, the only threat to the group's peace and prosperity is their own limitless stupidity.

	WATCHED_UNTIL=Watched until season 3 (included).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=No
	
	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=Maybe
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Funny and quirky sitcom from badly managed book store. I like it, it makes me laugh. Quite decent comedy.

Would recommend.

