{{#template
	../templates/show.md
	PUB_DATE=Sat, 19 Feb 2022 14:14:00 UTC
	MOVIE_NAME=Parasyte -the maxim- (2014)
	MOVIE_URL=https://www.imdb.com/title/tt3358020
	MOVIE_IMG=parasyte_the_maxim__2014.jpg
	PLOT=A species of parasitic aliens descends on Earth and quickly infiltrates humanity by entering the brains of vulnerable targets; insatiable beings that gain total control of their host and are capable of transforming themselves to feed on unsuspecting prey. High school student Shinichi Izumi falls victim to one of these parasites, but the creature fails to take over his brain and ends up in his right hand.

	WATCHED_UNTIL=Watched season 1 (24 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Yes
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=No
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Intriguing story with heavy philosophical undertones. The music is sometimes repetitive, sometimes it makes very little sense, however it is well made story, with interesting characters.

I like how the anime tries to look at humans and entire civilization from another perspective. Contemplating what is means to be human. The main character is thrown into abnormal situation, which forces him to re-evaluate hit perspective of the outside world and himself. Over the course of the anime, the main character undergoes a journey to understand himself, and grows as person.

It is quite often gruesome, full of blood, guts and gore everywhere, but overall great watch if you can get over that.

