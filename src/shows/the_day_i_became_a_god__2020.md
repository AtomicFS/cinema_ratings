{{#template
	../templates/show.md
	PUB_DATE=Fri, 15 Apr 2022 11:30:00 UTC
	MOVIE_NAME=The Day I Became a God (2020)
	MOVIE_URL=https://www.imdb.com/title/tt12305588
	MOVIE_IMG=the_day_i_became_a_god__2020.jpg
	PLOT=At the end of the last summer vacation of high school, Youta Narukami spends his days preparing for the university entrance exams, when a young girl named Hina suddenly appears, proclaiming herself as the &quot;god of omniscience.&quot; Youta is confused and does not believe Hina when she tells him &quot;The world will end in 30 days.&quot; After witnessing Hina's God-like predictive ability, Youta begins to believe her powers are real. Hina, who is innocent and childlike despite her supernatural powers, decides for some reason to stay at Youta's home. Thus begins the start of their lively summer before the end of the world.

	WATCHED_UNTIL=Watched season 1 (12 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Yes

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Yes

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=Sometimes
	VIOLENCE=No
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I love this anime.

Excellent story, good characters, nice music. For the music, while not standing out too much and not being very memorable, is just a perfect fit to underline the scenes and story. And the story, it is touching and well crafted. In the beginning you have this seemingly uncorrelated mess which in the end forms well crafted knot tying everything together.

Beware, it is quite emotional. It is dealing with some heavy topics can can hit you really hard.

```admonish warning collapsible=true title="SPOILER WARNING"
First of all, things are not as they seem. As the story progresses, we are slowly shown what is really going on, and it makes it that much more impact-full. The clever twist that awaits just hits you.

There is this girl claiming to be god, with abilities to foretell the future which is proven on many occasions, but there is still something out of place, something missing. As the story goes on, you slowly figure out that she is not a god, but a human in flesh who got caught in extraordinary situation - and unfortunately she will suffer because of it.

When she looses her abilities - not only ability to predict future, but also most of her mental abilities - and you see what it does to a person and people around, that is some heavy stuff. For me at least, it hit really hard. It demonstrates well many mental illnesses such as Alzheimer's or Dementia with the only difference that in this story, there is hope.
```

