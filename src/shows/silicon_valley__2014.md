{{#template
	../templates/show.md
	PUB_DATE=Thu, 08 Jun 2023 13:24:38 UTC
	MOVIE_NAME=Silicon Valley (2014)
	MOVIE_URL=https://www.imdb.com/title/tt2575988
	MOVIE_IMG=silicon_valley__2014.jpg
	PLOT=In the high-tech gold rush of modern Silicon Valley, the people most qualified to succeed are the least capable of handling success. A comedy partially inspired by Mike Judge's own experiences as a Silicon Valley engineer in the late 1980s.

	WATCHED_UNTIL=Watched until season 6 (included).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes
	
	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=1
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=No
	DRUGS=Yes
	EROTIC=Yes
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


I just love the too see so many greedy assholes in one place trying to out-fuck each other, only to have it hilariously backfire. Just one big fuck-pile of greedy bastards ... brilliant.

```admonish warning collapsible=true title="SPOILER WARNING"
Probably the best example is when Gavin Belson forcefully acquires the video-chat platform in episode 4x02. That is pure gold.
```

Besides all of that, there are some truly funny moments. Great show.

```admonish warning collapsible=true title="SPOILER WARNING"
The ending is unfortunately rather disappointing.
```

