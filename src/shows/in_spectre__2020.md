{{#template
	../templates/show.md
	PUB_DATE=Tue, 17 May 2022 18:28:17 UTC
	MOVIE_NAME=In/Spectre (2020)
	MOVIE_URL=https://www.imdb.com/title/tt11328872
	MOVIE_IMG=in_spectre__2020.jpg
	PLOT=At the young age of 11, Kotoko Iwanaga was abducted by youkai for two weeks and asked to become their "God of Wisdom", a mediator between the spirit and human worlds, to which the girl quickly agreed but at the cost of her right eye and left leg. Now, six years later, whenever youkai wish for their problems to be solved, they make their way to Kotoko for consultation.<br><br><br><br>Meanwhile, Kurou Sakuragawa, a 22-year-old university student, has just broken up with his girlfriend after he fled alone when the two encountered a kappa. Seeing this as her chance to become closer with him, Kotoko immediately makes her move, hoping to get married to Kurou one day. However, she quickly realizes there is something more to Kurou. With this knowledge, she asks for his help in solving the various issues presented by the supernatural, all the while wishing her newfound partner will eventually reciprocate her feelings.

	WATCHED_UNTIL=Watched season 1 (12 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Maybe

	STORY_SENSIBLE=Maybe
	STORY_SMART=Maybe
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Maybe
	FEELING_RELATABLE=No

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=No
	OVERALL_WOULD_PAY_FOR=No
	OVERALL_WOULD_RECOMMEND=No


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Yes
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Not gonna lie, I was a bit disappointed. When I found out about this anime, I hoped for something like [The Ancient Magus' Bride](the_ancient_magus_bride__2016.md). Instead it is rather messy and heavily diluted story that is hard to follow and makes a little sense.

The first season could have easily fit into two or three episodes and make room for better character introduction and development.

I don't know, in my opinion it does have very interesting ideas, it has a lot of potential, but the execution is lacking a lot. Overall, I am rather disappointed. Maybe the next season will be better.

