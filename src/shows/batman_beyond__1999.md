{{#template
	../templates/show.md
	PUB_DATE=Sun, 20 Mar 2022 09:26:00 UTC
	MOVIE_NAME=Batman Beyond (1999)
	MOVIE_URL=https://www.imdb.com/title/tt0147746
	MOVIE_IMG=batman_beyond__1999.jpg
	PLOT=It's been years since Batman was last seen and Bruce Wayne secludes himself away from the resurgence of crime in Gotham. After discovering Bruce's secret identity, troubled teenager Terry McGinnis dons the mantle of Batman. With Bruce supervising him, Terry battles criminals in a futuristic Gotham and brings hope to its citizens.

	WATCHED_UNTIL=Watched until season 3 (included; 13+26+13 episodes).

	VISUAL_WATCHABLE=Yes
	VISUAL_STORYTELLING=Maybe

	MUSIC_MEMORABLE=Maybe
	MUSIC_STORYTELLING=Yes

	STORY_SENSIBLE=Yes
	STORY_SMART=Yes
	STORY_ENDING=Maybe

	FEELING_MEMORABLE=Yes
	FEELING_RELATABLE=Yes

	OVERALL_WATCHED_TO_END=Yes
	OVERALL_WOULD_WATCH_AGAIN=Yes
	OVERALL_WOULD_PAY_FOR=Yes
	OVERALL_WOULD_RECOMMEND=Yes


	FUCKED_UP_LEVEL=2
	RELIGIOUS=Maybe
	AFTER_CREDIT_SCENE=No
	VIOLENCE=Yes
	DRUGS=No
	EROTIC=No
	SEX=No

	RELEASE_VERSION=Standard
	SPOKEN_LANG=ENG
}}


Well, imagine classic Batman lurking in shadows, but this time in dystopian near future. Also, with a twist: Batman is too old run around the city and catching criminals. As he thinks of retiring, he finds himself a successor. The story then revolves around the forming relationship between mentor and his student.

