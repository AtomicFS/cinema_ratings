# Cinema Ratings

Static website for my personal rating of movies and tv shows.

Links:
- [hosted site](https://cinema-ratings.white-hat-hacker.icu/)
- [main repository](https://git.sr.ht/%7Eatomicfs/cinema_ratings)
- [RSS feed](https://cinema-ratings.white-hat-hacker.icu/rss.xml)

I do not own rights to the used promotional pictures, nor the plot text. Everyting else is licensed under GPLv2.

