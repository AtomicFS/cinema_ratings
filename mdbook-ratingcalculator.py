#!/usr/bin/python
import json
import sys
import re

SECTIONS = ["Shows", "Movies"]

if __name__ == '__main__':
    if len(sys.argv) > 1:  # we check if we received any argument
        if sys.argv[1] == "supports":
            # then we are good to return an exit status code of 0, since the other argument will just be the renderer's name
            sys.exit(0)

    # load both the context and the book representations from stdin
    context, book = json.load(sys.stdin)
    # and now, we can just modify the content

    # For movies and shows:
    for section in book['sections']:
        # If current section is section with movies or shows
        if "Chapter" in section and section["Chapter"]["name"] in SECTIONS:
            score_sum = 0
            fuckedup_sum = 0
            scoreboard = []
            # For each item (movie or show)
            for item in book['sections'][book['sections'].index(section)]["Chapter"]["sub_items"]:
                # Parse old content 'original' and convert it into 'newcontent'
                original = item["Chapter"]["content"].splitlines()
                newcontent = []
                bool_evaluate = False
                score = 0
                count = 0
                fuckedup = -1
                # Find the 'fucked-up' rating
                for line in original:
                    if re.match(r'^\|.*\|.*fucked up.*\| [0-9][ ]+\|$', line):
                        fuckedup = int(
                            re.search(r'^\|.*\|.*fucked up.*\| ([0-9])[ ]+\|$', line).group(1))
                        fuckedup_sum += fuckedup
                        break
                for line in original:
                    # Find 'SCORE:' text and place in the computed score
                    if re.match(r'^#', line):
                        if bool_evaluate:
                            newcontent.append("**SCORE: {:.2f}%**".format(100/(2*count)*score))
                            scoreboard.append("| {:6.2f}% | [{}]({}) | {} |".format(
                                100/(2*count)*score, item["Chapter"]["name"], item["Chapter"]["path"], fuckedup))
                            newcontent.append("")
                        bool_evaluate = False
                    # Find the rating tables to enable score-calculation
                    if re.match(r'^## My rating', line):
                        bool_evaluate = True
                    # Parse the rating tables and calculate the score
                    if bool_evaluate:
                        #rating = re.search(r'^\|.*\|.*\| (Yes|No|Maybe|N/A)[ ]+\|$', line)
                        rating = re.search(r'^\|.*\|.*\| (Yes|No|Maybe)[ ]+\|$', line)
                        if rating:
                            if rating.group(1) == "Yes":
                                score += 2
                                count += 1
                            # if rating.group(1) == "Maybe" or rating.group(1) == "N/A":
                            if rating.group(1) == "Maybe":
                                score += 1
                                count += 1
                            if rating.group(1) == "No":
                                score += 0
                                count += 1
                    newcontent.append(line)
                book['sections'][book['sections'].index(section)]["Chapter"]["sub_items"][book['sections'][book['sections'].index(
                    section)]["Chapter"]["sub_items"].index(item)]["Chapter"]["content"] = "\n".join(newcontent)
                score_sum += 100/(2*count)*score

            # Add some statistics
            total_count = len(book['sections'][book['sections'].index(section)]["Chapter"]["sub_items"])
            book['sections'][book['sections'].index(section)]["Chapter"]["content"] += "## Statistics\n"
            stats = [
                "| Stat | Value |",
                "|:---|:--:|",
                "| Total count | {:} |".format(total_count),
                "| Average score | {:.2f}% |".format(score_sum/total_count),
                "| Average fucked-up level | {:.2f} |".format(fuckedup_sum/total_count),
                "",
                "",
                ]
            book['sections'][book['sections'].index(section)]["Chapter"]["content"] += "\n".join(stats)

            # Add score-board table
            book['sections'][book['sections'].index(section)]["Chapter"]["content"] += "## Overview\n"
            scoreboard.sort(reverse=True)
            scoreboard.insert(0, "|---:|:---|:--:|")
            scoreboard.insert(0, "| Score | Link | Fucked-up level |")
            book['sections'][book['sections'].index(section)]["Chapter"]["content"] += "\n".join(scoreboard)

            # Add statistics table to front page too
            book['sections'][0]["Chapter"]["content"] += f"## {section['Chapter']['name']}\n"
            book['sections'][0]["Chapter"]["content"] += "\n".join(stats)

    # we are done with the book's modification, we can just print it to stdout
    print(json.dumps(book))

